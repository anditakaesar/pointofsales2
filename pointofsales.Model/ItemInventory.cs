using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("MST_ITEMS_INVENTORY")]
    public class ItemInventory
    {
        public int ID { get; set; }

       
        public int? VariantID { get; set; }

        public int? Beginning { get; set; }

        public int? PurchaseOrder { get; set; }

        public int? Sales { get; set; }

        public int? Transfer { get; set; }

        public int? Adjusment { get; set; }

        public int? Ending { get; set; }

        public int? AlertAt { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("VariantID")]
        public ItemVariant ItemVariant { get; set; }
    }
}
//=======
//﻿    using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace pointofsales.Model
//{
//    [Table("MST_ITEMS_INVENTORY")]
//    public class ItemInventory
//    {
//        public int ID { get; set; }

//        public int? VariantID { get; set; }

//        public int? Beginning { get; set; }

//        public int? PurchaseOrder { get; set; }

//        public int? Sales { get; set; }

//        public int? Transfer { get; set; }

//        public int? Adjusment { get; set; }

//        public int? Ending { get; set; }

//        public int? AlertAt { get; set; }

//        public int? CreatedBy { get; set; }

//        public DateTime? CreatedOn { get; set; }

//        public int? ModifiedBy { get; set; }

//        public DateTime? ModifiedOn { get; set; }

//        // Relationships
//        public ItemVariant ItemVariant { get; set; }
//    }
//}
//>>>>>>> 8b2076fccd2a6f3be166da23486273f6991605c5

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pointofsales.Model
{
    [Table("MST_REGION")]
    public class Region
    {
        public int ID { get; set; }

        public int? ProvinceID { get; set; }

        [StringLength(50)]
        public string RegionName { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        /*
        public List<Customer> Customers { get; set; }        
        */

        // Relationships
        public List<District> Districs { get; set; }

        public Province Province { get; set; }

    }
}
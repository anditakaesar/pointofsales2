﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_ADJUSMENT_STOCK_DETAIL")]
    public class AdjustmentStockDetail
    {
        public int ID { get; set; }

        public int? VariantID { get; set; }

        public int? InStock { get; set; }

        public int? ActualStock { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // Data - Relationships

        [ForeignKey("AdjustmentStock")]
        public int? HeaderID { get; set; }

        public AdjustmentStock AdjustmentStock { get; set; }



    }
}

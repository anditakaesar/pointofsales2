﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_PAYMENT_DETAIL")]
    public class PaymentDetail
    {
        public int ID { get; set; }

        public int? PaymentID { get; set; }

        public int? VariantID { get; set; }

        public int? Quantity { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? SubTotal { get; set; }

        public int? CreatedBy { get; set; } 

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // relation paymantdetail to payment by abid
        public Payment Payment { get; set; }
        public ItemVariant Variant { get; set; }
    }
}

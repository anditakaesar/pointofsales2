﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_PAYMENT")]
    public class Payment
    {
        public int ID { get; set; }

        public int? CustomerID { get; set; }

        public int? EmployeeID { get; set; }

        public decimal? GrandTotal { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }



        // Relationship
        // Hubungan Payment dengan detailnya (one to many) by abid
        public List<PaymentDetail> PaymentDetails { get; set; }
        
        public Customer Customer { get; set; }

        public Employee Employee { get; set; }

    }
}

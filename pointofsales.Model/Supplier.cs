﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("MST_SUPPLIERS")]
    public class Supplier
    {
        public Supplier()
        {

        }

        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        // public int? ProvinceID { get; set; }

        // public int? RegionID { get; set; }

        public int? DistrictID { get; set; }

        [StringLength(6)]
        public string PostalCode { get; set; }

        [StringLength(16)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // Relationships
        public List<PurchaseOrder> PurchaseOrder { get; set; }

        public District District { get; set; }
    }
}

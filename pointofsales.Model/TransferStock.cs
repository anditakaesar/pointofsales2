﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_TRANSFER_STOCK")]
    public class TransferStock
    {
        public int ID { get; set; }

        public int? FromOutlet { get; set; }

        public int? ToOutlet { get; set; }

        [StringLength(255)]
        public string Note { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // Relationships

    }
}

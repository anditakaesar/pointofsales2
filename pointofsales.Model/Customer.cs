﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("MST_CUSTOMERS")]
    public class Customer
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string CustomerName { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(16)]
        public string Phone { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        //public int? ProvinceID { get; set; }

        //public int? RegionID { get; set; }

        // Customer hanya akan ada DistrictID

        public int? DistrictID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public District Districts { get; set; }

        /* Foreign Keys 

        public Province Province { get; set; }

        public Region Region { get; set; }

        */
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_PURCHASE_ORDER_HISTORY")]
    public class PurchaseOrderHistory
    {
        public int ID { get; set; }
        public int? PurchaseOrderID { get; set; }

        public int? StatusID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
        // inisiasi FK ke PurchaseOrder by Fachri
        public PurchaseOrder PurchaseOrder { get; set; }
    }
}

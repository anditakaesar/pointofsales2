﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pointofsales.Model
{
    [Table("MST_PROVINCE")]
    public class Province
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string ProvinceName { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        /*
        public List<Customer> Customers { get; set; }
        */

        // Relationships
        public List<Region> Regions { get; set; }
    }
}
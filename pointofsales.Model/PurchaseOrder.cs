﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_PURCHASE_ORDER")]
    public class PurchaseOrder
    {
        public int ID { get; set; }

        public int? OutletID { get; set; }

        public int? SupplierID { get; set; }

        [StringLength(20)]
        public string OrderNo { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        public int? StatusID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        // update FK untuk PurchaseOrder by Fachri
        public List<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }
        public List<PurchaseOrderHistory> PurchaseOrderHistory { get; set; }

    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("TRX_TRANSFER_STOCK_DETAIL")]
    public class TransferStockDetail
    {
        public int ID { get; set; }

        public int? TransferStockID { get; set; }

        public int? InStock { get; set; }

        public int? VariantID { get; set; }

        public int? Quantity { get; set; }

        public string SKU { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // Relationship
        public TransferStock TransferStock { get; set; }

    }
}
//=======
//﻿using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace pointofsales.Model
//{
//    [Table("TRX_TRANSFER_STOCK_DETAIL")]
//    public class TransferStockDetail
//    {
//        public int ID { get; set; }

//        public int? TransferStockID { get; set; }

//        public int? InStock { get; set; }

//        public int? VariantID { get; set; }

//        public int? Quantity { get; set; }

//        public int? CreatedBy { get; set; }

//        public DateTime? CreatedOn { get; set; }

//        public int? ModifiedBy { get; set; }

//        public DateTime? ModifiedOn { get; set; }

//        // Relationship
//        public TransferStock TransferStock { get; set; }

//    }
//}
//>>>>>>> 8b2076fccd2a6f3be166da23486273f6991605c5

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.Model
{
    [Table("MST_ITEMS")]
    public class Item
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public int? CategoryID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // Data Relationships
        public Category Category { get; set; }

        public List<ItemVariant> ItemVariants { get; set; }
    }
}

﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//library yang ditambahkan
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace pointofsales.DAL
{
    public class LoginDALToni
    {
        public static bool RemoveSession() {
            bool result = false;
            if (HttpContext.Current.Session["username"] != null) {
                HttpContext.Current.Session.Clear();
                //HttpContext.Current.Session.RemoveAll();
                //MessageBox.Show("Your Name: "+HttpContext.Current.Session["username"].ToString());
                result = true;
            }
            return result;
        }

        public static Boolean CekUser(string username, string password)
        {
            var result = false;
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                var employee = (from a in db.Employee
                                join b in db.Outlet on a.OutletID equals b.ID
                                join c in db.Role on a.RoleID equals c.ID
                                where a.Email == username
                                where a.Password == password
                                select a).SingleOrDefault<Employee>();
                if (employee != null)
                {
                    HttpContext.Current.Session["username"] = employee.Email;
                    HttpContext.Current.Session["Nama"] = employee.FirstName + ' ' + employee.LastName;
                    HttpContext.Current.Session["IDStaff"] = employee.ID;
                    HttpContext.Current.Session["IDOutlet"] = employee.OutletID;
                    HttpContext.Current.Session["IDRole"] = employee.RoleID;
                    result = true;
                    //MessageBox.Show("sukses");
                    
                }
                else
                {
                    result = false;
                    //MessageBox.Show("gagal");
                }

            }
            return result;
        }

        public static Boolean CekPassUser(int ID, string password)
        {
            var result = true;


            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                var employee = (from a in db.Employee
                                where a.ID == ID &&
                                a.Password == password
                                select a).SingleOrDefault<Employee>();
                if (employee == null)
                {
                    result = false;
                }
            }
            return result;
        }

        public static Boolean ChangePass(int ID, string password)
        {
            var result = true;
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Employee employee = db.Employee.FirstOrDefault(x=>x.ID==ID);
                if (employee != null)
                {
                    employee.Password = password;
                    //MessageBox.Show("sukses");
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
                else
                {
                    result = false;
                    //MessageBox.Show("gagal");
                }
            }
            return result;
		}

        public static UserLogin CekUser2(string username, string password)
        {
            using (var db = new PointOfSalesDBContext())
            {
                var employee = (from a in db.Employee
                                join b in db.Outlet on a.OutletID equals b.ID
                                join c in db.Role on a.RoleID equals c.ID
                                where a.Email == username
                                where a.Password == password
                                select a).SingleOrDefault<Employee>();

                if (employee != null)
                {
                    HttpContext.Current.Session["username"] = employee.Email;
                    HttpContext.Current.Session["Nama"] = employee.FirstName + ' ' + employee.LastName;
                    HttpContext.Current.Session["IDStaff"] = employee.ID;
                    HttpContext.Current.Session["IDOutlet"] = employee.OutletID;
                    HttpContext.Current.Session["IDRole"] = employee.RoleID;
                    return new UserLogin
                    {
                        result = true,
                        IDRole = (int) employee.RoleID
                    };
                }
                else
                {
                    return new UserLogin
                    {
                        result = false,
                        IDRole = 0
                    };
                }
            }
        }

        public class UserLogin
        {
            public bool result { get; set; }
            public int IDRole { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;
using pointofsales.DAL.ViewModel;

namespace pointofsales.DAL
{
    public class CategoryDAL
    {
        // Mengambil semua list category dari database, dengan Status Enabled == TRUE
        public static List<Category> GetCategory()
        {
            List<Category> result = new List<Category>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Category.Where(cat => cat.Enabled == true).ToList();
                // todo: where status is Enabled == true

            }

            return result;
        }

        // Mengambil category dengan ID tertentu (overload GetCategory())
        public static List<Category> GetCategory(int id)
        {
            List<Category> result = new List<Category>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Category.Where(x => x.ID == id).ToList();
            }

            return result;
        }

        // Insert category (tidak digabung bersama Update category)
        public static bool InsertCategory(Category cat)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                // set createdOn dengan NOW
                cat.CreatedOn = DateTime.Now;
                cat.ModifiedOn = DateTime.Now;
                cat.Enabled = true;

                try
                {
                    db.Category.Add(cat);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    // Todo: logs, etc...
                    // throw;
                }

            }

            return result;
        }

        // Insert category ke SEMUA outlet, NOT USED
        public static bool InsertCategoryToOutlets(Category cat)
        {
            bool result = false;

            // if user punya akses ke semua outlet, save to all outlet

            using (var db = new PointOfSalesDBContext())
            {
                if (cat != null)
                {
                    // insert Category property
                    // cat.CreatedBy = ''; employee.ID
                    cat.CreatedOn = DateTime.Now;
                    try
                    {
                        db.Category.Add(cat);

                        // save
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        // todo: logs, etc
                    }
                }
            }

            return result;
        }

        // Update category, dibuat return bool untuk mereturn berhasil apa error
        public static bool UpdateCategory(Category category)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                // Retrieve category yang akan diupdate
                Category categoryToUpdate = db.Category.FirstOrDefault(cat => cat.ID == category.ID);

                // cek jika null
                if (categoryToUpdate != null)
                {
                    categoryToUpdate.Name = category.Name;
                    categoryToUpdate.OutletID = category.OutletID;
                    categoryToUpdate.CreatedBy = category.CreatedBy;
                    categoryToUpdate.CreatedOn = category.CreatedOn;
                    categoryToUpdate.ModifiedBy = category.ModifiedBy;
                    // set modifiedOn datetime
                    categoryToUpdate.ModifiedOn = DateTime.Now;

                    try
                    {
                        // Set state Category menjadi Modified
                        db.Entry(categoryToUpdate).State = System.Data.Entity.EntityState.Modified;
                        // Save db pada EF
                        db.SaveChanges();
                        // Set status jika berhasil
                        result = true;
                    }
                    catch (Exception)
                    {
                        // throw;
                    }
                }

            }

            return result;
        }

        // Delete category, dibuat bool untuk return berhasil apa error, Enabled = FALSE
        public static bool DeleteCategory(int id)
        {
            bool status = false;

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Category CatToDelete = db.Category.FirstOrDefault(cat => cat.ID == id);

                // set Enabled menjadi FALSE
                if (CatToDelete != null)
                {
                    // PSEUDO-DELETE
                    CatToDelete.Enabled = false;
                    CatToDelete.ModifiedOn = DateTime.Now;

                    try
                    {
                        db.Entry(CatToDelete).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        status = true;
                    }
                    catch (Exception)
                    {
                        // TBA : Logs, Error, etc
                        // throw;
                    }
                }

            }

            return status;
        }

        // Search by key of categoryName dengan 
        public static List<Category> SearchCategory(string key, bool enabled)
        {
            List<Category> result = new List<Category>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                // Can be more simple?, search: DbContext, return object result
                if (enabled || key.Equals(String.Empty))
                {
                    result = db.Category.Where(cat => cat.Name.Contains(key) && cat.Enabled == true).ToList();
                }
                else
                {
                    result = db.Category.Where(cat => cat.Name.Contains(key) && cat.Enabled == false).ToList();
                }


            }

            return result;
        }


        // Get Semua Item untuk Assign To Item
        public static List<Item> GetAllItem()
        {
            // Asumsi ID Category untuk UNCATEGORIZED adalah 1
            return new PointOfSalesDBContext().Item.Where(item => (item.CategoryID == null || item.CategoryID == 1)).ToList();
        }

        // Update Item agar memiliki Category Id
        public static bool UpdateItem(Item item)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                Item itemToUpdate = db.Item.FirstOrDefault(it => it.ID == item.ID);
                if (itemToUpdate != null)
                {
                    itemToUpdate.CategoryID = item.CategoryID;

                    try
                    {
                        db.Entry(itemToUpdate).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        result = true;
                    }
                    catch (Exception) { }
                }

            }

            return result;
        }

        // Search Item by Name
        public static List<Item> SearchItem(string key)
        {
            // Asumsi Uncategorized = 1
            return new PointOfSalesDBContext().Item
                .Where(item => item.Name.Contains(key))
                .Where(item => (item.CategoryID == null || item.CategoryID == 1))
                .ToList();
        }

        // Get Item EXCLUDING Category tertentu
        public static List<Item> GetItemExcludeCategory(int catId)
        {

            return new PointOfSalesDBContext().Item.Where(item => item.CategoryID != catId).ToList();

        }

        // Get Category Dengan View Model, Count Item dengan Category Enabled option
        public static List<CategoryViewModel> GetCategoryCount(bool enabled)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new PointOfSalesDBContext())
            {
                // Ambil Semua Category
                List<Category> Categories = db.Category
                    .Where(cat => cat.Enabled == enabled)
                    .ToList();

                foreach (var category in Categories)
                {
                    CategoryViewModel Cvm = new CategoryViewModel
                    {
                        ID = category.ID,
                        CreatedBy = category.CreatedBy,
                        CreatedOn = category.CreatedOn,
                        ModifiedBy = category.ModifiedBy,
                        ModifiedOn = category.ModifiedOn,
                        Name = category.Name,
                        OutletID = category.OutletID,
                        ItemCount = db.Item
                            .Where(it => it.CategoryID == category.ID)
                            .Distinct()
                            .ToList()
                            .Count()
                    };
                    result.Add(Cvm);
                }
            }

            return result;
        }


        // Update
        public static bool UpdateCategoryOfItem(UpdateCategoryVM update)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                foreach (var index in update.ItemsToUpdate)
                {
                    Item itemToUpdate = db.Item.FirstOrDefault(item => item.ID == index);

                    itemToUpdate.CategoryID = update.CategoryID;

                    db.Entry(itemToUpdate).State = System.Data.Entity.EntityState.Modified;
                }

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception) {}
            }

            return result;
        }
    }
}

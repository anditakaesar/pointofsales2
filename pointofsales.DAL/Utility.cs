﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL
{
    public class Utility
    {
        // Hash encryption, possible encrypt SHA1
        public static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // lowercase hash
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }
    }
}

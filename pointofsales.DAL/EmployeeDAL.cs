﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;
using System.Security.Cryptography;

namespace pointofsales.DAL
{
    public class EmployeeDAL
    {
        // Read
        public static List<Employee> GetEmployee()
        {
            List<Employee> result = new List<Employee>();
            using (var db = new PointOfSalesDBContext())
            {
                result = db.Employee.ToList();
            }

            return result;
        }

        // Read Employee by ID
        public static Employee GetEmployee(int empId)
        {
            Employee result = new Employee();
            using (var db = new PointOfSalesDBContext())
            {
                result = db.Employee.FirstOrDefault(emp => emp.ID == empId);
            }
            return result;
        }


        // Insert
        public static bool InsertEmployee(Employee emp)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                try
                {
                    emp.CreatedOn = DateTime.Now;
                    emp.ModifiedOn = DateTime.Now;
                    db.Employee.Add(emp);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception) {}
            }
            return result;
        }

        // Update
        public static bool UpdateEmployee(Employee emp)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                Employee empToUpdate = db.Employee.FirstOrDefault(e => e.ID == emp.ID);
                if(empToUpdate != null)
                {
                    empToUpdate.FirstName = emp.FirstName;
                    empToUpdate.LastName = emp.LastName;
                    empToUpdate.ModifiedBy = emp.ModifiedBy;
                    empToUpdate.OutletID = emp.OutletID;
                    empToUpdate.Title = emp.Title;
                    empToUpdate.RoleID = emp.RoleID;
                    empToUpdate.Email = emp.Email;
                    empToUpdate.ModifiedOn = DateTime.Now;

                    // Need encryption here
                    empToUpdate.Password = emp.Password;
                    try
                    {
                        db.Entry(empToUpdate).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception) {}
                }
            }
            return result;
        }

        // Delete, HARD-DELETE
        public static bool DeleteEmployee(int empId)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                // Not a pseudo-delete
                Employee empToDelete = db.Employee.FirstOrDefault(emp => emp.ID == empId);

                try
                {
                    db.Entry(empToDelete).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                }
                catch (Exception) { }
            }

            return result;
        }

        // Search Employee
        public static List<Employee> SearchEmployee(string key)
        {
            List<Employee> result = new List<Employee>();
            using (var db = new PointOfSalesDBContext())
            {
                result = db.Employee.Where(emp => emp.FirstName.Contains(key) || emp.LastName.Contains(key)).ToList();
            }
            return result;
        }

        // Get Roles
        public static List<Role> GetRoles()
        {
            return new PointOfSalesDBContext().Role.ToList();
        }

        // Get Role by Id
        public static Role GetRoleById(int roleId)
        {
            return new PointOfSalesDBContext()
                .Role
                .FirstOrDefault(role => role.ID == roleId);
        }
    }
}

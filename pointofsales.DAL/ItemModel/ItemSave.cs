﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;

namespace pointofsales.DAL.ItemModel
{
    public class ItemSave
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int CategoryID { get; set; }

        public List<VariantSave> VariantSave { get; set; }
    }

    public class VariantSave
    {
        public int ID { get; set; }

        public string VariantName { get; set; }

        public decimal? Price { get; set; }

        public string SKU { get; set; }

        public InveSave InveSave{get; set;}
    }

    public class InveSave
    {
        public int Stock { get; set; }

        public int AlertAt { get; set; }

        public int Ending { get; set; }

        public int Begining { get; set;}
    }


}

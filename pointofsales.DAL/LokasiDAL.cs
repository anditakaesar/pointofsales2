﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;

namespace pointofsales.DAL
{
    public class LokasiDAL
    {
        #region Provinsi
        // GetProvinsi ALL
        public static List<Province> GetProvinsi()
        {
            return new PointOfSalesDBContext().Province.ToList();
        }

        // GetProvinsi by ID
        public static Province GetProvinsiById(int id)
        {
            return new PointOfSalesDBContext().Province.FirstOrDefault(prov => prov.ID == id);
        }

        // Search Provinsi
        public static List<Province> SearchProvinsi(string key)
        {
            return new PointOfSalesDBContext().Province.Where(prov => prov.ProvinceName.Contains(key)).ToList();
        }
        #endregion

        #region Kota
        // Get Kota
        public static List<Region> GetKota()
        {
            return new PointOfSalesDBContext().Region.ToList();
        }

        // Get Kota by ID
        public static Region GetKotaById(int id)
        {
            return new PointOfSalesDBContext().Region.FirstOrDefault(kota => kota.ID == id);
        }

        // Search Kota
        public static List<Region> SearchKota(string key)
        {
            return new PointOfSalesDBContext().Region.Where(regi => regi.RegionName.Contains(key)).ToList();
        }

        // Get Kota by ProvId
        public static List<Region> GetKotaByProvId(int id)
        {
            return new PointOfSalesDBContext().Region.Where(kota => kota.ProvinceID == id).ToList();
        }
        #endregion

        #region Kecamatan
        // Get Kecamatan ALL
        public static List<District> GetKecamatan()
        {
            return new PointOfSalesDBContext().District.ToList();
        }

        // Get Kecamatan by Id
        public static District GetKecamatanById(int id)
        {
            return new PointOfSalesDBContext().District.FirstOrDefault(kec => kec.ID == id);
        }

        // Search Kecamatan
        public static List<District> SearchKecamatan(string key)
        {
            return new PointOfSalesDBContext().District.Where(kec => kec.DistrictName.Contains(key)).ToList();
        }

        // Get Kecamatan by Kota id
        public static List<District> GetKecamatanByKotaId(int id)
        {
            return new PointOfSalesDBContext().District.Where(kec => kec.RegionID == id).ToList();
        }

        // Get Nama Kecamatan
        public static string GetNamaKecamatanById(int id)
        {
            District keca = GetKecamatanById(id);
            return keca.DistrictName;
        }
        #endregion

    }
}

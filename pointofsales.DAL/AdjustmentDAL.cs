﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

//library untuk session
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using pointofsales.DAL.ViewModel;
using System.Globalization;

namespace pointofsales.DAL
{
    public class AdjustmentDAL
    {
        //method mengambil data adjustment
        /*select Note, Name + ' - ' + VariantName as items, Adjusment 
            from MST_ITEMS_INVENTORY a inner join MST_ITEMS b on a.ItemVariant_ID = b.ID
            left join MST_ITEMS_VARIANT c on a.VariantID = c.ID
            left join TRX_ADJUSMENT_STOCK_DETAIL d on d.VariantID = c.ID
            left join TRX_ADJUSMENT_STOCK e on e.ID = d.HeaderID*/

        //fungsi untuk menampilkan semua data adjusment
        public static List<ItemInventoryViewModel> GetAllAdjustment()
        {
            var usr = HttpContext.Current.Session["username"].ToString();


            List<ItemInventoryViewModel> result = new List<ItemInventoryViewModel>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Employee employee = db.Employee.FirstOrDefault(x => x.Email == usr);

                //DateTime startDate = new DateTime(2016, 10, 14);
                result = (from a in db.ItemInventory
                          join b in db.ItemVariant on a.VariantID equals b.ID
                          join c in db.AdjustmentStockDetail on a.VariantID equals c.VariantID
                          join d in db.AdjustmentStock on c.HeaderID equals d.ID
                          where b.OutletID == employee.OutletID
                          select new ItemInventoryViewModel
                          {
                              VariantID = a.VariantID,
                              CreatedOn = d.CreatedOn,
                              Note = d.Note,
                              Adjusment = a.Adjusment,
                              VariantName = b.VariantName
                          }).ToList();

            }

            return result;
        }

        /*fungsi untuk mengambil data berdasar tanggal*/
        public static List<ItemInventoryViewModel> getDataByDate(DateTime tglAwal, DateTime tglAkhir)
        {
            var usr = HttpContext.Current.Session["username"].ToString();
            List<ItemInventoryViewModel> result = new List<ItemInventoryViewModel>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Employee employee = db.Employee.FirstOrDefault(x => x.Email == usr);
                //DateTime sdate = new DateTime(2016, 10, 14);
                IFormatProvider culture = new CultureInfo("en-US", true);

                //string dateString;
                //dateString = "2012-07-18";
                // Exception: String was not recognized as a valid DateTime because the day of week was incorrect.  
                //DateTime dateTime12 = Convert.ToDateTime(dateString);  

                //DateTime dtFromDate;
                //DateTime dtToDate;

                //DateTime.TryParse(tglAwal,out dtFromDate);
                //DateTime.TryParse(tglAkhir, out dtToDate);
                //DateTime dResult = DateTime.ParseExact("2015-07-18", "yyyy-mm-dd", culture, DateTimeStyles.AdjustToUniversal);

                //MessageBox.Show("tanggal awal : " + new DateTime(tglAwal.Year, tglAwal.Month, tglAwal.Day) + " tanggal akhir : " + tglAkhir);

                //String format = "yyyy/MM/dd hh:mm:ss";
                result = (from a in db.ItemInventory
                          join b in db.ItemVariant on a.VariantID equals b.ID
                          join c in db.AdjustmentStockDetail on a.VariantID equals c.VariantID
                          join d in db.AdjustmentStock on c.HeaderID equals d.ID
                          where d.CreatedOn >= tglAwal
                          where d.CreatedOn <= tglAkhir
                          where b.OutletID == employee.OutletID
                          select new ItemInventoryViewModel
                          {
                              VariantID = a.VariantID,
                              CreatedOn = d.CreatedOn,
                              Note = d.Note,
                              Adjusment = a.Adjusment,
                              VariantName = b.VariantName
                          }).ToList();

            }

            return result;
        }
        /*end fungsi untuk mengambil data berdasar tanggal*/

        public static List<EmployeeViewModel> GetCurrentOutlet()
        {
            List<EmployeeViewModel> result = new List<EmployeeViewModel>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                var usr = HttpContext.Current.Session["username"].ToString();
                result = (from employe in db.Employee
                          join outlet in db.Outlet on employe.OutletID equals outlet.ID
                          where employe.Email == usr
                          select new EmployeeViewModel
                          {
                              OutletID = employe.OutletID,
                              OutletName = outlet.OutletName
                          }
                       ).ToList();
            }
            return result;
        }

        public static List<ItemVariantViewModel> GetAllVariant()
        {
            var usr = HttpContext.Current.Session["username"].ToString();
            List<ItemVariantViewModel> result = new List<ItemVariantViewModel>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Employee employee = db.Employee.FirstOrDefault(x => x.Email == usr);
                result = (from items in db.Item
                          join itemvariant in db.ItemVariant on items.ID equals itemvariant.ItemID
                          where itemvariant.OutletID == employee.OutletID
                          select new ItemVariantViewModel
                          {
                              ID = itemvariant.ID,
                              VariantName = itemvariant.VariantName
                          }).ToList();

            }

            return result;
        }

        public static List<ItemVariantViewModel> GetVariantByID(int ID)
        {
            List<ItemVariantViewModel> result = new List<ItemVariantViewModel>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from items in db.Item
                          join itemvariant in db.ItemVariant on items.ID equals itemvariant.ItemID
                          join iteminventory in db.ItemInventory on itemvariant.ID equals iteminventory.VariantID
                          where itemvariant.ID == ID
                          select new ItemVariantViewModel
                          {
                              ID = itemvariant.ID,
                              VariantName = itemvariant.VariantName,
                              Ending = iteminventory.Beginning
                          }).ToList();
            }
            return result;
        }

        //fungsi untuk menyimpan data adjusment
        //isi parameter dengan nama view modelnya
        public static bool SaveDataAdjusment(HeaderAdjusmentStockViewModel obj)
        {
            var result = false;

            using (var db = new PointOfSalesDBContext())
            {
                //inisialisasi nama tabel yang akan ditambah datanya
                AdjustmentStock tbAdjusment = new AdjustmentStock();
                tbAdjusment.OutletID = obj.OutletID;
                tbAdjusment.Note = obj.Note;
                tbAdjusment.CreatedBy = 123;
                tbAdjusment.CreatedOn = DateTime.Now;
                tbAdjusment.ModifiedBy = 123;
                tbAdjusment.ModifiedOn = DateTime.Now;

                db.AdjustmentStock.Add(tbAdjusment);

                //inisialisasi list untuk table adjustment stock detail
                List<AdjustmentStockDetail> tbAdjusmentDetail = new List<AdjustmentStockDetail>();

                //lakukan pengulangan untuk list yang dikirim via ajax
                foreach (var item in obj.AdjusmentStockDetailViewModels)
                {
                    //isi list ke table adjusment stock details
                    tbAdjusmentDetail.Add(new AdjustmentStockDetail
                    {
                        HeaderID = tbAdjusment.ID,
                        VariantID = item.VariantID,
                        InStock = item.InStock,
                        ActualStock = item.ActualStock,
                        CreatedBy = 123,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = 123,
                        ModifiedOn = DateTime.Now
                    });
                }

                //lakukan pengulangan untuk menambahkan data ke adjusment stock detail
                foreach (var item in tbAdjusmentDetail)
                {
                    db.AdjustmentStockDetail.Add(item);
                }


                try
                {

                    if (UpdateInventory(obj.AdjusmentStockDetailViewModels, (int)obj.OutletID) == true)
                    {
                        db.SaveChanges();
                        result = true;
                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show("Error : " + e.Message);
                    throw;
                }
            }
            return result;
        }

        //fungsi untuk update inventory
        public static bool UpdateInventory(List<AdjusmentStockDetailViewModel> detailAdjusment, int currentOutlet)
        {
            var result = false;
            using (var db = new PointOfSalesDBContext())
            {
                //lakukan pengulangan dari adjusment detail view model

                //MessageBox.Show("isi dan panjang : " + detailAdjusment.Count() + " p : " + adjustDetail.InStock + " - " + adjustDetail.ActualStock + " - " + currentOutlet);

                var jumVariant = (from d in db.ItemVariant
                                  join e in db.Outlet on d.OutletID equals e.ID
                                  where d.OutletID == currentOutlet
                                  select d).Count();

                if (jumVariant > 0)
                {
                    foreach (AdjusmentStockDetailViewModel adjustDetail in detailAdjusment)
                    {

                        //ambil data variant yang ada di outlet pengguna
                        ItemVariant tempVariant = db.ItemVariant.FirstOrDefault(x => x.ID == adjustDetail.VariantID && x.OutletID == currentOutlet);
                        //MessageBox.Show("item variant : " + tempVariant + " - ");

                        //bandingkan data varian yang ada di oulet dengan yang ada di tabel inventory
                        ItemInventory inventoryToUpdate = db.ItemInventory.FirstOrDefault(x => x.VariantID == tempVariant.ID);
                        //MessageBox.Show("item inventory : " + inventoryToUpdate.Adjusment + " - " + inventoryToUpdate.Ending);

                        if (inventoryToUpdate != null)
                        {
                            int jumAdj = 0;

                            if (inventoryToUpdate.Adjusment == null)
                            {
                                inventoryToUpdate.Adjusment = 0;
                            }

                            jumAdj = (int)adjustDetail.ActualStock - (int)adjustDetail.InStock;


                            //MessageBox.Show("jumlah : " + jumAdj);
                            if (jumAdj >= 0)
                            {
                                inventoryToUpdate.Adjusment = jumAdj;
                                inventoryToUpdate.Ending = (inventoryToUpdate.Beginning + inventoryToUpdate.PurchaseOrder + jumAdj) - (inventoryToUpdate.Sales + inventoryToUpdate.Transfer);
                            }
                            else
                            {
                                inventoryToUpdate.Adjusment = jumAdj;
                                inventoryToUpdate.Ending = ((inventoryToUpdate.Beginning + inventoryToUpdate.PurchaseOrder) - (inventoryToUpdate.Sales + inventoryToUpdate.Transfer)) + jumAdj;
                                //MessageBox.Show("hasil : " + inventoryToUpdate.Ending);
                            }

                            db.SaveChanges();
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else
                {
                    result = false;
                }


            }

            return result;

        }


    }
}


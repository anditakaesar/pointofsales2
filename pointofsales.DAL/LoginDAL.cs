﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//library yang ditambahkan
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace pointofsales.DAL
{
    public class LoginDAL
    {
        public static Boolean CekUser(string username, string password)
        {
            var result = false;
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                var employee = (from a in db.Employee
                                join b in db.Outlet on a.OutletID equals b.ID
                                join c in db.Role on a.RoleID equals c.ID
                                where a.Email == username
                                where a.Password == password
                                select a).SingleOrDefault<Employee>();
                if (employee != null)
                {
                    MessageBox.Show("login successed");
                    HttpContext.Current.Session["username"] = employee.Email;
                    result = true;
                   
                }
                else
                {
                    result = false;
                    //MessageBox.Show("gagal");
                }
            }
            return result;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL
{
    public class IsiItem
    {
        public Decimal? PriceList { get; set; }

        public int? AlertAtList { get; set; }

        public int? StockList { get; set; }

        public string VariantList { get; set; }
        public int? OutletID { get; set; }
        public string SKUlist { get; set; }

        public string VarIDList { get; set; }

        public ItemDAL ItemDAL { get; set; }

    }
}

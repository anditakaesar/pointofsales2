﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class SummaryVM
    {
        public string ItemName { get; set; }
        public string VariantName { get; set; }
        public string CategoryName { get; set; }
        public int? Beginning { get; set; }
        public int? PurchaseOrder { get; set; }
        public int? Sales { get; set; }
        public int? Adjusment { get; set; }
        public int? Transfer { get; set; }
        public int? Ending { get; set; }
        
    }
}

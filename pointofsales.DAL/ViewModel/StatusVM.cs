﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class StatusVM
    {
        public DateTime? CreatedOn { get; set; }
        public string Name { get; set; }
        public string  OrderNo { get; set; }
        public decimal? SubTotal { get; set; }
        public string StatusName { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class UpdateCategoryVM
    {
        public int CategoryID { get; set; }
        public List<int> ItemsToUpdate { get; set; }
    }
}

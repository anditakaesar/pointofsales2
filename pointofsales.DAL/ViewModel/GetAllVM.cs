﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class GetAllVM
    {
        public int? DetailID { get; set; }
        public int? HistoryID { get; set; }
        public int? SupplierID { get; set; }
        public int? OutletID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Name { get; set; }
        public string OrderNo { get; set; }
        public decimal? SubTotal { get; set; }
        public string StatusName { get; set; }
        
    }
}

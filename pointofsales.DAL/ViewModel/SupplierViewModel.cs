﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class SupplierViewModel
    {
        public SupplierViewModel()
        {

        }

        public SupplierViewModel(Supplier supplier)
        {
            int kecId = (int)supplier.DistrictID;
            int kotaId = (int)LokasiDAL.GetKecamatanById(kecId).RegionID;
            int provId = (int)LokasiDAL.GetKotaById(kotaId).ProvinceID;

            this.ID = supplier.ID;
            this.Address = supplier.Address;
            this.CreatedBy = supplier.CreatedBy;
            this.CreatedOn = supplier.CreatedOn;
            this.DistrictID = supplier.DistrictID;
            this.Email = supplier.Email;
            this.ModifiedBy = supplier.ModifiedBy;
            this.ModifiedOn = supplier.ModifiedOn;
            this.Name = supplier.Name;
            this.Phone = supplier.Phone;
            this.PostalCode = supplier.PostalCode;
            this.Kecamatan = LokasiDAL.GetKecamatanById(kecId).DistrictName;
            this.Kota = LokasiDAL.GetKotaById(kotaId).RegionName;
            this.Provinsi = LokasiDAL.GetProvinsiById(provId).ProvinceName;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? DistrictID { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        // Lokasi
        public string Kecamatan { get; set; }
        public string Kota { get; set; }
        public string Provinsi { get; set; }
    }
}

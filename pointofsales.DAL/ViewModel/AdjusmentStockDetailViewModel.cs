﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class AdjusmentStockDetailViewModel
    {
        public int ID { get; set; }

        public int? VariantID { get; set; }

        public int? InStock { get; set; }

        public int? ActualStock { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("AdjustmentStock")]
        public int? HeaderID { get; set; }

        public HeaderAdjusmentStockViewModel HeaderAdjusmentStockViewModel { get; set; }
    }
}

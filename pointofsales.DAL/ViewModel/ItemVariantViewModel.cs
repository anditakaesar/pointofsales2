﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class ItemVariantViewModel
    {
        public int ID { get; set; }

        public int? ItemID { get; set; }

        public int? OutletID { get; set; }


        public string VariantName { get; set; }


        public string SKU { get; set; }

        public decimal? Price { get; set; }

        public int? CreatedBy { get; set; }

        public int? Ending { get; set; }
        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class PurchaseOrderVM
    {
        public int ID { get; set; }

        public int? OutletID { get; set; }

        public int? SupplierID { get; set; }

        public string OrderNo { get; set; }

        public string Notes { get; set; }

        public int? StatusID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
       
        public int? PurchaseOrderID { get; set; } // a.k.a HeaderID

        public int? IdVariant { get; set; }

        public int? Quantity { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? SubTotal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class ItemVM
    {
        public int IdItem { get; set; }
        public int IdVariant { get; set; }
        public string NamaItem { get; set; }
        public string NamaVariant { get; set; }
        public int? Stock { get; set; }
        public int? Order { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? Total { get; set; }
        
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class HeaderAdjusmentStockViewModel
    {
        public int ID { get; set; }

        public int? OutletID { get; set; }


        public string Note { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public List<AdjusmentStockDetailViewModel> AdjusmentStockDetailViewModels { get; set; }

    }
}

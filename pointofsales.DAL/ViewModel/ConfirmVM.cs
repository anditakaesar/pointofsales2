﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class ConfirmVM
    {
        public string Notes { get; set; }
        public string OrderNo { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }
        public string OtlName { get; set; }
        public string OtlPhone { get; set; }
        public string OtlAddress { get; set; }
        
    }
}

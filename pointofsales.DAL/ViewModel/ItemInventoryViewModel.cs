﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class ItemInventoryViewModel
    {
        public int ID { get; set; }

        public int? VariantID { get; set; }
        public int? OutletID { get; set; }

        public int? Beginning { get; set; }

        public int? PurchaseOrder { get; set; }

        public int? Sales { get; set; }

        public int? Transfer { get; set; }

        public int? Adjusment { get; set; }

        public int? Ending { get; set; }

        public string VariantName { get; set; }

        public string Note { get; set; }
        public int? AlertAt { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}

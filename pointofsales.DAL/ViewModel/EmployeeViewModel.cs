﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL.ViewModel
{
    public class EmployeeViewModel
    {
        public int ID { get; set; }


        public string FirstName { get; set; }


        public string LastName { get; set; }

        public string OutletName { get; set; }


        public string Email { get; set; }


        public string Title { get; set; }


        public string Password { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? OutletID { get; set; }

        public int? RoleID { get; set; }
    }
}

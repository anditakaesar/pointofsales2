﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL
{
    public class TransferStockViewModel
    {

        public TransferStockViewModel()
        {

        }

        public TransferStockViewModel(TransferStock obj)
        {
            this.ID = obj.ID;
            this.FromOutlet = obj.FromOutlet;
            this.ToOutlet = obj.ToOutlet;
            this.Note = obj.Note;
            this.CreatedBy = obj.CreatedBy;
            this.CreatedOn = obj.CreatedOn;
            this.ModifiedBy = obj.ModifiedBy;
            this.ModifiedOn = obj.ModifiedOn;
            this.NamaOutletTo = OutletDAL.GetOutletVMById((int)obj.ToOutlet).OutletName;
            this.NamaOutletFrom = OutletDAL.GetOutletVMById((int)obj.FromOutlet).OutletName;
        }

        public int ID { get; set; }

        public int? FromOutlet { get; set; }

        public int? ToOutlet { get; set; }

        public string Note { get; set; }

        public String NamaOutletTo { get; set; }

        public String NamaOutletFrom { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}

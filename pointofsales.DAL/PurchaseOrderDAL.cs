﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;
using pointofsales.DAL.ViewModel;

namespace pointofsales.DAL
{
    public class PurchaseOrderDAL
    {

        public static List<GetAllVM> GetData()
        {
            List<GetAllVM> result = new List<GetAllVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from tpo in db.PurchaseOrder
                         join ms in db.Supplier on tpo.SupplierID equals ms.ID
                         join mpos in db.PurchaseOrderStatus on tpo.StatusID equals mpos.ID
                         join tpod in db.PurchaseOrderDetail on tpo.ID equals tpod.PurchaseOrderID
                         select new { CreatedOn = tpo.CreatedOn, Name = ms.Name, OrderNo = tpo.OrderNo, SubTotal = tpod.SubTotal, StatusName = mpos.StatusName }).ToList().Select(x => new GetAllVM(){ CreatedOn = x.CreatedOn, Name = x.Name, OrderNo = x.OrderNo, SubTotal = x.SubTotal, StatusName = x.StatusName}).ToList();
            }
            return result;
        }
        public static List<GetAllVM> GetDataBySearch(string search, int id)
        {
            List<GetAllVM> result = new List<GetAllVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from tpo in db.PurchaseOrder
                          join ms in db.Supplier on tpo.SupplierID equals ms.ID
                          join mpos in db.PurchaseOrderStatus on tpo.StatusID equals mpos.ID
                          join tpod in db.PurchaseOrderDetail on tpo.ID equals tpod.PurchaseOrderID
                          join otl in db.Outlet on tpo.OutletID equals otl.ID
                          where otl.ID.Equals(id) && ms.Name.Contains(search)
                          select new { CreatedOn = tpo.CreatedOn, Name = ms.Name, OrderNo = tpo.OrderNo, SubTotal = tpod.SubTotal, StatusName = mpos.StatusName }).ToList().Select(x => new GetAllVM() { CreatedOn = x.CreatedOn, Name = x.Name, OrderNo = x.OrderNo, SubTotal = x.SubTotal, StatusName = x.StatusName }).ToList();
            }
            return result;
        }
        //public static List<Outlet> GetOutlet()
        //{
        //    List<Outlet> result = new List<Outlet>();
        //    using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //    {
        //        result = (from otl in db.Outlet
        //                  select otl).ToList();
        //    }
        //    return result;
        //}
        public static List<Outlet> GetOutletById(int id)
        {
            List<Outlet> result = new List<Outlet>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                //Employee staf = db.Employee.FirstOrDefault(x => x.ID == id);
                //int otlid = int.Parse(staf.OutletID);
                result = (from otl in db.Outlet
                          where otl.ID.Equals(id)
                          select otl).ToList();
            }
            return result;
        }
        public static List<Outlet> GetOutletByPOId(int id)
        {
            List<Outlet> result = new List<Outlet>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                PurchaseOrder po = db.PurchaseOrder.FirstOrDefault(p => p.ID == id);
                // hasil dari po.OutletID berupa int?, sehingga perlu di convert menjadi int sesuai dengan tipe data Outlet ID
                var otlid = po.OutletID.GetValueOrDefault();
                result = (from otl in db.Outlet
                          where otl.ID.Equals(otlid)
                          select otl).ToList();
            }
            return result;
        }
        public static List<Supplier> GetSupplier()
        {
            List<Supplier> result = new List<Supplier>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from sup in db.Supplier
                          select sup).ToList();
            }
            return result;
        }
        public static List<PurchaseOrderStatus> GetStatus()
        {
            List<PurchaseOrderStatus> result = new List<PurchaseOrderStatus>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from stat in db.PurchaseOrderStatus
                          select stat).ToList();
            }
            return result;
        }
        public static List<Supplier> GetSupplierById(int id)
        {
            // ID didapat dari ID tabel supplier
            List<Supplier> result = new List<Supplier>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                //result = db.Supplier.FirstOrDefault(x => x.ID == id);
                result = (from sup in db.Supplier
                          where sup.ID.Equals(id)
                          select sup).ToList();
            }
            return result;
        }
        public static List<Supplier> GetSupplierById2(int id)
        {
            // ID diarahkan ke ID purchase order lalu dicari supplier ID berdasar  PO
            List<Supplier> result = new List<Supplier>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                PurchaseOrder po = db.PurchaseOrder.FirstOrDefault(x => x.ID == id);
                var supid = po.SupplierID.GetValueOrDefault();
                result = (from sup in db.Supplier
                          where sup.ID.Equals(po.SupplierID)
                          select sup).ToList();
            }
            return result;
        }
        // sudah pake VM
        public static List<StatusVM> GetStatusById(int id)
        {
            List<StatusVM> result = new List<StatusVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from tpo in db.PurchaseOrder
                          join ms in db.Supplier on tpo.SupplierID equals ms.ID
                          join mpos in db.PurchaseOrderStatus on tpo.StatusID equals mpos.ID
                          join tpod in db.PurchaseOrderDetail on tpo.ID equals tpod.PurchaseOrderID
                          where tpo.StatusID.Value.Equals(id)
                          select new { CreatedOn = tpo.CreatedOn, Name = ms.Name, OrderNo = tpo.OrderNo, SubTotal = tpod.SubTotal, StatusName = mpos.StatusName }).ToList().Select(x => new StatusVM() { CreatedOn = x.CreatedOn, Name = x.Name, OrderNo = x.OrderNo, SubTotal = x.SubTotal, StatusName = x.StatusName }).ToList();
            }
            return result;
        }
        public static List<PurchaseOrder> GetPOById(int id)
        {
            List<PurchaseOrder> result = new List<PurchaseOrder>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from po in db.PurchaseOrder
                          where po.ID.Equals(id)
                          select po).ToList();
            }
            return result;
        }
        public static List<PurchaseOrderDetail> GetPODetailById(int id)
        {
            List<PurchaseOrderDetail> result = new List<PurchaseOrderDetail>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from pod in db.PurchaseOrderDetail
                          where pod.ID.Equals(id)
                          select pod).ToList();
            }
            return result;
        }
        public static List<PurchaseOrderHistory> GetPOHistoryById(int id)
        {
            List<PurchaseOrderHistory> result = new List<PurchaseOrderHistory>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from poh in db.PurchaseOrderHistory
                          where poh.ID.Equals(id)
                          select poh).ToList();
            }
            return result;
        }
        // sudah pake GetAllVM
        public static List<GetAllVM> GetAllId(int id)
        {
            List<GetAllVM> result = new List<GetAllVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = ( from po in db.PurchaseOrder
                           join otl in db.Outlet on po.OutletID equals otl.ID
                           join sup in db.Supplier on po.SupplierID equals sup.ID
                           join podd in db.PurchaseOrderDetail on po.ID equals podd.PurchaseOrderID 
                           join pohh in db.PurchaseOrderHistory on po.ID equals pohh.PurchaseOrderID
                           where po.ID.Equals(id)
                           select new { DetailID = podd.ID, HistoryID = pohh.ID, SupplierID = sup.ID, OutletID = otl.ID}).ToList().Select(x=> new GetAllVM(){ DetailID = x.DetailID, HistoryID = x.HistoryID, SupplierID = x.SupplierID, OutletID = x.OutletID}).ToList();                        
            }
            return result;
        }
        // pakai confirm VM
        public static List<ConfirmVM> GetDetailConfirm(int id)
        {
            List<ConfirmVM> result = new List<ConfirmVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from po in db.PurchaseOrder
                          join otl in db.Outlet on po.OutletID equals otl.ID
                          join staf in db.Employee on po.CreatedBy equals staf.ID
                          where po.ID.Equals(id)
                          select new { Notes = po.Notes, OrderNo = po.OrderNo, CreatedOn = po.CreatedOn, CreatedBy = staf.FirstName, Email = staf.Email, OtlName = otl.OutletName, OtlPhone = otl.Phone, OtlAddress = otl.Address }).ToList().Select(x => new ConfirmVM() {Notes = x.Notes, OrderNo = x.OrderNo, CreatedOn = x.CreatedOn, CreatedBy = x.CreatedBy, Email = x.Email, OtlName = x.OtlName, OtlPhone = x.OtlPhone, OtlAddress = x.OtlAddress }).ToList();
            }
            return result;
        }
        // summary sudah pake SummaryVM
        public static List<SummaryVM> GetSum(int id)
        {
            List<SummaryVM> result = new List<SummaryVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from p in db.ItemInventory
                          join itemvar in db.ItemVariant on p.VariantID equals itemvar.ID
                          join item in db.Item on itemvar.ItemID equals item.ID
                          join cat in db.Category on item.CategoryID equals cat.ID
                          join otl in db.Outlet on itemvar.OutletID equals otl.ID
                          where otl.ID.Equals(id)
                          select new { ItemName = item.Name, VariantName = itemvar.VariantName, CategoryName = cat.Name, Beginning = p.Beginning, PurchaseOrder = p.PurchaseOrder, Sales = p.Sales, Adjusment = p.Adjusment, Transfer = p.Transfer, Ending = p.Ending }).ToList().Select(x => new SummaryVM() { ItemName = x.ItemName, VariantName = x.VariantName, CategoryName = x.CategoryName, Beginning = x.Beginning, PurchaseOrder = x.PurchaseOrder, Sales = x.Sales, Adjusment = x.Adjusment, Transfer = x.Transfer, Ending = x.Ending }).ToList();
            }
            return result;
        }
        public static List<SummaryVM> GetSumBySearch(string search, int id)
        {
            List<SummaryVM> result = new List<SummaryVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from p in db.ItemInventory
                          join itemvar in db.ItemVariant on p.VariantID equals itemvar.ID
                          join item in db.Item on itemvar.ItemID equals item.ID
                          join cat in db.Category on item.CategoryID equals cat.ID
                          join otl in db.Outlet on itemvar.OutletID equals otl.ID
                          where otl.ID.Equals(id) && itemvar.VariantName.Contains(search)
                          select new { ItemName = item.Name, VariantName = itemvar.VariantName, CategoryName = cat.Name, Beginning = p.Beginning, PurchaseOrder = p.PurchaseOrder, Sales = p.Sales, Adjusment = p.Adjusment, Transfer = p.Transfer, Ending = p.Ending }).ToList().Select(x => new SummaryVM() { ItemName = x.ItemName, VariantName = x.VariantName, CategoryName = x.CategoryName, Beginning = x.Beginning, PurchaseOrder = x.PurchaseOrder, Sales = x.Sales, Adjusment = x.Adjusment, Transfer = x.Transfer, Ending = x.Ending }).ToList();
            }
            return result;
        }
        // Item VM
        public static List<ItemVM> GetForAddPO()
        {
            List<ItemVM> result = new List<ItemVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from iteminv in db.ItemInventory
                          join itemvar in db.ItemVariant on iteminv.VariantID equals itemvar.ID
                          join item in db.Item on itemvar.ItemID equals item.ID
                          select new { NamaItem = item.Name, NamaVariant = itemvar.VariantName, Stock = iteminv.Ending }).ToList().Select(x => new ItemVM() { NamaItem = x.NamaItem, NamaVariant = x.NamaVariant, Stock = x.Stock }).ToList();
            }
            return result;
        }
        public static List<ItemVM> GetItemSearch(string keySearch, int id)
        {
            List<ItemVM> result = new List<ItemVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {

                result = (from iteminv in db.ItemInventory
                          join itemvar in db.ItemVariant on iteminv.VariantID equals itemvar.ID
                          join item in db.Item on itemvar.ItemID equals item.ID
                          join otl in db.Outlet on itemvar.OutletID equals otl.ID
                          where itemvar.VariantName.Contains(keySearch) && otl.ID.Equals(id)
                          select new { IdItem = item.ID, IdVariant = itemvar.ID, NamaItem = item.Name, NamaVariant = itemvar.VariantName, Stock = iteminv.Ending }).ToList().Select(x => new ItemVM() { IdItem = x.IdItem, IdVariant = x.IdVariant, NamaItem = x.NamaItem, NamaVariant = x.NamaVariant, Stock = x.Stock }).ToList();
            }
            return result;
        }
        public static List<ItemVM> GetItemById(int id)
        {
            List<ItemVM> result = new List<ItemVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from iteminv in db.ItemInventory
                          join itemvar in db.ItemVariant on iteminv.VariantID equals itemvar.ID
                          join item in db.Item on itemvar.ItemID equals item.ID
                          where itemvar.ID.Equals(id)
                          select new { IdItem = item.ID, IdVariant = itemvar.ID, NamaItem = item.Name, NamaVariant = itemvar.VariantName, Stock = iteminv.Ending }).ToList().Select(x => new ItemVM() { IdItem = x.IdItem, IdVariant = x.IdVariant, NamaItem = x.NamaItem, NamaVariant = x.NamaVariant, Stock = x.Stock }).ToList();
            }
            return result;
        }
        public static List<ItemVM> GetItemByPOId(int id)
        {
            List<ItemVM> result = new List<ItemVM>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from po in db.PurchaseOrder
                          join pod in db.PurchaseOrderDetail on po.ID equals pod.PurchaseOrderID
                          join itemvar in db.ItemVariant on pod.VariantID equals itemvar.ID
                          join item in db.Item on itemvar.ItemID equals item.ID
                          join iteminv in db.ItemInventory on itemvar.ID equals iteminv.VariantID
                          where po.ID.Equals(id)
                          select new { IdItem = item.ID, IdVariant = itemvar.ID, NamaItem = item.Name, NamaVariant = itemvar.VariantName, Stock = iteminv.Ending, Order = pod.Quantity, UnitCost = pod.UnitCost, Total = pod.SubTotal }).ToList().Select(x => new ItemVM() { IdItem = x.IdItem, IdVariant = x.IdVariant, NamaItem = x.NamaItem, NamaVariant = x.NamaVariant, Stock = x.Stock, Order = x.Order, UnitCost = x.UnitCost, Total = x.Total }).ToList();
            }
            return result;
        }
        public static int AddTrxPo(PurchaseOrderVM add)
        {
            Random orderno = new Random();
            int result = 0;
            int lengthOrder = 6;
            const string chars = "0123456789";
            try
            {
                var addPO = new PurchaseOrder();
                
                addPO.ID = add.ID;
                addPO.OutletID = add.OutletID;
                addPO.SupplierID = add.SupplierID;
                addPO.Notes = add.Notes;
                addPO.CreatedOn = DateTime.Now;
                addPO.OrderNo = new string(Enumerable.Repeat(chars, lengthOrder).Select(s => s[random.Next(s.Length)]).ToArray());
                // asumsi sebelum mendapat data user dari login
                addPO.CreatedBy = add.CreatedBy;

                using (PointOfSalesDBContext db = new PointOfSalesDBContext())
                {       
                    
                    db.PurchaseOrder.Add(addPO);

                    var addPOdetail = new PurchaseOrderDetail();
                    addPOdetail.VariantID = add.IdVariant;
                    addPOdetail.Quantity = add.Quantity;
                    addPOdetail.UnitCost = add.UnitCost;
                    addPOdetail.SubTotal = add.SubTotal;
                    addPOdetail.PurchaseOrderID = add.ID;
                    addPOdetail.CreatedOn = DateTime.Now;
                    addPOdetail.CreatedBy = add.CreatedBy;

                    db.PurchaseOrderDetail.Add(addPOdetail);

                    var addHistory = new PurchaseOrderHistory();
                    addHistory.PurchaseOrderID = addPO.ID;
                    addHistory.CreatedOn = DateTime.Now;
                    addPO.CreatedBy = add.CreatedBy;

                    db.PurchaseOrderHistory.Add(addHistory);
                    db.SaveChanges();
                }
                result = addPO.ID;
            }
            catch (Exception)
            {
                result = 0;
            }
            return result;
        }
        public static bool EditTrxPO(PurchaseOrderVM add)
        {
            bool result = false;
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                PurchaseOrder editPO = db.PurchaseOrder.FirstOrDefault(p => p.ID == add.ID);

                if (editPO != null)
                {
                    editPO.ID = add.ID;
                    editPO.OutletID = add.OutletID;
                    editPO.SupplierID = add.SupplierID;
                    editPO.Notes = add.Notes;
                    editPO.ModifiedOn = DateTime.Now;
                    // asumsi sebelum mendapat data user dari login
                    editPO.ModifiedBy = add.ModifiedBy;

                    try
                    {
                        PurchaseOrderDetail editPOdetail = db.PurchaseOrderDetail.FirstOrDefault(d => d.PurchaseOrderID == editPO.ID);
                        editPOdetail.VariantID = add.IdVariant;
                        editPOdetail.Quantity = add.Quantity;
                        editPOdetail.UnitCost = add.UnitCost;
                        editPOdetail.SubTotal = add.SubTotal;
                        editPOdetail.ModifiedOn = DateTime.Now;
                        editPOdetail.ModifiedBy = 1;

                        PurchaseOrderHistory editHistory = new PurchaseOrderHistory();
                        editHistory.PurchaseOrderID = editPO.ID;
                        editHistory.ModifiedOn = DateTime.Now;
                        editHistory.ModifiedBy = 1;

                        db.Entry(editPO).State = System.Data.Entity.EntityState.Modified;
                        db.Entry(editPOdetail).State = System.Data.Entity.EntityState.Modified;
                        db.PurchaseOrderHistory.Add(editHistory);
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        
                        //throw;
                    }
                }
            }
            return result;
        }
        public static bool UpdateStatusPO(int id, int stat, int order)
        {
            bool ret = false;
            try
            {
                using (PointOfSalesDBContext db = new PointOfSalesDBContext())
                {
                    PurchaseOrder result = db.PurchaseOrder.FirstOrDefault(po => po.ID == id);

                    PurchaseOrderDetail detail = db.PurchaseOrderDetail.FirstOrDefault(pod => pod.PurchaseOrderID == result.ID);
                    ItemVariant variant = db.ItemVariant.FirstOrDefault(itemvar => itemvar.ID == detail.VariantID);                    
                    ItemInventory addOrder = db.ItemInventory.FirstOrDefault(iteminv => iteminv.VariantID == variant.ID);
                    
                    PurchaseOrderHistory history = new PurchaseOrderHistory();
                    history.PurchaseOrderID = result.ID;
                    history.StatusID = stat;

                    addOrder.PurchaseOrder = order;
                    addOrder.Beginning = addOrder.Ending;
                    addOrder.Ending = addOrder.Beginning + addOrder.Transfer + addOrder.Sales + addOrder.PurchaseOrder;
                    addOrder.ModifiedOn = DateTime.Now;
                    result.StatusID = stat;
                    result.ModifiedOn = DateTime.Now;

                    db.Entry(addOrder).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(result).State = System.Data.Entity.EntityState.Modified;

                    db.PurchaseOrderHistory.Add(history);
                    db.SaveChanges();
                }
                ret = true;
            }
            catch (Exception)
            {  
                ret = false;
            }
            return ret;
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.DAL;
using pointofsales.Model;

namespace pointofsales.DAL
{
    public class PaymentDAL
    {
       
        public static bool SavePay(Payment Payment, List<PaymentDetail> PaymentDetails)
        {
            bool result = false;
            try
            {
                using (PointOfSalesDBContext db = new PointOfSalesDBContext())
                {
                    Payment.CreatedOn = DateTime.Now;
                    Payment.ModifiedOn = DateTime.Now;
                    

                    foreach (PaymentDetail PD in Payment.PaymentDetails)
                    {
                        PD.CreatedOn = DateTime.Now;
                        PD.ModifiedOn = DateTime.Now;
                        ItemInventory Inve = db.ItemInventory.Where(x => x.VariantID == PD.VariantID).SingleOrDefault();
                        Inve.Sales = PD.Quantity;
                        Inve.Ending = Inve.Ending - PD.Quantity;
                        Inve.ModifiedOn = DateTime.Now;
                        db.Entry(Inve).State = System.Data.Entity.EntityState.Modified;
                    }

                    db.Payment.Add(Payment);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}

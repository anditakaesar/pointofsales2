﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;

namespace pointofsales.DAL
{
    public class CostemerDAL
    {
        //get list customer
        public static List<Customer> GetCustomer(string Search)
        {
            List<Customer> result = new List<Customer>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Customer.Where(x => x.CustomerName.Contains(Search)).ToList();
            }
            return result;
        }

        //Save Custemer Baru
        public static bool SaveCustemer(Customer Customer)
        {
            var result = false;

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Customer.CreatedOn = DateTime.Now;
                Customer.ModifiedOn = DateTime.Now;

                try
                {
                    db.Customer.Add(Customer);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    result = false;
                }
            }
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;
using pointofsales.DAL.ViewModel;

namespace pointofsales.DAL
{
    public class SupplierDAL
    {
        // Get Semua Supplier
        public static List<Supplier> GetSupplier()
        {
            List<Supplier> result = new List<Supplier>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Supplier.ToList();
            }

            return result;
        }

        // Get Supplier by SupplierID
        public static Supplier GetSupplier(int supID)
        {
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                return db.Supplier.FirstOrDefault(supplier => supplier.ID == supID);
            }

        }

        // Search Supplier
        public static List<Supplier> SearchSupplier(string key)
        {
            List<Supplier> result = new List<Supplier>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Supplier.Where(sup => sup.Name.Contains(key)).ToList();
            }

            return result;
        }

        // Insert/Create/Add Supplier, tidak digabung dengan Update supplier
        public static bool InsertSupplier(Supplier supp)
        {
            bool result = false;

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                // Cek if supplier exist
                Supplier supExist = db.Supplier.FirstOrDefault(sup => sup.ID == supp.ID);

                if (supp != null && supExist == null)
                {
                    supp.CreatedOn = DateTime.Now;
                    supp.ModifiedOn = DateTime.Now;
                    db.Supplier.Add(supp);
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        // Update Supplier
        public static bool UpdateSupplier(Supplier supp)
        {
            bool result = false;

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Supplier SuppToUpdate = db.Supplier.FirstOrDefault(stu => stu.ID == supp.ID);
                // Set modified
                SuppToUpdate.ModifiedOn = DateTime.Now;
                SuppToUpdate.Name = supp.Name;
                SuppToUpdate.Phone = supp.Phone;
                SuppToUpdate.PostalCode = supp.PostalCode;
                SuppToUpdate.Address = supp.Address;
                SuppToUpdate.Email = supp.Email;

                SuppToUpdate.ModifiedBy = supp.ModifiedBy;
                SuppToUpdate.DistrictID = supp.DistrictID;
                SuppToUpdate.ModifiedBy = supp.ModifiedBy;

                try
                {
                    db.Entry(SuppToUpdate).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception) { }

            }

            return result;
        }

        // Delete/Hapus supplier
        public static bool DeleteSupplier(int supID)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                Supplier SuppToDelete = db.Supplier.FirstOrDefault(supp => supp.ID == supID);

                try
                {
                    db.Supplier.Remove(SuppToDelete);
                    db.SaveChanges();
                }
                catch (Exception) { }
            }

            return result;
        }

        // Inisiasi Supplier <-test only, disable after this
        //public static List<Supplier> InisiasiSupplier()
        //{
        //    var result = new List<Supplier>();

        //    using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //    {
        //        db.Supplier.Add(new Supplier
        //        {
        //            Name = "PT. Indofood",
        //            Address = "Jalan Industri",
        //            Phone = "021-111111",
        //            Email = "admin@indofood.co.id"
        //        });
        //        db.Supplier.Add(new Supplier
        //        {
        //            Name = "PT. Alfamart",
        //            Address = "Jalan Toko",
        //            Phone = "021-222222",
        //            Email = "admin@alfamart.co.id"
        //        });
        //        db.Supplier.Add(new Supplier
        //        {
        //            Name = "CV. Butik Batik",
        //            Address = "Jalan Sandang",
        //            Phone = "021-333333",
        //            Email = "admin@butikbatik.co.id"
        //        });

        //        try
        //        {
        //            db.SaveChanges();
        //            result = db.Supplier.ToList();
        //        }
        //        catch (Exception) {}

        //        return result;
        //    }
        //}

        // Supplier VM dengan lokasi
        public static List<SupplierViewModel> GetSupplierWithLocation()
        {
            List<SupplierViewModel> result = new List<SupplierViewModel>();

            using (var db = new PointOfSalesDBContext())
            {
                // list semua supplier
                List<Supplier> Suppliers = db.Supplier.ToList();
                // loop semua supplier menjadi view model
                foreach (var sup in Suppliers)
                {
                    result.Add(new SupplierViewModel(sup));
                }
            }

            return result;
        }

        public static List<SupplierViewModel> SearchSupplierVM(string key)
        {
            List<SupplierViewModel> result = new List<SupplierViewModel>();

            using (var db = new PointOfSalesDBContext())
            {
                // list semua supplier
                List<Supplier> Suppliers = db.Supplier.Where(supp => supp.Name.Contains(key)).ToList();
                // loop semua supplier menjadi view model
                foreach (var sup in Suppliers)
                {
                    result.Add(new SupplierViewModel(sup));
                }
            }

            return result;
        }

        public static SupplierViewModel GetSupplierVMById(int id)
        {
            Supplier supplier = new PointOfSalesDBContext().Supplier.FirstOrDefault(supp => supp.ID == id);

            return new SupplierViewModel(supplier);
        }
    }
}

﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL
{
    public class tempTransferStock
    {
        public int FromOutlet { get; set; }
        public int ToOutlet { get; set; }
        public string Note { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public List<Detail> Details { get; set; }

    }

    public class Detail
    {
        public Detail()
        {

        }

        public Detail(TransferStockDetail obj)
        {
            this.IDVariant = obj.VariantID;
            this.SKU = obj.SKU;
            this.CreatedBy = obj.CreatedBy;
            this.CreatedOn = obj.CreatedOn;
            this.IDTransferStock = obj.TransferStockID;
            this.InStock = ItemDAL.GetInventory((int)obj.VariantID).Ending;
            this.TrfQty = obj.Quantity;
            this.namaVariant = ItemDAL.GetVariant((int)obj.VariantID).VariantName;
        }
        public int? IDVariant { get; set; }
        public string SKU { get; set; }
        public int? TrfQty { get; set; }
        public int? InStock { get; set; }
        public int? IDTransferStock { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string namaVariant { get; set; }
    }
}

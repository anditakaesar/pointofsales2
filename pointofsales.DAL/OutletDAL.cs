﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;
using pointofsales.DAL.ViewModel;

namespace pointofsales.DAL
{
    public class OutletDAL
    {
        // Read
        public static List<Outlet> GetOutlet()
        {
            List<Outlet> result = new List<Outlet>();
            using (var db = new PointOfSalesDBContext())
            {
                result = db.Outlet.ToList();
            }
            return result;
        }

        // Read, Get Outlet by Id
        public static Outlet GetOutlet(int id)
        {
            using (var db = new PointOfSalesDBContext())
            {
                return db.Outlet.FirstOrDefault(outlet => outlet.ID == id);
            }
        }
        // Create
        public static bool InsertOutlet(Outlet outlet)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                if (outlet != null)
                {
                    outlet.ModifiedOn = DateTime.Now;
                    outlet.CreatedOn = DateTime.Now;

                    try
                    {
                        db.Outlet.Add(outlet);
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception) { }
                }
            }
            return result;
        }

        // Update
        public static bool UpdateOutlet(Outlet outlet)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                Outlet outLetToUpdate = db.Outlet.FirstOrDefault(o => o.ID == outlet.ID);
                if (outlet != null && outLetToUpdate != null)
                {
                    outLetToUpdate.ModifiedBy = outlet.ModifiedBy;
                    outLetToUpdate.OutletName = outlet.OutletName;
                    outLetToUpdate.Phone = outlet.Phone;
                    outLetToUpdate.PostalCode = outlet.PostalCode;
                    outLetToUpdate.Email = outlet.Email;
                    outLetToUpdate.DistrictID = outlet.DistrictID;
                    outLetToUpdate.Address = outlet.Address;

                    // Set Modified
                    outLetToUpdate.ModifiedOn = DateTime.Now;
                    try
                    {
                        db.Entry(outLetToUpdate).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception) { }
                }
            }
            return result;
        }

        // Delete, Real DELETE not a pseudo-delete
        public static bool DeleteOutlet(int outId)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                Outlet outletToDelete = db.Outlet.FirstOrDefault(o => o.ID == outId);
                try
                {
                    db.Outlet.Remove(outletToDelete);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception) { }
            }
            return result;
        }

        // Search
        public static List<Outlet> SearchOutlet(string key)
        {
            using (var db = new PointOfSalesDBContext())
            {
                return db.Outlet.Where(ou => ou.OutletName.Contains(key)).ToList();
            }
        }

        // Outlet View Model, Dibuat agar menampilkan District
        public static List<OutletViewModel> GetOutletWithLocation()
        {
            List<OutletViewModel> result = new List<OutletViewModel>();

            using (var db = new PointOfSalesDBContext())
            {
                // Ambil semua Outlet
                List<Outlet> Outlets = GetOutlet();

                foreach (var outlet in Outlets)
                {
                    // IDs daripada Lokasi
                    // dimasukkan pada try catch jaga2 jika null
                    try
                    {
                        int KecId = (int)outlet.DistrictID;

                        District Dist = LokasiDAL.GetKecamatanById(KecId);

                        int KotaId = db.Region
                            .FirstOrDefault(kot => kot.ID == Dist.RegionID)
                            .ID;

                        Region Reg = LokasiDAL.GetKotaById(KotaId);

                        int ProvId = db.Province
                            .FirstOrDefault(prov => prov.ID == Reg.ProvinceID)
                            .ID;
                        // View Model
                        OutletViewModel Ovm = new OutletViewModel
                        {
                            ID = outlet.ID,
                            Address = outlet.Address,
                            OutletName = outlet.OutletName,
                            CreatedBy = outlet.CreatedBy,
                            CreatedOn = outlet.CreatedOn,
                            DistrictID = outlet.DistrictID,
                            Email = outlet.Email,
                            ModifiedBy = outlet.ModifiedBy,
                            ModifiedOn = outlet.ModifiedOn,
                            Phone = outlet.Phone,
                            PostalCode = outlet.PostalCode,
                            Kecamatan = LokasiDAL.GetKecamatanById(KecId).DistrictName,
                            Kota = LokasiDAL.GetKotaById(KotaId).RegionName,
                            Provinsi = LokasiDAL.GetProvinsiById(ProvId).ProvinceName

                        };
                        result.Add(Ovm);
                    }
                    catch (Exception) { }
                }
            }
            return result;
        }

        public static OutletViewModel GetOutletVMById(int id)
        {

            using (var db = new PointOfSalesDBContext())
            {
                Outlet outlet = db.Outlet.FirstOrDefault(ot => ot.ID == id);

                // IDs daripada Lokasi
                int KecId = (int)outlet.DistrictID;

                District Dist = LokasiDAL.GetKecamatanById(KecId);

                int KotaId = db.Region
                    .FirstOrDefault(kot => kot.ID == Dist.RegionID)
                    .ID;

                Region Reg = LokasiDAL.GetKotaById(KotaId);

                int ProvId = db.Province
                    .FirstOrDefault(prov => prov.ID == Reg.ProvinceID)
                    .ID;

                // View Model RETURN
                return new OutletViewModel
                {
                    ID = outlet.ID,
                    Address = outlet.Address,
                    OutletName = outlet.OutletName,
                    CreatedBy = outlet.CreatedBy,
                    CreatedOn = outlet.CreatedOn,
                    DistrictID = outlet.DistrictID,
                    Email = outlet.Email,
                    ModifiedBy = outlet.ModifiedBy,
                    ModifiedOn = outlet.ModifiedOn,
                    Phone = outlet.Phone,
                    PostalCode = outlet.PostalCode,
                    Kecamatan = LokasiDAL.GetKecamatanById(KecId).DistrictName,
                    Kota = LokasiDAL.GetKotaById(KotaId).RegionName,
                    Provinsi = LokasiDAL.GetProvinsiById(ProvId).ProvinceName

                };
            }
        }

        public static List<OutletViewModel> SearchOutletVM(string key)
        {
            List<OutletViewModel> result = new List<OutletViewModel>();

            using (var db = new PointOfSalesDBContext())
            {
                // Ambil semua Outlet dengan searchkey
                List<Outlet> Outlets = SearchOutlet(key);

                foreach (var outlet in Outlets)
                {
                    // IDs daripada Lokasi
                    int KecId = (int)outlet.DistrictID;

                    District Dist = LokasiDAL.GetKecamatanById(KecId);

                    int KotaId = db.Region
                        .FirstOrDefault(kot => kot.ID == Dist.RegionID)
                        .ID;

                    Region Reg = LokasiDAL.GetKotaById(KotaId);

                    int ProvId = db.Province
                        .FirstOrDefault(prov => prov.ID == Reg.ProvinceID)
                        .ID;

                    // View Model
                    OutletViewModel Ovm = new OutletViewModel
                    {
                        ID = outlet.ID,
                        Address = outlet.Address,
                        OutletName = outlet.OutletName,
                        CreatedBy = outlet.CreatedBy,
                        CreatedOn = outlet.CreatedOn,
                        DistrictID = outlet.DistrictID,
                        Email = outlet.Email,
                        ModifiedBy = outlet.ModifiedBy,
                        ModifiedOn = outlet.ModifiedOn,
                        Phone = outlet.Phone,
                        PostalCode = outlet.PostalCode,
                        Kecamatan = LokasiDAL.GetKecamatanById(KecId).DistrictName,
                        Kota = LokasiDAL.GetKotaById(KotaId).RegionName,
                        Provinsi = LokasiDAL.GetProvinsiById(ProvId).ProvinceName

                    };
                    result.Add(Ovm);
                }

            }
            return result;
        }

        // method untuk membuat item ketika outlet juga dibuat
        public static bool InsertOutletWithItem(Outlet outlet)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                // buat outlet
                if (outlet != null)
                {
                    outlet.ModifiedOn = DateTime.Now;
                    outlet.CreatedOn = DateTime.Now;
                    

                    db.Outlet.Add(outlet);

                }

                // Get semua item
                List<Item> items = db.Item.ToList();
                // Loop semua item
                foreach (var item in items)
                {
                    // get semua variant
                    //List<ItemVariant> variants = db.ItemVariant.Where(variant => variant.ItemID == item.ID).ToList();
                    List<ItemVariant> variants = db.ItemVariant.Where(variant => variant.ItemID == item.ID && variant.OutletID==1).ToList();
                    
                    // loop semua variant
                    foreach (var variant in variants)
                    {
                        ItemVariant newVariant = new ItemVariant
                        {
                            ItemID = item.ID,
                            // error probability
                            OutletID = outlet.ID,
                            VariantName = variant.VariantName,
                            SKU = variant.SKU,
                            Price = variant.Price,
                            CreatedBy = outlet.CreatedBy,
                            CreatedOn = DateTime.Now,
                            ItemInventory = new List<ItemInventory>()

                        };
                        // insert pada item inventory
                        ItemInventory inv = new ItemInventory
                        {
                            Ending = 0,
                            Beginning = 0,
                            AlertAt = 1,
                            CreatedOn = DateTime.Now,
                            ModifiedOn = DateTime.Now,
                            CreatedBy = outlet.CreatedBy
                        };

                        // add inventory pada Variant
                        newVariant.ItemInventory.Add(inv);
                        db.ItemVariant.Add(newVariant);
                    }
                }

                // save change
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception) { }

            }

            return result;
        }
    }
}

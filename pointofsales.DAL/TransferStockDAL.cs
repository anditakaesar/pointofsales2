﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pointofsales.DAL
{
    public class TransferStockDAL
    {
        public static List<Outlet> GetAllOutlet()
        {
            var result = new List<Outlet>();
            using (var db = new PointOfSalesDBContext())
            {
                result = db.Outlet.ToList();
            }
            return result;
        }

        public static bool SaveTransferStock(tempTransferStock obj)
        {
            var result = false;
            using (var db = new PointOfSalesDBContext())
            {
                TransferStock tsAdd = new TransferStock();
                tsAdd.FromOutlet = obj.FromOutlet;
                tsAdd.ToOutlet = obj.ToOutlet;
                tsAdd.Note = obj.Note;
                tsAdd.CreatedBy = obj.CreatedBy;
                tsAdd.ModifiedBy = obj.ModifiedBy;
                tsAdd.CreatedOn = DateTime.Now;
                tsAdd.ModifiedOn = DateTime.Now;

                List<TransferStockDetail> tsDetail = new List<TransferStockDetail>();

                foreach (var item in obj.Details)
                {
                    tsDetail.Add(new TransferStockDetail
                    {
                        VariantID = item.IDVariant,
                        InStock = item.InStock - item.TrfQty,
                        Quantity = item.TrfQty,
                        TransferStockID = tsAdd.ID,
                        SKU = item.SKU,
                        CreatedBy = obj.CreatedBy,
                        ModifiedBy = obj.ModifiedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                    });
                }
                try
                {
                    db.TransferStock.Add(tsAdd);
                    foreach (var item in tsDetail)
                    {
                        db.TransferStockDetail.Add(item);
                    }
                    if (UpdateInventory(obj.Details, obj.FromOutlet, obj.ToOutlet) == true)
                    {
                        db.SaveChanges();
                        result = true;
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return result;
        }

        public static bool UpdateInventory(List<Detail> details, int IDOutletFrom, int IDOutletTo)
        {
            var result = false;
            using (var db = new PointOfSalesDBContext())
            {
                foreach (Detail inventory in details)
                {
                    //update inventory outlet from
                    ItemVariant tempVariant = db.ItemVariant.FirstOrDefault(x => x.SKU == inventory.SKU && x.OutletID == IDOutletFrom);
                    ItemInventory inventoryToUpdate = db.ItemInventory.FirstOrDefault(x => x.VariantID == tempVariant.ID);
                    if (inventoryToUpdate != null)
                    {
                        if (inventoryToUpdate.Transfer == null)
                        {
                            inventoryToUpdate.Transfer = 0;
                        }
                        inventoryToUpdate.Transfer = inventoryToUpdate.Transfer - inventory.TrfQty;
                        inventoryToUpdate.Ending = inventoryToUpdate.Ending - inventory.TrfQty;
                        inventoryToUpdate.ModifiedBy = inventory.ModifiedBy;
                        inventoryToUpdate.ModifiedOn = DateTime.Now;
                    }

                    //update inventory outlet to
                    ItemVariant tempVariant2 = db.ItemVariant.FirstOrDefault(x => x.SKU == inventory.SKU && x.OutletID == IDOutletTo);
                    ItemInventory inventoryToUpdate2 = db.ItemInventory.FirstOrDefault(x => x.VariantID == tempVariant2.ID);
                    if (tempVariant2 != null)
                    {
                        if (inventoryToUpdate2.Transfer == null)
                        {
                            inventoryToUpdate2.Transfer = 0;
                        }
                        inventoryToUpdate2.Transfer = inventoryToUpdate2.Transfer + inventory.TrfQty;
                        inventoryToUpdate2.Ending = inventoryToUpdate2.Ending + inventory.TrfQty;
                        inventoryToUpdate2.ModifiedBy = inventory.ModifiedBy;
                        inventoryToUpdate2.ModifiedOn = DateTime.Now;
                    }
                }
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return result;
            }
        }

        public static List<TransferStockViewModel> GetAllTransferStockByIdOutlet(int id) {
            var result = new List<TransferStockViewModel>();
            using( var db = new PointOfSalesDBContext()){
                var result2 = db.TransferStock.Where(x=>x.FromOutlet==id).ToList();
                foreach (var item in result2)
                {
                    result.Add(new TransferStockViewModel(item));
                }
            }
            return result;
        }

        public static List<Detail> GetAllTransferStockDetailById(int id) {
            var result = new List<Detail>();
            using(var db= new PointOfSalesDBContext()){
                var result2 = db.TransferStockDetail.Where(x=>x.TransferStockID == id).ToList();
                foreach (var item in result2)
                {
                    result.Add(new Detail(item));
                }
            }
            return result;
        }
    }
}

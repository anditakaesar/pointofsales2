﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace pointofsales.DAL
{
    public class InisiasiDAL
    {
        public static List<Category> Inisiasi()
        {
            List<Category> result = new List<Category>();

            using (var db = new PointOfSalesDBContext())
            {
                Category cat = new Category
                {
                    //Name = "Elektronik", Edit Menjadi Uncategorized
                    Name = "Uncategorized",
                    CreatedOn = DateTime.Now,
                    Enabled = true
                };

                try
                {
                    db.Category.Add(cat);
                    // save
                    db.SaveChanges();
                    result = db.Category.ToList();

                    // inisiasi Role
                    InisiasiRole();
                    // Inisiasi Outlet
                    InisiasiOutlet();
                    // Inisiasi User
                    InisiasiUser();
                }
                catch (Exception) { }
            }

            return result;
        }

        public static void InisiasiRole()
        {
            using (var db = new PointOfSalesDBContext())
            {
                Role BackOffice = new Role
                {
                    ID = 1,
                    RoleName = "BackOffice",
                    Description = "Everything except payment and staff?",
                    CreatedOn = DateTime.Now
                };
                Role App = new Role
                {
                    ID = 2,
                    RoleName = "App",
                    Description = "Payment/Sales (Kasir)",
                    CreatedOn = DateTime.Now
                };
                Role AppAndBackOffice = new Role
                {
                    ID = 3,
                    RoleName = "App & BackOffice",
                    Description = "Super admin",
                    CreatedOn = DateTime.Now
                };
                db.Role.Add(BackOffice);
                db.Role.Add(App);
                db.Role.Add(AppAndBackOffice);

                // Save
                db.SaveChanges();
            }
        }

        //public static List<Province> InisiasiLokasi(FileStream file)
        //{
        //    List<Province> result = new List<Province>();
        //    var reader = new StreamReader(file);
        //    using (var db = new PointOfSalesDBContext())
        //    {
        //        while (!reader.EndOfStream)
        //        {
        //            var line = reader.ReadLine();
        //            var values = line.Split(',');

        //            db.Province.Add(new Province
        //            {
        //                ID = int.Parse(values[0]),
        //                ProvinceName = values[1]
        //            });

        //            try
        //            {
        //                db.SaveChanges();
        //            }
        //            catch (Exception) { }
        //        }
        //        result = db.Province.ToList();
        //    }
        //    return result;
        //}
        // BAD OPTION, ID generated secara otomatis harus ganti Model secara manual
        // [DatabaseGenerated(DatabaseGeneratedOption.None)] pada ID model

        public static void InisiasiOutlet()
        {
            using (var db = new PointOfSalesDBContext())
            {
                Outlet outlet = new Outlet
                {
                    OutletName = "Default Outlet",
                    Address = "Jalan Mampang Prapatan",
                    Email = "admin@default.com",
                    Phone = "022-12345",
                    PostalCode = "44444",
                    DistrictID = 1101010,
                    CreatedOn = DateTime.Now,
                    CreatedBy = 1
                };

                db.Outlet.Add(outlet);
                db.SaveChanges();
            }
        }

        public static void InisiasiUser()
        {
            using (var db = new PointOfSalesDBContext())
            {
                Employee admin = new Employee
                {
                    FirstName = "admin",
                    LastName = "default",
                    Email = "admin@admin.com",
                    CreatedOn = DateTime.Now,
                    Password = "admin",
                    OutletID = 1,
                    RoleID = 1,
                    Title = "Mr."
                };

                db.Employee.Add(admin);
                db.SaveChanges();
            }
        }
    }
}

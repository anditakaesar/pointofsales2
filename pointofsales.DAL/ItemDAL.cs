﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pointofsales.Model;
using pointofsales.DAL.ItemModel;

namespace pointofsales.DAL
{
    public class ItemDAL
    {
        public int ID { get; set; }
        public string ItemName { get; set; }
        public int? VarItemId { get; set; }
        public int CatID { get; set; }
        public int varID { get; set; }
        public string CatName { get; set; }
        public string SKU { get; set; }
        public decimal? Price { get; set; }
        public int? AlertAt { get; set; }
        public int? Stock { get; set; }
        public string Variant { get; set; }
        public int? IDOutlet { get; set; }
        public List<IsiItem> IsiItem { get; set; }
        public List<IsiItem> Inventory { get; set; }

        //mengambil seluruh item saja, id dan nama nya saja. buatan anthony
        public static List<ItemDAL> GetAllItem()
        {
            List<ItemDAL> InnerJoinLayout = new List<ItemDAL>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {

                InnerJoinLayout = (from It in db.Item
                                   select new ItemDAL
                                   {
                                       ID = It.ID,
                                       ItemName = It.Name,
                                   }).ToList();
            }
            return InnerJoinLayout;
        }

        //Outlet Outlet anouncenc
        ////Mengambil data dari database ( table item, variant, inventory) dimasukan kedalam itemdal viewmodel
        //public static List<ItemDAL> GetLayoutItem(string key, int IDOutlet)
        //{
        //    List<ItemDAL> InnerJoinLayout = new List<ItemDAL>();

        //    using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //    {

        //        InnerJoinLayout = (from It in db.Item
        //                           join cat in db.Category on It.CategoryID equals cat.ID
        //                           join vart in db.ItemVariant on It.ID equals vart.ItemID
        //                           join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
        //                           where It.Name.Contains(key) && vart.OutletID == IDOutlet
        //                           select new ItemDAL
        //                           {
        //                               ID = It.ID,
        //                               varID = vart.ID,
        //                               Variant = vart.VariantName,
        //                               CatID = cat.ID,
        //                               ItemName = It.Name,
        //                               CatName = cat.Name,
        //                               Price = vart.Price,
        //                               Stock = Inv.Ending,
        //                               AlertAt = Inv.AlertAt,
        //                               SKU = vart.SKU,
        //                           }).ToList();
        //    }
        //    return InnerJoinLayout;
        //}
        ////Mengambil data dari database ( table item, variant, inventory) by categori DDL
        //public static List<ItemDAL> GetLayoutItembyCat(string key,int IDOutlet)
        //{
        //    List<ItemDAL> InnerJoinLayout = new List<ItemDAL>();

        //    using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //    {

        //        InnerJoinLayout = (from It in db.Item
        //                           join cat in db.Category on It.CategoryID equals cat.ID
        //                           join vart in db.ItemVariant on It.ID equals vart.ItemID
        //                           join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
        //                           where cat.Name.Contains(key) && vart.OutletID == IDOutlet
        //                           select new ItemDAL
        //                           {
        //                               ID = It.ID,
        //                               varID = vart.ID,
        //                               Variant = vart.VariantName,
        //                               CatID = cat.ID,
        //                               ItemName = It.Name,
        //                               CatName = cat.Name,
        //                               Price = vart.Price,
        //                               Stock = Inv.Ending,
        //                               AlertAt = Inv.AlertAt,
        //                               SKU = vart.SKU
        //                           }).ToList();
        //    }
        //    return InnerJoinLayout;
        //}
        ////Mengambil data dari database ( table item, variant, inventory) by ItemID
        //public static List<ItemDAL> GetDataByID(int ID, int IDOutlet)
        //{
        //    List<ItemDAL> result = new List<ItemDAL>();

        //    using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //    {
        //        result = (from It in db.Item
        //                  join cat in db.Category on It.CategoryID equals cat.ID
        //                  join vart in db.ItemVariant on It.ID equals vart.ItemID
        //                  join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
        //                  where It.ID == ID && vart.OutletID == IDOutlet
        //                  select new ItemDAL
        //                  {
        //                      ID = It.ID,
        //                      VarItemId = vart.ItemID,
        //                      varID = vart.ID,
        //                      Variant = vart.VariantName,
        //                      CatID = cat.ID,
        //                      ItemName = It.Name,
        //                      CatName = cat.Name,
        //                      Price = vart.Price,
        //                      Stock = Inv.Ending,
        //                      AlertAt = Inv.AlertAt,
        //                      SKU = vart.SKU,
        //                  }).ToList();
        //    }
        //    return result;
        //}

        ////Stock= Inv.Ending dirubah, asalnya Inv.Begining
        //public static List<ItemDAL> GetDataByIDVar(int ID, int IDOutlet)
        //{
        //    List<ItemDAL> result = new List<ItemDAL>();

        //    using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //    {
        //        result = (from It in db.Item
        //                  join cat in db.Category on It.CategoryID equals cat.ID
        //                  join vart in db.ItemVariant on It.ID equals vart.ItemID
        //                  join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
        //                  where vart.ID == ID
        //                  select new ItemDAL
        //                  {
        //                      ID = It.ID,
        //                      varID = vart.ID,
        //                      Variant = vart.VariantName,
        //                      CatID = cat.ID,
        //                      ItemName = It.Name,
        //                      CatName = cat.Name,
        //                      Price = vart.Price,
        //                      Stock = Inv.Ending,
        //                      AlertAt = Inv.AlertAt,
        //                      SKU = vart.SKU

        //                  }).ToList();
        //    }
        //    return result;
        //}

        //ditambah parameter IDOutlet, karena data item-variant yang ditampilkan berdasarkan outlet yang sedang di login

        //Mengambil data dari database ( table item, variant, inventory) dimasukan kedalam itemdal viewmodel

        public static List<ItemDAL> GetLayoutItem(string key)
        {
            List<ItemDAL> InnerJoinLayout = new List<ItemDAL>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {

                InnerJoinLayout = (from It in db.Item
                                   join cat in db.Category on It.CategoryID equals cat.ID
                                   join vart in db.ItemVariant on It.ID equals vart.ItemID
                                   join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
                                   where It.Name.Contains(key)
                                   select new ItemDAL
                                   {
                                       ID = It.ID,
                                       IDOutlet = vart.OutletID,
                                       varID = vart.ID,
                                       Variant = vart.VariantName,
                                       CatID = cat.ID,
                                       ItemName = It.Name,
                                       CatName = cat.Name,
                                       Price = vart.Price,
                                       Stock = Inv.Ending,
                                       AlertAt = Inv.AlertAt,
                                       SKU = vart.SKU,
                                   }).ToList();
            }
            return InnerJoinLayout;
        }
        //Mengambil data dari database ( table item, variant, inventory) by categori DDL
        public static List<ItemDAL> GetLayoutItembyCat(string key)
        {
            List<ItemDAL> InnerJoinLayout = new List<ItemDAL>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {

                InnerJoinLayout = (from It in db.Item
                                   join cat in db.Category on It.CategoryID equals cat.ID
                                   join vart in db.ItemVariant on It.ID equals vart.ItemID
                                   join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
                                   where cat.Name.Contains(key)
                                   select new ItemDAL
                                   {
                                       ID = It.ID,
                                       IDOutlet = vart.OutletID,
                                       varID = vart.ID,
                                       Variant = vart.VariantName,
                                       CatID = cat.ID,
                                       ItemName = It.Name,
                                       CatName = cat.Name,
                                       Price = vart.Price,
                                       Stock = Inv.Ending,
                                       AlertAt = Inv.AlertAt,
                                       SKU = vart.SKU
                                   }).ToList();
            }
            return InnerJoinLayout;
        }
        //Mengambil data dari database ( table item, variant, inventory) by ItemID
        public static List<ItemDAL> GetDataByID(int ID, int IDOutlet)
        {
            List<ItemDAL> result = new List<ItemDAL>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from It in db.Item
                          join cat in db.Category on It.CategoryID equals cat.ID
                          join vart in db.ItemVariant on It.ID equals vart.ItemID
                          join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
                          where It.ID == ID && vart.OutletID == IDOutlet
                          select new ItemDAL
                          {
                              ID = It.ID,
                              IDOutlet = vart.OutletID,
                              VarItemId = vart.ItemID,
                              varID = vart.ID,
                              Variant = vart.VariantName,
                              CatID = cat.ID,
                              ItemName = It.Name,
                              CatName = cat.Name,
                              Price = vart.Price,
                              Stock = Inv.Ending,
                              AlertAt = Inv.AlertAt,
                              SKU = vart.SKU,
                          }).ToList();
            }
            return result;
        }

        //Stock= Inv.Ending dirubah, asalnya Inv.Begining
        public static List<ItemDAL> GetDataByIDVar(int ID)
        {
            List<ItemDAL> result = new List<ItemDAL>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from It in db.Item
                          join cat in db.Category on It.CategoryID equals cat.ID
                          join vart in db.ItemVariant on It.ID equals vart.ItemID
                          join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
                          where vart.ID == ID
                          select new ItemDAL
                          {
                              ID = It.ID,
                              IDOutlet = vart.OutletID,
                              varID = vart.ID,
                              Variant = vart.VariantName,
                              CatID = cat.ID,
                              ItemName = It.Name,
                              CatName = cat.Name,
                              Price = vart.Price,
                              Stock = Inv.Ending,
                              AlertAt = Inv.AlertAt,
                              SKU = vart.SKU

                          }).ToList();
            }
            return result;
        }

        public static List<ItemDAL> GetDataVar(int IDOutlet)
        {
            List<ItemDAL> result = new List<ItemDAL>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from It in db.Item
                          join cat in db.Category on It.CategoryID equals cat.ID
                          join vart in db.ItemVariant on It.ID equals vart.ItemID
                          join Inv in db.ItemInventory on vart.ID equals Inv.VariantID
                          where vart.OutletID == IDOutlet
                          select new ItemDAL
                          {
                              ID = It.ID,
                              varID = vart.ID,
                              Variant = vart.VariantName,
                              CatID = cat.ID,
                              ItemName = It.Name,
                              CatName = cat.Name,
                              Price = vart.Price,
                              Stock = Inv.Ending,
                              AlertAt = Inv.AlertAt,
                              SKU = vart.SKU,
                          }).ToList();
            }
            return result;
        }

        public static Item GetItem(int ID)
        {
            Item result = new Item();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Item.Where(x => x.ID == ID).SingleOrDefault();
            }

            return result;
        }

        public static ItemVariant GetVariant(int ID)
        {
            ItemVariant result = new ItemVariant();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.ItemVariant.Where(x => x.ID == ID).SingleOrDefault();
            }

            return result;
        }

        public static ItemInventory GetInventory(int ID)
        {
            ItemInventory result = new ItemInventory();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.ItemInventory.Where(x => x.VariantID == ID).SingleOrDefault();
            }

            return result;
        }

        //save Item
        //public static bool SaveItem(ItemDAL SaveItem, IsiItem Isi)
        //{
        //    try
        //    {
        //        using (PointOfSalesDBContext db = new PointOfSalesDBContext())
        //        {
        //            List<Outlet> Ot = new List<Outlet>();
        //            Ot = db.Outlet.ToList();

        //            var exisItem = db.Item.Where(x => x.ID == SaveItem.ID).SingleOrDefault();

        //            Item itemeach = new Item();
        //            itemeach.ID = SaveItem.ID;
        //            itemeach.Name = SaveItem.ItemName;
        //            itemeach.CategoryID = SaveItem.CatID;
        //            itemeach.CreatedOn = DateTime.Now;
        //            itemeach.ModifiedOn = DateTime.Now;

        //            if (GetItem(SaveItem.ID) == null)
        //            {
        //                db.Item.Add(itemeach);
        //            }
        //            else
        //            {
        //                exisItem.ID = SaveItem.ID;
        //                exisItem.Name = SaveItem.ItemName;
        //                exisItem.CategoryID = SaveItem.CatID;
        //                exisItem.CreatedOn = DateTime.Now;
        //                exisItem.ModifiedOn = DateTime.Now;
        //                db.Entry(exisItem).State = System.Data.Entity.EntityState.Modified;
        //            }
        //            foreach (var OTle in Ot)
        //            {
        //                for (int i = 0; i < Isi.VariantList.Count; i++)
        //                {
        //                    var IDvariant = Isi.VarIDList[i];
        //                    var exisVariant = db.ItemVariant.Where(x => x.ID == IDvariant).SingleOrDefault();
        //                    var exisInve = db.ItemInventory.Where(x => x.VariantID == IDvariant).SingleOrDefault();

        //                    ItemVariant Vareach = new ItemVariant();
        //                    Vareach.ID = Isi.VarIDList[i];
        //                    Vareach.ItemID = SaveItem.ID;
        //                    Vareach.VariantName = Isi.VariantList[i];
        //                    Vareach.SKU = Isi.SKUlist[i];
        //                    Vareach.Price = Isi.PriceList[i];
        //                    Vareach.OutletID = OTle.ID;
        //                    Vareach.CreatedOn = DateTime.Now;
        //                    Vareach.ModifiedOn = DateTime.Now;

        //                    if (GetVariant(Isi.VarIDList[i]) == null)
        //                    {
        //                        db.ItemVariant.Add(Vareach);
        //                    }
        //                    else
        //                    {
        //                        //exisVariant.ID = Isi.VarIDList[i];
        //                        exisVariant.ItemID = SaveItem.ID;
        //                        exisVariant.VariantName = Isi.VariantList[i];
        //                        exisVariant.SKU = Isi.SKUlist[i];
        //                        exisVariant.Price = Isi.PriceList[i];
        //                        exisVariant.OutletID = OTle.ID;
        //                        exisVariant.CreatedOn = DateTime.Now;
        //                        exisVariant.ModifiedOn = DateTime.Now;
        //                        db.Entry(exisVariant).State = System.Data.Entity.EntityState.Modified;
        //                    }


        //                    ItemInventory inveach = new ItemInventory();
        //                    inveach.VariantID = Isi.VarIDList[i];
        //                    inveach.Beginning = Isi.StockList[i];
        //                    inveach.Ending = Isi.StockList[i];
        //                    inveach.AlertAt = Isi.AlertAtList[i];
        //                    inveach.CreatedOn = DateTime.Now;
        //                    inveach.ModifiedOn = DateTime.Now;


        //                    if (GetInventory(Isi.VarIDList[i]) == null)
        //                        db.ItemInventory.Add(inveach);
        //                    else
        //                    {
        //                        exisInve.VariantID = Isi.VarIDList[i];
        //                        exisInve.Beginning = Isi.StockList[i];
        //                        exisInve.AlertAt = Isi.AlertAtList[i];
        //                        exisInve.CreatedOn = DateTime.Now;
        //                        exisInve.ModifiedOn = DateTime.Now;
        //                        db.Entry(exisInve).State = System.Data.Entity.EntityState.Modified;
        //                    }
        //                }
        //            }
        //            db.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public static bool SaveItem2(ItemDAL SaveItem, IsiItem Isi)
        //{
        //    bool result = false;
        //    try
        //    {

        //        using (var db = new PointOfSalesDBContext())
        //        {
        //            List<Outlet> OtletList = db.Outlet.ToList();
        //            foreach (var Ot in OtletList)
        //            {
        //                Item ItemEach = new Item();
        //                ItemEach.Name = SaveItem.ItemName;
        //                ItemEach.CategoryID = SaveItem.CatID;
        //                ItemEach.CreatedOn = DateTime.Now;
        //                ItemEach.ModifiedOn = DateTime.Now;

        //                for (int i = 0; i < Isi.VariantList.Count; i++)
        //                {
        //                    ItemVariant VarEach = new ItemVariant();
        //                    VarEach.VariantName = Isi.VariantList[i];
        //                    VarEach.ItemID = SaveItem.ID;
        //                    VarEach.SKU = Isi.SKUlist[i];
        //                    VarEach.Price = Isi.PriceList[i];
        //                    VarEach.OutletID = Ot.ID;
        //                    VarEach.CreatedOn = DateTime.Now;
        //                    VarEach.ModifiedOn = DateTime.Now;
        //                    db.ItemVariant.Add(VarEach);

        //                    ItemInventory InvEach = new ItemInventory();
        //                    InvEach.Beginning = Isi.StockList[i];
        //                    InvEach.Ending = Isi.StockList[i];
        //                    InvEach.AlertAt = Isi.AlertAtList[i];
        //                    InvEach.VariantID = Isi.VarIDList[i];
        //                    InvEach.CreatedOn = DateTime.Now;
        //                    InvEach.ModifiedOn = DateTime.Now;
        //                    db.ItemInventory.Add(InvEach);

        //                }
        //                db.Item.Add(ItemEach);
        //            }
        //            db.SaveChanges();
        //        }
        //        result = true;
        //    }
        //    catch
        //    {
        //        result = false;
        //    }
        //    return result;
        //}

        public static bool SaveItem3(ItemSave SaveItem)
        {
            var result = false;
            using (var db = new PointOfSalesDBContext())
            {
                List<Outlet> OtletList = db.Outlet.ToList();

                Item ItemEach = new Item();
                ItemEach.Name = SaveItem.Name;
                ItemEach.CategoryID = SaveItem.CategoryID;
                ItemEach.CreatedOn = DateTime.Now;
                ItemEach.ModifiedOn = DateTime.Now;
                ItemEach.ItemVariants = new List<ItemVariant>();
                foreach (var Ot in OtletList)
                {
                    foreach (var Variant in SaveItem.VariantSave)
                    {
                        ItemVariant VariantEach = new ItemVariant();
                        VariantEach.OutletID = Ot.ID;
                        VariantEach.Price = Variant.Price;
                        VariantEach.SKU = Variant.SKU;
                        VariantEach.VariantName = Variant.VariantName;
                        VariantEach.ItemID = SaveItem.ID;
                        VariantEach.CreatedOn = DateTime.Now;
                        VariantEach.ModifiedOn = DateTime.Now;
                        ItemEach.ItemVariants.Add(VariantEach);

                        var listof = new List<ItemInventory>();

                        ItemInventory invEach = new ItemInventory();
                        invEach.Beginning = Variant.InveSave.Begining;
                        invEach.Ending = Variant.InveSave.Ending;
                        invEach.AlertAt = Variant.InveSave.AlertAt;
                        invEach.CreatedOn = DateTime.Now;
                        invEach.ModifiedOn = DateTime.Now;
                        invEach.PurchaseOrder = 0;
                        invEach.Sales = 0;
                        invEach.Transfer = 0;
                        invEach.Adjusment = 0;
                        listof.Add(invEach);
                        VariantEach.ItemInventory = listof;

                    }
                }
                    db.Item.Add(ItemEach);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch
                    {
                        result = false;
                    }
                
                return result;
            }
        }
        public static bool UpdateItem(ItemSave SaveItem)
        {
            bool result = false;
            using (var db = new PointOfSalesDBContext())
            {
                var ExisItem = db.Item.Where(x => x.ID == SaveItem.ID).SingleOrDefault();
                if (ExisItem != null)
                {
                    ExisItem.Name = SaveItem.Name;
                    ExisItem.CategoryID = SaveItem.CategoryID;
                    ExisItem.ModifiedOn = DateTime.Now;
                    ExisItem.ItemVariants = new List<ItemVariant>();
                    db.Entry(ExisItem).State = System.Data.Entity.EntityState.Modified;

                    foreach (var variant in SaveItem.VariantSave)
                    {
                        var ExisVari = db.ItemVariant.Where(x => x.ID == variant.ID).SingleOrDefault();
                        ExisVari.VariantName = variant.VariantName;
                        ExisVari.Price = variant.Price;
                        ExisVari.SKU = variant.SKU;
                        ExisVari.ModifiedOn = DateTime.Now;
                        db.Entry(ExisVari).State = System.Data.Entity.EntityState.Modified;

                        var ExisInv = db.ItemInventory.Where(x => x.VariantID == variant.ID).SingleOrDefault();
                        ExisInv.ModifiedOn = DateTime.Now;
                        ExisInv.Beginning = variant.InveSave.Begining;
                        ExisInv.Ending = variant.InveSave.Ending;
                        ExisInv.AlertAt = variant.InveSave.AlertAt;
                        db.Entry(ExisInv).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch
                {
                    result = false;
                }
            }
            return result;
        }
        public static bool DeleteVariant(int ID)
        {
            var Agru = true;

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                var result = db.ItemVariant.Where(x => x.ID == ID).FirstOrDefault();
                var exisInve = db.ItemInventory.Where(x => x.VariantID == ID).FirstOrDefault();
                if (result != null)
                {
                    db.ItemVariant.Remove(result);
                    db.ItemInventory.Remove(exisInve);
                    try
                    {
                        db.SaveChanges();
                        Agru = true;
                    }
                    catch (Exception)
                    {

                        Agru = false;
                    }
                }
            }
            return Agru;
        }
        public static int? EndingValue(int VarID)
        {
            int? result = 0;
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {

                var Inventory = db.ItemInventory.Where(x => x.VariantID == VarID).FirstOrDefault();
                result = Inventory.Beginning + Inventory.PurchaseOrder - Inventory.Sales - Inventory.Transfer + Inventory.Adjusment;
            }
            return result;
        }

    }
}

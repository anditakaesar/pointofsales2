# README #

Point Of Sales, Mini Project Bootcamp .NET Batch 92

### Set-up Database ###

* **App.config** dan **Web.config** => connectionString dibuat ke local, dengan default database **PointOfSales**
* Buat Database pada SQLServer dengan nama : **PointOfSales**
* Compile dan init database dengan menjalankan service: **inisiasi.asmx**

### Commit Guidelines ###

* PULL perubahan
* PASTIKAN SEBELUM COMMIT, PROGRAM SUDAH **WORK** & **TANPA ERROR**
* Kemudian report ke semua anggota tim
* PUSH
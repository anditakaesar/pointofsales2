﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="ListSuppliers2.aspx.cs" Inherits="pointofsales.Web.suppliers.ListSuppliers2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        // Cek USER jika memiliki IDRole 1 dan 3 yaitu (Backend dan SuperAdmin)
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {

    %>
    <div class="row">
        <h3>Supplier List</h3>
    </div>
    <br />
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <input type="text" class="form-control" id="supplier-search" placeholder="Search..." style="width: 50%" />
        </div>
        <div class="col-md-6 col-lg-6">
            <button type="button" class="btn btn-success pull-right" id="supplier-btncreate">Add Supplier</button>
        </div>
    </div>
    <br />
    <div class="row">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="table-supplier-body"></tbody>
            <tfoot>
                <tr>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- Modal Untuk Supplier -->
    <div class="modal fade" tabindex="-1" role="dialog" id="supplier-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 style="margin-left: 5px;">Supplier Detail</h4>
                    <hr />
                    <div class="form-group">
                        <input type="hidden" id="supplier-modal-id" />
                        <input type="text" class="form-control" placeholder="Nama Supplier..." style="margin-bottom: 4px;" id="supplier-modal-name" />
                        <div class="form-inline">
                            <input type="text" class="form-control" placeholder="Telepon" style="width: 49%; margin-bottom: 4px;" id="supplier-modal-phone" />
                            <input type="text" class="form-control pull-right" placeholder="Email" style="width: 49%; margin-bottom: 4px;" id="supplier-modal-email" />
                        </div>
                    </div>
                    <hr />
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Alamat" style="margin-bottom: 4px;" id="supplier-modal-address" />
                        <div class="form-inline">
                            <select id="select-provinsi" class="form-control" style="width: 49%; margin-bottom: 4px;"></select>
                            <select id="select-kota" class="form-control" style="width: 49%; margin-bottom: 4px;"></select>
                        </div>
                        <div class="form-inline">
                            <select id="select-kecamatan" class="form-control" style="width: 49%; margin-bottom: 4px;"></select>
                            <input type="text" class="form-control" placeholder="Postcode" style="width: 49%; margin-bottom: 4px;" id="supplier-modal-postcode" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="supplier-modal-btnsave">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Untuk Supplier -->
    <!-- Modal Pesan -->
    <div class="modal fade" tabindex="-1" role="dialog" id="supplier-pesan">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <span id="supplier-pesan-msg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Pesan -->
    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavascript" runat="server">
    <script src="../Scripts/location-ajax.js"></script>
    <script type="text/javascript">
        <% 
        Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString() + ";");
        Response.Write("var IDStaff =" + HttpContext.Current.Session["IDStaff"].ToString() + ";");
        %>
        // Load semua supplier
        function LoadSupplier(callback) {
            $.ajax({
                url: '../services/supplierservice.asmx/GetSupplierWithLocation', // before: GetSemuaSupplier
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataSup) {
                    callback(dataSup);
                }
            });
        }

        // Load supplier by SupplierID
        function LoadSupplierById(supID, callback) {
            $.ajax({
                url: '../services/supplierservice.asmx/GetSupplierVMById', // before: GetSupplierByID
                data: '{ "id" : ' + supID + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataSup) {
                    callback(dataSup);
                }
            });
        }

        // Search supplier
        function SearchSupplier(key, callback) {
            $.ajax({
                url: '../services/supplierservice.asmx/SearchSupplierVM', // before: SearchSupplier
                data: '{ "key" : ' + JSON.stringify(key) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataSup) {
                    callback(dataSup);
                }
            });
        }

        // Save Add Supplier
        function SaveSupplier(supplier, callback) {
            $.ajax({
                url: '../services/supplierservice.asmx/AddSupplier',
                data: '{ supplier : ' + JSON.stringify(supplier) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    callback(result); // result = true OR false
                }
            });
        }

        // Save Update Supplier
        function UpdateSupplier(supplier, callback) {
            $.ajax({
                url: '../services/supplierservice.asmx/UpdateSupplier',
                data: '{ supplier : ' + JSON.stringify(supplier) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    callback(result); // result = true OR false
                }
            });
        }

        // Isi Tabel supplier
        function FillSupplierTable(supplier) {
            var listSupplier = '';
            $.each(supplier.d, function (index, item) {
                var kecId = parseInt(item.DistrictID)
                listSupplier += '<tr>'
                + '<td>' + item.Name + '</td>'
                //+ '<td>' + item.Address + ', ' + item.DistrictID + '</td>'
                + '<td>' + item.Address + ', ' + item.Kecamatan + ', ' + item.Kota + '</td>'
                + '<td>' + item.Phone + '</td>'
                + '<td>' + item.Email + '</td>'
                + '<td><button onclick="EditSupplier(this.value)" type="button" class="btn btn-default" value="' + item.ID + '">Edit</button></td>'
                + '</tr>';
            });
            $('#table-supplier-body').html(listSupplier);
        }
        
        // Tampilkan pesan supplier
        function TampilkanPesanSupplier(pesan) {
            $('#supplier-pesan-msg').html(pesan);

            $('#supplier-pesan').modal('show');

            LoadSupplier(FillSupplierTable);

        }

        // Edit Supplier Button
        function EditSupplier(supID) {
            ClearFormSupplier();
            LoadSupplierById(supID, function (data) {
                IsiFormSupplier(data);
                $('#supplier-modal').modal('show');
            })
        }

        // Isi Form supplier
        function IsiFormSupplier(dataSupplier) {
            var supplier = dataSupplier.d;
            $('#supplier-modal-id').val(supplier.ID);
            $('#supplier-modal-name').val(supplier.Name);
            $('#supplier-modal-phone').val(supplier.Phone);
            $('#supplier-modal-email').val(supplier.Email);
            $('#supplier-modal-address').val(supplier.Address);
            $('#select-kecamatan').html('<option value="' + supplier.DistrictID + '">'+ supplier.Kecamatan +'</option>')
            $('#select-kota').html('<option>' + supplier.Kota + '</option>')
            $('#supplier-modal-postcode').val(supplier.PostalCode);
        }

        // Isi Dropdown Provinsi
        function IsiSelectProvinsi(dataProvinsi) {
            var listProv = '';
            listProv += '<option value="-1">--Pilih Provinsi--</option>'
            $.each(dataProvinsi, function (index, item) {
                listProv += '<option value="'+ item.ID +'">'+ item.ProvinceName +'</option>'
            })
            $('#select-provinsi').html(listProv);
        }

        // Isi Dropdown Kecamatan

        // Isi Dropdown Kota
        // Trigger provinsi diubah -> kota di load
        $('#select-provinsi').change(function () {
            var listKota = '';
            listKota += '<option value="-1">--Pilih Kota--</option>'
            var provId = $('#select-provinsi').val();
            // Clear kota
            $('#select-kota').html(listKota);
            LoadKotaByProvinsiId(provId, function (datakota) {
                $.each(datakota, function (i, item) {
                    listKota += '<option value="' + item.ID + '">' + item.RegionName + '</option>';
                })
                $('#select-kota').html(listKota);
            })            
        })

        // Isi Dropdown Kecamatan
        // Trigger Kota diubah -> kecamatan di load
        $('#select-kota').change(function () {
            var listKeca = '';
            listKeca += '<option value="-1">--Pilih Kecamatan--</option>'
            var kotaId = $('#select-kota').val();
            // Clear kota
            $('#select-kecamatan').html(listKeca);
            LoadKecamatanByKotaId(kotaId, function (datakeca) {
                $.each(datakeca, function (i, item) {
                    listKeca += '<option value="' + item.ID + '">' + item.DistrictName + '</option>';
                })
                $('#select-kecamatan').html(listKeca);
            })
        })

        // Set Dropdown to default
        function ClearLocationDropdown() {
            var listKota = '<option value="-1">--Pilih Kota--</option>';
            var listKeca = '<option value="-1">--Pilih Kecamatan--</option>';
            $('#select-kecamatan').html(listKeca);
            $('#select-kota').html(listKota);
        }

        // Clear Form Supplier
        function ClearFormSupplier() {
            $('#supplier-modal-id').val('');
            $('#supplier-modal-name').val('');
            $('#supplier-modal-phone').val('');
            $('#supplier-modal-email').val('');
            $('#supplier-modal-address').val('');
            //$('#supplier-modal-district').val('');
            $('#supplier-modal-postcode').val('');
            ClearLocationDropdown();
        }

        // Trigger save button
        $('#supplier-modal-btnsave').click(function () {
            var testId = $('#supplier-modal-id').val();
            var supplier = {};
            if (testId) { // Jika ID Ada UPDATE    
                supplier.ID = $('#supplier-modal-id').val();
                supplier.Name = $('#supplier-modal-name').val();
                supplier.Phone = $('#supplier-modal-phone').val();
                supplier.Email = $('#supplier-modal-email').val();
                supplier.Address = $('#supplier-modal-address').val();
                // AWAS ERROR!
                supplier.DistrictID = $('#select-kecamatan').val();
                supplier.PostalCode = $('#supplier-modal-postcode').val();
                supplier.ModifiedBy = IDStaff;

                // save melalui ajax, refresh table supplier
                UpdateSupplier(supplier, function (data) {
                    var pesan = "Supplier GAGAL UPDATE"
                    if (data)
                        pesan = "Supplier BERHASIL UPDATE"

                    TampilkanPesanSupplier(pesan);
                });
                $('#supplier-modal').modal('hide');

            } else { // Jika ID kosong CREATE                
                // TODO: Form Validation
                // masukkan detail ke supplier object
                supplier.Name = $('#supplier-modal-name').val();
                supplier.Phone = $('#supplier-modal-phone').val();
                supplier.Email = $('#supplier-modal-email').val();
                supplier.Address = $('#supplier-modal-address').val();
                // BUAT SEBAGAI DROPDOWN LIST
                supplier.DistrictID = $('#select-kecamatan').val();
                supplier.PostalCode = $('#supplier-modal-postcode').val();
                supplier.ModifiedBy = IDStaff;
                supplier.CreatedBy = IDStaff;

                // save melalui ajax, refresh table supplier
                SaveSupplier(supplier, function (data) {
                    var pesan = "Supplier gagal dibuat"
                    if (data)
                        pesan = "Supplier berhasil dibuat";

                    TampilkanPesanSupplier(pesan);
                });

                $('#supplier-modal').modal('hide');
            }
        })

        // Triger Search
        $('#supplier-search').keypress(function () {
            var key = $('#supplier-search').val();
            if (key.length >= 3) {
                SearchSupplier(key, FillSupplierTable);
            } else {
                LoadSupplier(FillSupplierTable);
            }
        })

        // Trigger Create button
        $('#supplier-btncreate').click(function () {
            ClearFormSupplier();
            $('#supplier-modal').modal('show');
        })

        // Document ready function
        $(document).ready(function () {
            LoadSupplier(FillSupplierTable);
            LoadSemuaProvinsi(IsiSelectProvinsi);

        });
    </script>
</asp:Content>

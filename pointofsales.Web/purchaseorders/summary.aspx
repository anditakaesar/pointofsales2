﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="summary.aspx.cs" Inherits="pointofsales.Web.purchaseorders.summary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POS - Summary</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/daterangepicker.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <%--nav bar fixed buat menu--%>

            <nav class="navbar navbar-default" style="background-color: #428bca;">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>

                </div>
            </nav>
        <div class="container">
            <div class="form-inline">
                <div class="form-group">

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date-range"/>
                </div>
              </div>
                <div class="form-group">
                    <label class="sr-only" for="searching">search</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        <input type="text" class="form-control" id="searching" placeholder="Search.." />
                    </div>
                </div>
                <button type="button" class="btn btn-primary pull-right" disabled="disabled">Export</button>
            </div>
            <div class="row">
            <br />
            </div>
            <div class="row">
                <table id="tbl-sum" class="table table-bordered">
                    <thead>
                        <tr style="background-color: #cccccc;">
                            <th>Name - Variant</th>
                            <th>Category</th>
                            <th>Beginning</th>
                            <th>Purchase Order</th>
                            <th>Sales</th>
                            <th>Transfer</th>
                            <th>Adjusment</th>
                            <th>Ending</th>
                        </tr>
                    </thead>
                    <tbody id="tbl-data-sum">
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
<script src="../Scripts/jquery-3.1.1.min.js"></script>
<script src="../Scripts/moment.min.js"></script>
<script src="../Scripts/moment-with-locales.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script src="../Scripts/bootstrap-datetimepicker.min.js"></script>
<script src="../Scripts/daterangepicker.js"></script>
<script>
    function loadData() {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetSum',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (dataSum) {
                var cetakSum = "";
                $.each(dataSum.d, function (index, item) {
                    cetakSum += '<tr><td>' + item.ItemName +' '+ item.VariantName +'</td><td>Kategori dari id Variant</td><td>' + item.Beginning + '</td><td>' + item.PurchaseOrder + '</td><td>' + item.Sales + '</td><td>' + item.Transfer + '</td><td>' + item.Adjusment + '</td><td>' + item.Ending + '</td></tr>';
                });
                $('#tbl-data-sum').html(cetakSum);
            }
        });
    }
    $(document).ready(function () {
        loadData();
    });
    $('#date-range').daterangepicker();
</script>
</html>

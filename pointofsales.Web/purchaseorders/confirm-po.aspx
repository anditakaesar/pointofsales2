﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create-po.aspx.cs" Inherits="pointofsales.Web.purchaseorders.create_po" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POS - Purchase Order</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <style>
        .input-po {
            width:100%;
            border: none;
            border-color: none;
        }
    </style>
    <script src="../Scripts/jquery-3.1.1.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="https://mathias-bank.de/jQuery/jquery.getUrlParam.js"></script>
    <script>
        var dataId = $(document).getUrlParam("id");
        function getAllId(dataId) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetAllId',
                data: '{"id":"' + dataId + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $.each(data.d, function(index, item){
                        loadSupplier(item.SupplierID);
                        loadPO(dataId);
                        loadItem(dataId);
                    });
                }
            });
        }
        
    </script>
</head>
<body onload="getAllId(dataId)">
    <form id="form1" runat="server">
        <%--nav bar fixed buat menu--%>
        <nav class="navbar navbar-default" style="background-color:#428bca;">
            <div class="container-fluid">
                <div class="navbar-header">
                </div>

            </div>
        </nav>
        <div class="col-md-6 col-md-offset-3 column">
            <div class="row clearfix">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="form-group">
                            <button class="btn btn-default" type="button" id="resend">Resend</button>
                            <div class="dropdown pull-right">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   More
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><label class="btn" id="success">Mark as fulfilled </label><span style="color:green" class="glyphicon glyphicon-ok-sign"></span></li>
                                    <li><label class="btn" id="cancel">Cancel Order <span style="color:red" class="glyphicon glyphicon-minus-sign"></span></label></li>
                                    <li><a href="#">Print</a></li>
                                    <li><a href="edit-po.aspx">Edit</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="outlet">Purchase Order Details</label>
                            <hr />
                            <div class="h4"> Nama Suppliers </div>
                            <table class="table table-bordered">
                                <tbody id="tbl-supplier"></tbody>                               
                            </table>
                        </div>                                                 
                        <div class="form-group">
                            <label for="supplier">Note</label>
                            <hr />
                            <p id="txt-note"></p>
                            <hr />
                            <p id="txt-detail-order"></p>
                        </div>
                        <div class="form-group">
                            <label for="add-po">Status Activity</label>
                            <hr />
                            <input type="text" class="form-control" id="txt-status" />
                        </div>
                        <div class="form-group">
                            <label for="purchase-order">Purchase Order</label>
                            <hr />
                            <table class="table table-bordered">
                                <thead>
                                    <tr style="border:none;">
                                        <th>Item</th>
                                        <th>In Stock</th>
                                        <th>Order</th>
                                        <th>Unit Cost</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl-items">
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            TOTAL
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="button" id="btn-done" class="btn btn-primary pull-right">Done</button>
                    </div>
                </div>
            </div>           
        </div>
    </form>
</body>

<script>    
    function loadSupplier(idsup) {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetSupplierById',
            data: '{"id":"' + idsup + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var suplier = "";
                $.each(dataPO.d, function (index, items) {
                    suplier += '<tr><td>' + items.Phone + '</td><td colspan="2">' + items.Email + '</td></tr><tr><td colspan="3">' + items.Address + '</td></tr><tr><td>' + items.DistrictID + '</td><td>' + items.DistrictID + '</td><td>' + items.PostalCode + '</td></tr>';
                });
                $('#tbl-supplier').html(suplier);
            }
        });
    }
    function loadPO(id) {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetDetailConfirm',
            data: '{"id":"' + id + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetak = "";
                var not = "";
                var stat = "";
                $.each(dataPO.d, function (index, items) {
                    cetak += 'PO Number ' + items.OrderNo + ' <br>Created at : ' + formatJSONDate(items.CreatedOn).toDateString() + '<br>Created by : ' + items.CreatedBy + '<br>Email : ' + items.Email + '<br>Outlet Name : ' + items.OtlName + '<br>Phone : ' + items.OtlPhone + '<br>Address : ' + items.OtlAddress + '<br>';
                    not = items.Notes;
                    stat = 'At ' + formatJSONDate(items.CreatedOn).toDateString() + ' ' + items.OrderNo + 'is created';
                });
                $('#txt-detail-order').html(cetak);
                $('#txt-note').html(not);
                document.getElementById('txt-status').value = stat;
                //$('#txt-status').html(stat);
            }
        });
    }
    function loadItem(Id) {
        $.ajax({
            url: '../Services/PurchaseOrder.asmx/GetItemByPOId',
            data: '{"id":"' + Id + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataItem) {
                var listItem = "";
                $.each(dataItem.d, function (index, item) {
                    listItem += '<tr id="item-' + item.IdItem + '"><td><input type="hidden" id="idvariant" value="' + item.IdVariant + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input class="input-po" type="text" id="jumlah-order" value="' + item.Order + '"></td><td>' + item.UnitCost + '</td><td>' + item.Total + '</td></tr>';
                });
                $('#tbl-items').html(listItem);
            }
        });
    }
    function statusSuccess() {
        var id = dataId;
        var order = $('#jumlah-order').val();
        var status = 1;
        $.ajax({
            url: '../services/PurchaseOrder.asmx/UpdateStatusPO',
            data: '{id:'+JSON.stringify(id)+',stat:'+JSON.stringify(status)+',order:'+JSON.stringify(order)+'}',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            success: function (data) {
                alert("Status Berhasil diubah");
            }
        });
    }
    function statusCancel() {
        var id = dataId;
        var order = $('#jumlah-order').val();
        var status = 2;
        $.ajax({
            url: '../services/PurchaseOrder.asmx/UpdateStatusPO',
            data: '{id:' + JSON.stringify(id) + ',stat:' + JSON.stringify(status) + ',order:' + JSON.stringify(order) + '}',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            success: function (data) {
                alert("Pembatalan Berhasil");
                window.location.href = "purchase-order.aspx";
            }
        });
    }
    function formatJSONDate(jsonDate) {
        var newDate = new Date(parseInt(jsonDate.substr(6)));
        return newDate;
    }
    $('#success').click(function () {
        statusSuccess();
    });
    $('#cancel').click(function () {
        statusCancel();
    })
    $('#btn-done').click(function () {
        window.location.href = "purchase-order.aspx";
    });
</script>
</html>

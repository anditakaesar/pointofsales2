﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create-po.aspx.cs" Inherits="pointofsales.Web.purchaseorders.create_po" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POS - Purchase Order</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <style> 
        .input-po {
            width:100%;
            border: none;
            border-color: none;
        }
        .tr-th-polos {
            border:none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <%--nav bar fixed buat menu--%>
        <nav class="navbar navbar-default" style="background-color:#428bca;">
            <div class="container-fluid">
                <div class="navbar-header">
                </div>

            </div>
        </nav>
        <div class="col-md-6 col-md-offset-3 column">
            <div class="row clearfix">
                <div class="panel">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label for="outlet">Choose Outlet</label>
                            <hr />
                            <select id="select-outlet" class="form-control">
                            </select>
                        </div>                            
                     
                        <div class="form-group">
                            <label for="supplier">Choose Supplier</label>
                            <hr />
                            <select id="select-supplier" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <hr />
                            <textarea rows="4" placeholder="Notes" id="note" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="add-po">Purchase Order</label>
                            <hr />
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>In Stock</th>
                                        <th>Order</th>
                                        <th>Unit Cost</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input class="input-po" type="text" id="item" value="aaa"/></td>
                                        <td><input class="input-po" type="text" id="instock"  value="aaa"/></td>
                                        <td><input class="input-po" type="text" id="order"  value="aaa"/></td>
                                        <td><input class="input-po" type="text" id="unitcost" /></td>
                                        <td><input class="input-po" type="text" id="total"/></td>
                                    </tr>
                                </tbody>
                            </table>
                            <label for="totals">Total</label><input id="total-po" type="hidden" value="Rp. " class="pull-right" />
                            <button type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#modalss">Add Purchase Order</button>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-default" onclick="goBack()">Cancel</button>
                            <button type="button" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
           <%-- MODAL--%>
            <div id="modalss" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add Purchase Order</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="item-name" value="Lamborghini"/>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr style="border:none;">
                                        <th>Item</th>
                                        <th>In Stock</th>
                                        <th>Order</th>
                                        <th>Unit Cost</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input class="input-po" type="text" id="mdl-item" value="Lambhorgini"/></td>
                                        <td><input class="input-po" type="text" id="mdl-instock" /></td>
                                        <td><input class="input-po" type="text" id="mdl-order" /></td>
                                        <td><input class="input-po" type="text" id="mdl-unitcost" /></td>
                                        <td><input class="input-po" type="text" id="mdl-total"/></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary pull-right" id="add-item-po">Add</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </form>
</body>
<script src="../Scripts/jquery-3.1.1.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script>
    function loadOutlet() {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetOutlet',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakOutlet = "";
                $.each(dataPO.d, function (index, item) {
                    cetakOutlet += '<option>' + item.OutletName + '</option>';
                });
                $('#select-outlet').html(cetakOutlet);
            }
        });
    }
    function loadSupplier() {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetSupplier',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakSupplier = "";
                $.each(dataPO.d, function (index, item) {
                    cetakSupplier += '<option>' + item.Name + '</option>';
                });
                $('#select-supplier').html(cetakSupplier);
            }
        });
    }
    $(document).ready(function () {
        loadOutlet();
        loadSupplier();
    });
    function goBack() {
        window.history.back();
    }
</script>
</html>

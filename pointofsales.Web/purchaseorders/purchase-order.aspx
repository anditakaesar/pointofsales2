﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="purchase-order.aspx.cs" Inherits="pointofsales.Web.purchaseorders.AddPO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POS - Purchase Order</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/daterangepicker.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <%--nav bar fixed buat menu--%>
        <nav class="navbar navbar-default" style="background-color:#428bca;">
            <div class="container-fluid">
                <div class="navbar-header">
                </div>

            </div>
        </nav>
        <div class="container">
            <div class="form-inline">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="date-range" />
                    </div>
                </div>
                <div class="form-group">
                    <select class="form-control" id="select-status" onchange="loadSearch()">

                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="searching">search</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        <input type="text" class="form-control" id="searching" placeholder="Search.." />
                    </div>
                </div>
                <div class="form-group pull-right">
                    <a href="../purchaseorders/create-po.aspx" class="btn btn-primary ">Create Purchased Order</a>
                    <button type="button" class="btn btn-primary-spacing btn-primary" disabled="disabled">Export</button>
                </div>                
            </div>
        </div>
        <div class="row">
            <br />
        </div>
        <div class="container">
            <table id="tbl-po" class="table table-bordered">
                <thead style="border:none;">
                    <tr style="background-color:#cccccc;">
                        <th>Date</th>
                        <th>Supplier</th>
                        <th>Order No.</th>
                        <th>Total</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="tbl-data-po">
                </tbody>
            </table>
        </div>
    </form>
</body>
<script src="../Scripts/jquery-3.1.1.min.js"></script>
<script src="../Scripts/moment.min.js"></script>
<script src="../Scripts/moment-with-locales.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script src="../Scripts/bootstrap-datetimepicker.min.js"></script>
<script src="../Scripts/daterangepicker.js"></script>
<script>
    function loadData() {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetData',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakTabel = "";
                $.each(dataPO.d, function (index, item) {
                    cetakTabel += '<tr><td>' + formatJSONDate(item.CreatedOn).toDateString() + '</td><td>' + item.Name + '</td><td>' + item.OrderNo + '</td><td>' + item.SubTotal + '</td><td>' + item.StatusName + '</td></tr>';
                });
                $('#tbl-data-po').html(cetakTabel);
            }
        });
    }
    function loadSearch() {
        var strid = $('#select-status').val();
        var id = parseInt(strid);
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetStatusById',
            data: '{id:'+JSON.stringify(id)+'}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakTabel = "";
                $.each(dataPO.d, function (index, item) {
                    cetakTabel += '<tr><td>' + formatJSONDate(item.CreatedOn).toDateString() + '</td><td>' + item.Name + '</td><td>' + item.OrderNo + '</td><td>' + item.SubTotal + '</td><td>' + item.StatusName + '</td></tr>';
                });
                $('#tbl-data-po').html(cetakTabel);
            }
        });
    }
    function loadStatus() {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetStatus',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakStatus = "";
                $.each(dataPO.d, function (index, item) {
                    cetakStatus += '<option id="pilihstatus" value="'+item.ID+'">'+item.StatusName+'</option>';
                });
                $('#select-status').html(cetakStatus);
            }
        });
    }
    $(document).ready(function () {
        loadData();
        loadStatus();
        loadSearch();
    });
    //formatJSONDate(item.CreatedOn).toDateString()
    function formatJSONDate(jsonDate) {
        var newDate = new Date(parseInt(jsonDate.substr(6)));
        return newDate;
    }
    $('#date-range').daterangepicker();
</script>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create-po.aspx.cs" Inherits="pointofsales.Web.purchaseorders.create_po" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POS - Purchase Order</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <style> 
        .input-po {
            width:100%;
            border: none;
            border-color: none;
        }
        .tr-th-polos {
            border:none;
        }
        .button-po {
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            outline: none;
            border: 0;
            background: transparent;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <%--nav bar fixed buat menu--%>
        <nav class="navbar navbar-default" style="background-color:#428bca;">
            <div class="container-fluid">
                <div class="navbar-header">
                </div>

            </div>
        </nav>
        <div class="col-md-6 col-md-offset-3 column">
            <div class="row clearfix">
                <div class="panel">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label for="outlet">Choose Outlet</label>
                            <hr />
                            <select id="select-outlet" class="form-control">
                            </select>
                        </div>                            
                     
                        <div class="form-group">
                            <label for="supplier">Choose Supplier</label>
                            <hr />
                            <select id="select-supplier" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <hr />
                            <textarea rows="4" placeholder="Notes" id="note" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="add-po">Purchase Order</label>
                            <hr />
                            <div class="hidings">
                                <table class="table table-bordered" id="tbl-add-po">
                                    <thead>
                                        <tr style="border: none;">
                                            <th>Item</th>
                                            <th>In Stock</th>
                                            <th>Order</th>
                                            <th>Unit Cost</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl-po">
                                    </tbody>
                                </table>
                            </div>                            
                            <label for="totals">Total</label><input id="total-po" type="hidden" value="Rp. " class="pull-right" />
                            <button type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-add-po">Add Purchase Order</button>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-default" onclick="goBack()">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="saveItem()">Save</button>
                        </div>
                    </div>
                </div>
            </div>

           <%-- MODAL--%>
            <div id="modal-add-po" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add Purchase Order</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txt-search-item"/>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr style="border:none;">
                                        <th>Item</th>
                                        <th>In Stock</th>
                                        <th>Order</th>
                                        <th>Unit Cost</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl-modal-add-po">
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" onclick="callAdd()">Add</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </form>
</body>
<script src="../Scripts/jquery-3.1.1.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script>
    function loadOutlet() {
        // parameter outlet by login
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetOutlet',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakOutlet = "";
                $.each(dataPO.d, function (index, item) {
                    cetakOutlet += '<option value="'+item.ID+'">' + item.OutletName + '</option>';
                });
                $('#select-outlet').html(cetakOutlet);
            }
        });
    }
    function loadSupplier() {
        // parameter supplier by outlet
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetSupplier',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataPO) {
                var cetakSupplier = "";
                $.each(dataPO.d, function (index, item) {
                    cetakSupplier += '<option value="'+item.ID+'">' + item.Name + '</option>';
                });
                $('#select-supplier').html(cetakSupplier);
            }
        });
    }
    function goBack() {
        window.history.back();
    }    
    function cobain(searchValue) {
        $.ajax({
            url: '../services/PurchaseOrder.asmx/GetItemSearch',
            data: '{"search":"' +searchValue+'"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (datacoba) {
                var cetakcoba = "";
                $.each(datacoba.d, function (index, item) {
                    cetakcoba += '<tr id="item-' + item.ID + '"><td><input type="hidden" id="itemid" value="' + item.IdItem + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input type="text" id="input-order" class="input-po"/></td><td><input type="text" id="input-unitcost" onchange="count()" class="input-po"/></td><td><input type="text" id="input-total"  class="input-po" /></td></tr>';
                });
                $('#tbl-modal-add-po').html(cetakcoba);
            }
        });
    }
    function cobain1(Id) {
        $.ajax({
            url: '../Services/PurchaseOrder.asmx/GetItemById',
            data: '{"id":"' + Id + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (dataItem) {
                var listItem = "";
                $.each(dataItem.d, function (index, item) {
                    listItem += '<tr id="item-' + item.IdItem + '"><td><input type="hidden" id="idvariant" value="' + item.IdVariant + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input type="text" id="order-po" class="input-po"/></td><td><input type="text" id="unitcost-po" class="input-po"/></td><td><input type="text" id="total-po" class="input-po" /><button class="button-po" onclick="deletePo(' + item.IdItem + ')"><span style="color:red" class="glyphicon glyphicon-remove-circle"></span></button></td></tr>';
                });
                $('#tbl-po').html(listItem); 
            }
        });
        var order = $('#input-order').val();
        var unitcost = $('#input-unitcost').val();
        var total = $('#input-total').val();
        alert("Berhasil ditambahkan");
        $('#modal-add-po').modal('hide');
        document.getElementById('order-po').value = order;
        document.getElementById('unitcost-po').value = unitcost;
        document.getElementById('total-po').value = total;
    }  
    function callAdd() {
        var id = $('#itemid').val();
        cobain1(id);
    }
    function count() {
        var cost = $('#input-unitcost').val();
        var order = $('#input-order').val();
        var total = cost * order;
        document.getElementById('input-total').value = total;
    }
    function deletePo(Id) {
        var itemID = '#item-' + Id.toString();
        $(itemID).remove();
    }
    function savePo() {

    }
    function saveItem() {
        var addPO = {};
        addPO.OutletID = $("#select-outlet").val();
        addPO.SupplierID = $("#select-supplier").val();
        addPO.Notes = $("#note").val();
        addPO.IdVariant = $("#idvariant").val();
        addPO.Quantity = $("#order-po").val();
        addPO.UnitCost = $("#unitcost-po").val();
        addPO.SubTotal = $("#total-po").val();
        //addPO.CreatedBy = ;
        $.ajax({
            url: '../services/PurchaseOrder.asmx/AddTrxPo',
            data: '{add:'+JSON.stringify(addPO)+'}',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                alert("Transaksi berhasil disimpan.");
                window.location.href = "confirm-po.aspx?id=" + JSON.stringify(data.d);
            }
        });
    }
    function formatJSONDate(jsonDate) {
        var newDate = new Date(parseInt(jsonDate.substr(6)));
        return newDate;
    }
    $('#txt-search-item').keypress(function () {
        var searchValue = $('#txt-search-item').val();
        cobain(searchValue);
    });
    $(document).ready(function () {
        loadOutlet();
        loadSupplier();
        cobain();
    });
</script>
</html>

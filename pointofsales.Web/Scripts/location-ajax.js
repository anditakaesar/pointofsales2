﻿/*
FUNGSI LOAD LOCATION
author: Ndit
*/
// Provinsi
function LoadSemuaProvinsi(callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetProvinsi',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataProvinsi) {
            callback(dataProvinsi.d);
        }
    })
}

function LoadProvinsiById(provId, callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetProvinsiById',
        data: '{ "id": '+ provId +'}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataProvinsi) {
            callback(dataProvinsi.d);
        }
    })
}

// End of Provinsi

// Kota
function LoadKotaByProvinsiId(provID, callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetKotaByProvId',
        data: '{"id":'+ provID +'}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataKota) {
            callback(dataKota.d);
        }
    })
}

function LoadKotaById(kotaId, callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetKotaById',
        data: '{"id":' + kotaId + '}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataKota) {
            callback(dataKota.d);
        }
    })
}
// Kota

// Kecamatan
function LoadKecamatanByKotaId(kotaId, callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetKecamatanByKotaId',
        data: '{"id":' + kotaId + '}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataKeca) {
            callback(dataKeca.d);
        }
    })
}

function LoadKecamatanById(kecaId, callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetKecamatanById',
        data: '{"id":' + kecaId + '}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataKeca) {
            callback(dataKeca.d);
        }
    })
}

// Not Working - Ndit
function LoadNamaKecamatanById(kecaId, callback) {
    $.ajax({
        url: '../services/lokasiservice.asmx/GetKecamatanNameById',
        data: '{"id":' + kecaId + '}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8;',
        success: function (dataKeca) {
            callback(dataKeca.d);
        }
    })
}

// Kecamatan
﻿function GetCurrentTime() {
    var waktu = new Date();
    var jam = waktu.getHours();
    var menit = waktu.getMinutes();
    var detik = waktu.getSeconds();
    return jam + " : " + menit + " : " + detik;
}

function convertTanggal(tgl) {
    var tanggal = new Date(parseInt((tgl).replace(/[^\d]/g, '')));

    var tahun = tanggal.getFullYear();
    var bulan = tanggal.getMonth();
    var tg = tanggal.getDate();

    bulan = convertToInd(bulan + 1);
    tg = tg < 10 ? "0" + tg : tg;

    return tg + " - " + bulan + " - " + tahun;
}

function convertToInd(bulan) {
    switch (bulan) {
        case 1:
            return "Januari";
            break;
        case 2:
            return "Februari";
            break;
        case 3:
            return "Maret";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Agustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "Desember";
            break;
    }
}


function checkInput(obj) {

    var pola = "^";
    pola += "[0-9]*";
    pola += "$";
    rx = new RegExp(pola);

    if (!obj.value.match(rx)) {
        if (obj.lastMatched) {
            obj.value = obj.lastMatched;
        }
        else {
            obj.value = "";
        }
    }
    else {
        obj.lastMatched = obj.value;
    }
}
﻿$(document).ready(function () {
    loadAdjusment();
    getAllVariant();
    selectCurrentOutlet();
    $('.select2').select2();
    $("#list-item-variant").val('');
    $("#form-adjusment").hide();



    //$("#table-data-adjustment").dataTable({
    //    "searching": false
    //});

    //$("#table-data-adjustment").dataTable({
    //    "blengthChange": true,
    //    "paging": true,
    //    "sPaginationType": "full_numbers",
    //    "jQueryUI": true
    //    //"processing": true,
    //    //"serverSide": true
    //});

    $("#tgl-mulai").datepicker({
        inline: true
    });

    $("#tgl-berakhir").datepicker({
        inline: true
    });
})

$("#add-adjusment").click(function () {
    $("#form-adjusment").slideToggle("slow");
})

bkLib.onDomLoaded(function () { nicEditors.allTextAreas() });

//fungsi cari data adjusment by tanggal
$('#btn-cari').click(function () {

    //var tanggalMulai = convertTanggal($("#tgl-mulai").val());
    //var tangglAkhir = convertTanggal($("#tgl-berakhir").val());
    var tanggalMulai = $("#tgl-mulai").val();
    var tangglAkhir = $("#tgl-berakhir").val();

    if (tanggalMulai == "" || tangglAkhir == "") {
        alert("tanggal tidak boleh kosong");
        return false;
    }
    if (tangglAkhir < tanggalMulai) {
        alert("tanggal akhir harus lebih besar dari tanggal awal")
    } else {
        $.ajax({
            url: '../services/adjustmentservices.asmx/getDataByDate',
            data: '{"tglAwal":"' + tanggalMulai + '","tglAkhir":"' + tangglAkhir + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var jamsekarang = GetCurrentTime();
                var listAdj = '';
                $.each(data.d, function (index, item) {
                    var tanggalAdjusment = convertTanggal(item.CreatedOn);

                    listAdj += '<tr>' +
                        '<td>' + tanggalAdjusment + '</td>' +
                        '<td>' + item.Note + '</td>' +
                        '<td>' + item.VariantName + '</td>' +
                        '<td>' + item.Adjusment + '</td>' +
                        '</tr>'
                })
                $('#data-adjusment').html(listAdj);
            }
        })
    }
})


//fungsi load adjustment
function loadAdjusment() {
    $.ajax({
        url: '../services/adjustmentservices.asmx/GetAllAdjustment',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var jamsekarang = GetCurrentTime();
            var listAdj = '';
            $.each(data.d, function (index, item) {
                var tanggalAdjusment = convertTanggal(item.CreatedOn);

                listAdj += '<tr>' +
                    '<td>' + tanggalAdjusment + '</td>' +
                    '<td>' + item.Note + '</td>' +
                    '<td>' + item.VariantName + '</td>' +
                    '<td>' + item.Adjusment + '</td>' +
                    '</tr>'
            })
            $('#data-adjusment').html(listAdj);
        }
    })
}

/*fungsi jquery buat list variant*/

function selectVarById(id) {
    $.ajax({
        url: '../services/adjustmentservices.asmx/GetVariantByID',
        data: '{ID: ' + id + '}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data.d, function (index, item) {

                $('#table-adjusment > tbody:last-child')
                .append('<tr id=variantid-' + item.ID + '>' +
                '<td>' +
                '<input type="hidden" name="VariantID" value="' + item.ID + '" />' +
                item.VariantName + '</td>' +
                '<td>' + item.Ending + '</td>' +
                '<td><input type="text" id="actual-stock" class="form-control" name="ActualStock" onkeyup="return checkInput(this); " /></td></tr>');

            });
        }
    });
}


var $topo = $('#list-item-variant');

var valArray = ($topo.val()) ? $topo.val() : [];

$topo.change(function () {
    var val = $(this).val(),
        numVals = (val) ? val.length : 0,
        changes;
    if (numVals != valArray.length) {
        var longerSet, shortSet;
        (numVals > valArray.length) ? longerSet = val : longerSet = valArray;
        (numVals > valArray.length) ? shortSet = valArray : shortSet = val;
        //create array of values that changed - either added or removed
        changes = $.grep(longerSet, function (n) {
            return $.inArray(n, shortSet) == -1;
        });

        logChanges(changes, (numVals > valArray.length) ? 'selected' : 'removed');

    } else {
        // if change event occurs and previous array length same as new value array : items are removed and added at same time
        logChanges(valArray, 'removed');
        logChanges(val, 'selected');
    }
    valArray = (val) ? val : [];
});

var jumlah = 0;
function logChanges(array, type) {
    $.each(array, function (i, item) {
        if (type == 'selected') {
            selectVarById(item);
            jumlah += 1;
            $('#jumlah-adjusment').val(jumlah);

        } else {
            $('#variantid-' + item).remove();

            jumlah -= 1;
            $('#jumlah-adjusment').val(jumlah);
        }

    });
}


function getAllVariant() {
    $.ajax({
        url: '../services/adjustmentservices.asmx/GetAllVariant',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var listAdj = '';
            $.each(data.d, function (index, item) {
                listAdj += '<option value="' + item.ID + '">' + item.VariantName + '</option>'
            })
            $('#list-item-variant').html(listAdj);
        }
    })
}

//simpan data adjustment
function SaveTempAdjusment(dataAdjusment) {
    $.ajax({
        url: '../services/adjustmentservices.asmx/SaveDataAdjusment',
        data: '{obj:' + JSON.stringify(dataAdjusment) + '}',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset = utf-8;',
        success: function (data) {
            if (data.d) {
                loadAdjusment();
                alert('Data adjusment berhasil disimpan!');
                $('#jumlah-adjusment').val(0);
                location.reload();
            }
            else {
                alert('Data adjusment gagal disimpan!');
                $('#jumlah-adjusment').val(0);
                location.reload();
            }
        }
    });
}

function cekField() {
    $("#actual-stock").keypress(function (data) {
        if (data.which != 8 && data.which != 0 && (data.which < 48 || data.which > 57)) {
            alert("harus angka");
        }
    });
}

$('#save-ok').click(function () {
    if (confirm("Anda yakin akan menambahkan adjusment ?") == true) {

        var validasi = true;
        var dataAdjusment = {};
        var table = $("#adjusment-temp tr");

        dataAdjusment.OutletID = $('#outlet-name').val();
        dataAdjusment.Note = $('.nicEdit-main').val();
       
        //samakan dengan nama list
        dataAdjusment.AdjusmentStockDetailViewModels = [];

        table.each(function () {
            var detail = {};
            //detail.VariantID = $(this).attr('id');

            detail.VariantID = $(this).find("td:nth-child(1)").find("input[type=hidden]").val();
            detail.InStock = $(this).find("td:nth-child(2)").text();
            detail.ActualStock = $(this).find("td:nth-child(3)").find("input[type=text]").val();



            //alert("nilai dari array : " + detail.VariantID + " , " + detail.InStock + " , " + detail.ActualStock);
            if (detail.ActualStock == "") {
                alert("aktual stock tidak boleh kosong");
                $(this).find("td:nth-child(3)").find("input[type=text]").focus();
                validasi = false;
                exit;
            } else if (detail.ActualStock < 0) {
                alert("aktual stock tidak boleh < 0");
                $(this).find("td:nth-child(3)").find("input[type=text]").focus();
                validasi = false;
                exit;
            } else if (detail.ActualStock == detail.InStock) {
                alert("aktual stock tidak boleh sama dengan instock");
                $(this).find("td:nth-child(3)").find("input[type=text]").focus();
                validasi = false;
                exit;
            } else {
                dataAdjusment.AdjusmentStockDetailViewModels.push(detail);
            }


        });

        if (validasi == true) {
            var jumlahAdjusment = $('#jumlah-adjusment').val();
            if (jumlahAdjusment == 0) {
                alert("anda belum memilih barang yang akan di adjusment");
            } else {
                SaveTempAdjusment(dataAdjusment);
            }

        }

    }

    //buat return false agar pengiriman dilakukan via ajax
    //return false;
})

//fungsi untuk ambil outlet petugas
function selectCurrentOutlet() {
    $.ajax({
        url: '../services/adjustmentservices.asmx/GetCurrentOutlet',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var listOutlet = '';
            $.each(data.d, function (index, item) {
                listOutlet += '<option value="' + item.OutletID + '" >' + item.OutletName + '</ option>';
            });
            $('#outlet-name').html(listOutlet);
        }
    });
}









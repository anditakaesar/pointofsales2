﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="index.aspx.cs" Inherits="pointofsales.Web.adjustment.index" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:.Adjustments.:</title>
    <link rel="stylesheet" href="../Content/bootstrap.css" />
    <link href="../Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="../Content/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.css" rel="stylesheet" />
    <link href="../Content/select2-4.0.3/dist/css/select2.css" rel="stylesheet" />
    <style>
        #add-adjusment, #save-ok, #save-cancel{
            float: right;
        }
        #save-cancel{
            margin-left: 5px;
        }
    </style>
   
</head>
<body>
   
    <div class="container">
        <!--form pencarian data-->
      <div class="row">
            <div class="panel panel-primary">
            <div class="panel-heading">
               <div class="row">
                        <div class="col-md-9">
                            <h3 class="panel-title">Cari data</h3>
                        </div>
                </div>
           </div>
        
            <div class="panel-body">
                <div class="row">
                    <form>
                    <div class="col-md-3">
                        <label for="tgl-mulai" class="control-label">Tgl mulai</label>
                        <input type="text" name="tgl-mulai" id="tgl-mulai" />             
                    </div>
                     <div class="col-md-1">
                        <label class="control-label">To</label>
                    </div>
                    <div class="col-md-3">
                        <label for="tgl-berakhir" class="control-label">Tgl akhir</label>
                        <input type="text" name="tgl-berakhir" id="tgl-berakhir" />             
                    </div>
                    <div class="col-md-1">
                        <button type="button" name="btn-cari" class="btn btn-success" id="btn-cari">Cari data</button>
                    </div>
                    </form>
                </div>
              </div>
            </div>
      </div>
        <!--end form pencarian data-->
       <!--tampil adjustment-->
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-9">
                            <h3 class="panel-title">Data adjustment</h3>
                        </div>
                        <div class="col-md-3">
                            <!--<input id="add-adjusment" class="btn btn-success" type="button" value="Create adjusment" />-->
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <!--data adjustment-->
                    <table id="table-data-adjustment" class="table table-stripped table-responsive">
                        <colgroup>
                            <col id="date" />
                            <col id="note" />
                            <col id="item" />
                            <col id="adjusment" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Note</th>
                                <th scope="col">Items</th>
                                <th scope="col">Adjusment</th>
                            </tr>
                        </thead>
                        <tbody id="data-adjusment">
                           
                        </tbody>
                    </table>
                    <!--end data adjustment-->
                </div>
            </div>
        </div>
       <!--end tampil adjustment-->

       
         <!--create adjustment-->
        <div class="row">
            <form>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="panel-title">Form create adjusment</h3>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <!--containt body-->
                    <div class="row">
                       <div class="col-md-12">
                            <label for="outlet-name" class="control-label">Outlet</label>
                            <select name="outlet" id="outlet-name" class="form-control">
                                <!--<option value="1">Outlet1</option>
                                <option value="2">Outlet2</option>
                                <option value="3">Outlet3</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="note-id" class="control-label">Note</label>
                            <textarea name="note" placeholder="catatan tentang adjusment" id="note-adjusment" class="form-control">

                            </textarea>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-md-12">
                            <label for="data-item" class="control-label">Item</label>
                            <input type="text" name="item" id="data-item" class="form-control" />
                        </div>
                    </div>-->

                    <div class="row">
                        <div class="col-md-12  form-group">
                            <label for="data-item" class="control-label">Item</label>
                            <select class="form-control select2" id="list-item-variant" multiple="multiple">
                               <!-- <optgroup label="Lamborghini">
                                     <option value="1">Lamborghini Merah</option>
                                     <option value="2">Lamborghini Jingga</option>
                                     <option value="3">Lamborghini Kuning</option>
                                </optgroup>
                                <optgroup label="Maria Marcedez">
                                     <option value="4">Maria Marcedez Hijau</option>
                                     <option value="5">Maria Marcedez Biru</option>
                                     <option value="6">Maria Marcedez Nila</option>
                                </optgroup>
                                <optgroup label="Bajaj">
                                     <option value="7">Bajaj Ungu</option>
                                </optgroup>-->
                           </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="table-adjusment">
                                <caption>Adjusment stock</caption>
                                <colgroup>
                                    <col id="itemvariant" />
                                    <col id="instock" />
                                    <col id="actualstock" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Item name</th>
                                        <th>In Stock</th>
                                        <th>Actual stock</th>
                                    </tr>
                                </thead>
                                <tbody id="adjusment-temp">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input id="save-cancel" class="btn btn-default" type="reset" value="Cancel" />
                            <input id="save-ok" class="btn btn-success" type="button" value="Simpan" />
                        </div>
                    </div>
                    <!--end containt body-->
                </div>
            </div>
            </form>
        </div>
       <!--end create adjustment-->
    </div>
    <script type="text/javascript" src="../Scripts/jquery-3.1.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>
     <script src="../Scripts/jquery.dataTables.js"></script>
     <script src="../Scripts/dataTables.bootstrap.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Content/select2-4.0.3/dist/js/select2.full.js"></script>
    <script type="text/javascript" src="../Scripts/libjs.js"> </script>
    <script type="text/javascript" src="../Scripts/adjusment.js"> </script>

</body>
</html>



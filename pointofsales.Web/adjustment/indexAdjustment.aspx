﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="indexAdjustment.aspx.cs" Inherits="pointofsales.Web.adjustment.indexAdjustment" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Adjustment</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/select2-4.0.3/dist/css/select2.css" rel="stylesheet" />
     <link href="../Content/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.css" rel="stylesheet" />
    <style>
         #add-adjusment, #save-ok, #save-cancel{
            float: right;
        }
        #save-cancel{
            margin-left: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--if dibawah berfungsi untuk mengecek apakah user sudah login, dan mempunyai hak akses untuk mengakses halaman ini--%>
    <% 
       if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {
        /*if (HttpContext.Current.Session["username"] != "")
        {*/
    %>
   
        <!--create adjustment-->
        <div class="row">
            <form class="frm-adjusment">
            <div class="panel panel-primary" id="form-adjusment">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="panel-title">Form create adjusment</h3>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <!--containt body-->
                    <div class="row">
                       <div class="col-md-12">
                            <label for="outlet-name" class="control-label">Outlet</label>
                            <select name="outlet" id="outlet-name" class="form-control">
                                <!--<option value="1">Outlet1</option>
                                <option value="2">Outlet2</option>
                                <option value="3">Outlet3</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="note-id" class="control-label">Note</label>
                            <textarea name="note" placeholder="catatan tentang adjusment" id="note-adjusment" class="form-control">

                            </textarea>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-md-12">
                            <label for="data-item" class="control-label">Item</label>
                            <input type="text" name="item" id="data-item" class="form-control" />
                        </div>
                    </div>-->

                    <div class="row">
                        <div class="col-md-12  form-group">
                            <label for="data-item" class="control-label">Item</label>
                            <select class="form-control select2" id="list-item-variant" multiple="multiple">
                              
                           </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="table-adjusment">
                                <caption>Adjusment stock</caption>
                                <colgroup>
                                    <col id="itemvariant" />
                                    <col id="instock" />
                                    <col id="actualstock" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Item name</th>
                                        <th>In Stock</th>
                                        <th>Actual stock</th>
                                    </tr>
                                </thead>
                                
                                <tbody id="adjusment-temp">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="0" class="form-control" id="jumlah-adjusment" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input id="save-cancel" class="btn btn-default" type="reset" value="Cancel" />
                            <input id="save-ok" class="btn btn-success" type="button" value="Simpan" />
                        </div>
                    </div>
                    <!--end containt body-->
                </div>
            </div>
            </form>
        </div>
       <!--end create adjustment-->

        <!--form pencarian data-->
      <div class="row">
            <div class="panel panel-primary">
            <div class="panel-heading">
               <div class="row">
                        <div class="col-md-9">
                            <h3 class="panel-title">Cari data</h3>
                        </div>
                </div>
           </div>
        
            <div class="panel-body">
                <div class="row">
                    <form>
                    <div class="col-md-3">
                        <label for="tgl-mulai" class="control-label">Tgl mulai</label>
                        <input type="text" name="tgl-mulai" id="tgl-mulai" />             
                    </div>
                     <div class="col-md-1">
                        <label class="control-label">To</label>
                    </div>
                    <div class="col-md-3">
                        <label for="tgl-berakhir" class="control-label">Tgl akhir</label>
                        <input type="text" name="tgl-berakhir" id="tgl-berakhir" />             
                    </div>
                    <div class="col-md-1">
                        <button type="button" name="btn-cari" class="btn btn-success" id="btn-cari">Cari data</button>
                    </div>
                    </form>
                </div>
              </div>
            </div>
      </div>
        <!--end form pencarian data-->
       <!--tampil adjustment-->
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-9">
                            <h3 class="panel-title">Data adjustment</h3>
                        </div>
                        <div class="col-md-3">
                            <input id="add-adjusment" class="btn btn-success" type="button" value="Create adjusment" />
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <!--data adjustment-->
                    <table id="table-data-adjustment" class="table table-stripped table-responsive">
                        <colgroup>
                            <col id="date" />
                            <col id="note" />
                            <col id="item" />
                            <col id="adjusment" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Note</th>
                                <th scope="col">Items</th>
                                <th scope="col">Adjusment</th>
                            </tr>
                        </thead>
                        <tbody id="data-adjusment">
                           
                        </tbody>
                    </table>
                    <!--end data adjustment-->
                </div>
            </div>
        </div>
       <!--end tampil adjustment-->

    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
     <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/mytext.js"></script>
    <script type="text/javascript" src="../Scripts/libjs.js"> </script>
    <script type="text/javascript" src="../Scripts/adjusment.js"> </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="indexTransferStock2.aspx.cs" Inherits="pointofsales.Web.transferstocks.indexTransferStock2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Transfer Stock</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/select2-4.0.3/dist/css/select2.css" rel="stylesheet" />
    <link href="../Content/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--if dibawah berfungsi untuk mengecek apakah user sudah login, dan mempunyai hak akses untuk mengakses halaman ini--%>
    <% 
       //if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
       // {
        /*if (HttpContext.Current.Session["username"] != "")
        {*/
    %>

    <!--create transfer stock-->
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>Transfer Stock</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <p class="help-block">CHOOSE OUTLET</p>
                                <hr />
                            </div>
                            <div class="form-group">
                                <label class="control-label">From</label>
                                <input type="hidden" id="id-outlet-from" value="" />
                                <input type="text" class="form-control" value="" readonly="true" id="name-outlet-from" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">To</label>
                                <select class="form-control select2" id="cbtransfer-stock-to">
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Note" id="note-stock-transfer"></textarea>
                            </div>
                            <div class="col-md-12">&nbsp;</div>
                            <div class="form-group">
                                <p class="help-block">TRANSFER STOCK</p>
                                <hr />
                            </div>
                            <div class="form-group">
                                <select class="select2 form-control" id="list-item-transfer-stock" multiple="multiple">
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-submit-transfer-stock" class="btn btn-success" style="width: 100%;" value="Submit Transfer Stock" />
                            </div>
                            <table class="table table-condensed table-bordered" id="table-transfer-stock">
                                <thead>
                                    <tr>
                                        <th class="text-center">Item-Variant</th>
                                        <th class="text-center">In Stock</th>
                                        <th class="text-center">Transfer Quantity</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-table-list-item">
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="col-md-3">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
    <!--end create transfer stock-->

    <!--form pencarian data-->
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="panel-title">Cari data</h3>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="row">
                    <form>
                        <div class="col-md-3">
                            <label for="tgl-mulai" class="control-label">Tgl mulai</label>
                            <input type="text" name="tgl-mulai" id="tgl-mulai" />
                        </div>
                        <div class="col-md-1">
                            <label class="control-label">To</label>
                        </div>
                        <div class="col-md-3">
                            <label for="tgl-berakhir" class="control-label">Tgl akhir</label>
                            <input type="text" name="tgl-berakhir" id="tgl-berakhir" />
                        </div>
                        <div class="col-md-1">
                            <button type="button" name="btn-cari" class="btn btn-success" id="btn-cari">Cari data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end form pencarian data-->
    <!--tampil adjustment-->
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="panel-title">Data adjustment</h3>
                    </div>
                    <div class="col-md-3">
                        <input id="add-adjusment" class="btn btn-success" type="button" value="Create adjusment" />
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!--data adjustment-->
                <table id="table-data-adjustment" class="table table-stripped table-responsive">
                    <colgroup>
                        <col id="date" />
                        <col id="note" />
                        <col id="item" />
                        <col id="adjusment" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Note</th>
                            <th scope="col">Items</th>
                            <th scope="col">Adjusment</th>
                        </tr>
                    </thead>
                    <tbody id="data-adjusment">
                    </tbody>
                </table>
                <!--end data adjustment-->
            </div>
        </div>
    </div>
    <!--end tampil adjustment-->

    <%//}
      //  else
      //  {
      //      Response.Redirect("../login/index.aspx");
      //  }
    %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
    <script src="../Scripts/jquery-ui.js"></script>
    <%-- <script src="../Scripts/mytext.js"></script>--%>
    <script type="text/javascript" src="../Scripts/libjs.js"> </script>
    <%--<script type="text/javascript" src="../Scripts/adjusment.js"> </script>--%>
    <script>
        //ambil id outlet dari session staff yang sedang login
        <% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString()); %>

        $(document).ready(function () {
            $('.select2').select2();
            loadOutlet();
            //load item butuh parameter IDoutlet staff yang login.
            loadItem(IDOutlet);
            loadOutletFrom(IDOutlet);
            $("#list-item-transfer-stock").val('');
            $("#note-stock-transfer").val('');
            $('#btn-submit-transfer-stock').attr("disabled", "disabled");
        });

        function loadOutletFrom(id) {
            $.ajax({
                url: '../services/outletservice.asmx/GetOutletById',
                data: '{"id":"' + id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (item) {
                    $("#id-outlet-from").val(item.d.ID);
                    $("#name-outlet-from").val(item.d.OutletName);
                }
            });
        }
        function loadOutlet() {
            $.ajax({
                url: '../services/TransferStockService.asmx/GetAllOutlet',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    var listProp = '';
                    $.each(dataAnu.d, function (index, item) {
                        if (item.ID != IDOutlet) {
                            listProp += '<option value="' + item.ID + '">' + item.OutletName + '</option>';
                        }
                    });
                    $('#cbtransfer-stock-to').html(listProp);
                }
            });
        }

        function loadItem(IDOutlet) {
            $.ajax({
                url: '../services/ItemServis.asmx/GetDataVar',
                data: '{IDOutlet:' + IDOutlet + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    var listProp = '';
                    $.each(dataAnu.d, function (index, item) {
                        listProp += '<option value="' + item.varID + '">' + item.Variant + '</option>';
                    });
                    $('#list-item-transfer-stock').html(listProp);
                }
            });
        }

        function GetVarById(id) {
            $.ajax({
                url: '../services/ItemServis.asmx/GetDataByIDVar',
                data: '{"ID":"' + id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    $.each(dataAnu.d, function (index, item) {
                        $('#table-transfer-stock > tbody:last-child')
                            .append('<tr id="baris' + item.varID + '">' +
                            '<td style="text-center">' + item.Variant + '</td>' +
                            '<td style="text-center" id="stock"' + item.varID + '>' + item.Stock + '</td>' +
                            '<td><input type="text" class="form-control" id="' + item.varID + '" onkeydown="testing2(this.id)"/>' +
                            '<input type="hidden" value="' + item.SKU + '" /></td>' +
                            '<td style="display:none;">' + item.varID + '</td>' +
                            '</tr>');
                    });
                }
            });
        }

        //awal fungsi untuk memasukkan variant dari dropdown ke dalam tabel di bawahnya
        var $topo = $('#list-item-transfer-stock')

        var valArray = ($topo.val()) ? $topo.val() : [];

        $topo.change(function () {
            var val = $(this).val(),
                numVals = (val) ? val.length : 0,
                changes;
            if (numVals != valArray.length) {
                var longerSet, shortSet;
                (numVals > valArray.length) ? longerSet = val : longerSet = valArray;
                (numVals > valArray.length) ? shortSet = valArray : shortSet = val;
                //create array of values that changed - either added or removed
                changes = $.grep(longerSet, function (n) {
                    return $.inArray(n, shortSet) == -1;
                });
                logChanges(changes, (numVals > valArray.length) ? 'selected' : 'removed');
            } else {
                // if change event occurs and previous array length same as new value array : items are removed and added at same time
                logChanges(valArray, 'removed');
                logChanges(val, 'selected');
            }
            valArray = (val) ? val : [];
        });

        function logChanges(array, type) {
            $.each(array, function (i, item) {
                //alert(item + ' was ' + type);
                //$('#log').append(item + ' was ' + type + '<br>');
                if (type == 'selected') {
                    GetVarById(item);
                } else {
                    $('#baris' + item).remove();
                }
                if ($topo.val() == '') {
                    $('#btn-submit-transfer-stock').attr("disabled", "disabled");
                } else {
                    $('#btn-submit-transfer-stock').removeAttr("disabled");
                }
                //alert($topo.val());
            });
        }

        $('button').click(function () {
            $('#log').empty()
        });
        //akhir fungsi untuk memasukkan variant dari dropdown ke dalam tabel di bawahnya


        function saveTransferStock(ts) {
            $.ajax({
                url: '../services/TransferStockService.asmx/SaveTransferStock',
                data: '{obj:' + JSON.stringify(ts) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset = utf-8;',
                success: function (data) {
                    if (data) {
                        alert('Save Success!');
                        location.reload();
                    }
                    else {
                        alert('Save Failed!');
                    }
                }
            });
        }

        //event saat tombol submit ditekan
        $('#btn-submit-transfer-stock').click(function () {
            var result = true;
            if ($("#note-stock-transfer").val() == '') {
                result = false;
                alert("Note Tidak Boleh Kosong!");
                $("#note-stock-transfer").focus();
            }
            if (result == true) {
                var table = $("#tbody-table-list-item tr");
                var stok = 0, qty = 0;
                var nama = "";
                table.each(function () {
                    if (result == true) {
                        nama = $(this).find("td:nth-child(1)").text();
                        stok = parseInt($(this).find("td:nth-child(2)").text());
                        qty = parseInt($(this).find("td:nth-child(3)").find("input[type=text]").val());
                        if (result && stok == 0) {
                            alert("Item " + nama + " tidak bisa ditransfer, karena stok nya 0!")
                            result = false;
                            $(this).find("td:nth-child(3)").find("input[type=text]").focus();
                        }
                        if (result && qty > stok) {
                            alert("Item " + nama + " tidak bisa ditransfer, quantity transfer lebih besar dari stok!")
                            result = false;
                            $(this).find("td:nth-child(3)").find("input[type=text]").focus();
                        }
                    }
                });
            }
            if (result == true) {
                if (confirm('Do you want to save this transfer?') == true) {
                    var TS = {};
                    var table = $("#tbody-table-list-item tr");
                    TS.FromOutlet = $('#id-outlet-from').val();
                    TS.ToOutlet = $('#cbtransfer-stock-to').val();
                    TS.Note = $('#note-stock-transfer').val();
                    TS.Details = [];
                    table.each(function () {
                        var detail = {};
                        detail.IDVariant = $(this).find("td:nth-child(4)").text();
                        detail.InStock = $(this).find("td:nth-child(2)").text();
                        detail.TrfQty = $(this).find("td:nth-child(3)").find("input[type=text]").val();
                        detail.SKU = $(this).find("td:nth-child(3)").find("input[type=hidden]").val();
                        TS.Details.push(detail);
                    });
                    saveTransferStock(TS);
                }
            }
        });

        function testing(id) {
            var data = "#" + id;
            $(data).keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        }

        function testing2(id) {
            var data = "#" + id;
            $(data).keyup(function (e) {
                if (isNaN($(data).val())) {
                    $(data).val('');
                }
            });
        }
    </script>
</asp:Content>

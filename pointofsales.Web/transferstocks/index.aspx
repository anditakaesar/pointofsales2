﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="pointofsales.Web.transferstocks.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <link href="../Content/bootstrap-theme.css" rel="stylesheet" />
    <%--<link href="../Content/chosen_v1.6.2/docsupport/style.css" rel="stylesheet" />
    <link href="../Content/chosen_v1.6.2/docsupport/prism.css" rel="stylesheet" />
    <link href="../Content/chosen_v1.6.2/chosen.css" rel="stylesheet" />--%>
    <link href="../Content/select2-4.0.3/dist/css/select2.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2>Transfer Stock</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">&nbsp;</div>
                            <div class="col-md-6">
                                <form>
                                    <div class="form-group">
                                        <p class="help-block">CHOOSE OUTLET</p>
                                        <hr />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">From</label>
                                        <input type="hidden" value="1" id="id-outlet-from" />
                                        <input type="text" class="form-control" value="Outlet1" readonly="true" id="outlet-from" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">To</label>
                                        <select class="form-control select2" id="cbtransfer-stock-to">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Note" id="note-stock-transfer"></textarea>
                                    </div>
                                    <div class="col-md-12">&nbsp;</div>
                                    <div class="form-group">
                                        <p class="help-block">TRANSFER STOCK</p>
                                        <hr />
                                    </div>
                                    <div class="form-group">
                                        <select class="select2 form-control" id="list-item-transfer-stock" multiple="multiple">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="button" id="btn-submit-transfer-stock" class="btn btn-success" style="width: 100%;" value="Submit Transfer Stock" />
                                    </div>
                                    <table class="table table-condensed table-bordered" id="table-transfer-stock">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Item-Variant</th>
                                                <th class="text-center">In Stock</th>
                                                <th class="text-center">Transfer Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody-table-list-item">
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div class="col-md-3">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="../Scripts/jquery-3.1.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>
    <%--<script src="../Content/chosen_v1.6.2/chosen.jquery.js"></script>
    <script src="../Content/chosen_v1.6.2/docsupport/prism.js"></script>--%>
    <script src="../Content/select2-4.0.3/dist/js/select2.full.js"></script>
    <script>
        //var config = {
        //  '.chosen-select': {},
        //'.chosen-select-deselect': { allow_single_deselect: true },
        //    '.chosen-select-no-single': { disable_search_threshold: 10 },
        //    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        //    '.chosen-select-width': { width: "95%" }
        //}
        //for (var selector in config) {
        //   $(selector).chosen(config[selector]);
        //}

        $(document).ready(function () {
            $('.select2').select2();
            loadOutlet();
            //load item butuh parameter IDoutlet staff yang login.
            loadItem(1);
            $("#list-item-transfer-stock").val('');
            $("#note-stock-transfer").val('');
        });

        function loadOutlet() {
            $.ajax({
                url: '../services/TransferStockService.asmx/GetAllOutlet',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    var listProp = '';
                    $.each(dataAnu.d, function (index, item) {
                        listProp += '<option value="' + item.ID + '">' + item.OutletName + '</option>';
                    });
                    $('#cbtransfer-stock-to').html(listProp);
                }
            });
        }

        function loadItem(IDOutlet) {
            $.ajax({
                url: '../services/ItemServis.asmx/GetDataVar',
                data: '{IDOutlet:'+IDOutlet+'}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    var listProp = '';
                    $.each(dataAnu.d, function (index, item) {
                        listProp += '<option value="' + item.varID + '">' + item.Variant + '</option>';
                    });
                    $('#list-item-transfer-stock').html(listProp);
                }
            });
        }

        function GetVarById(id) {
            $.ajax({
                url: '../services/ItemServis.asmx/GetDataByIDVar',
                data: '{"ID":"' + id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    $.each(dataAnu.d, function (index, item) {
                        $('#table-transfer-stock > tbody:last-child')
                            .append('<tr id=' + item.varID + '>'+
                            '<td style="text-center">' + item.Variant + '</td>' +
                            //'<td><input type="text" class="form-control" id="stock' + item.varID + '" value="'+item.Stock+'"/></td>' +
                            '<td style="text-center">' + item.Stock + '</td>'+
                            '<td><input type="number" class="form-control" />'+
                            '<input type="hidden" value="'+item.SKU+'" /></td>'+
                            '</tr>');
                    });
                }
            });
        }

        var $topo = $('#list-item-transfer-stock')

        var valArray = ($topo.val()) ? $topo.val() : [];

        $topo.change(function () {
            var val = $(this).val(),
                numVals = (val) ? val.length : 0,
                changes;
            if (numVals != valArray.length) {
                var longerSet, shortSet;
                (numVals > valArray.length) ? longerSet = val : longerSet = valArray;
                (numVals > valArray.length) ? shortSet = valArray : shortSet = val;
                //create array of values that changed - either added or removed
                changes = $.grep(longerSet, function (n) {
                    return $.inArray(n, shortSet) == -1;
                });
                logChanges(changes, (numVals > valArray.length) ? 'selected' : 'removed');
            } else {
                // if change event occurs and previous array length same as new value array : items are removed and added at same time
                logChanges(valArray, 'removed');
                logChanges(val, 'selected');
            }
            valArray = (val) ? val : [];
        });

        function logChanges(array, type) {
            $.each(array, function (i, item) {
                //alert(item + ' was ' + type);
                //$('#log').append(item + ' was ' + type + '<br>');
                if (type == 'selected') {
                    GetVarById(item);
                } else {
                    $('#' + item).remove();
                }
                //alert($topo.val());
            });
        }

        $('button').click(function () {
            $('#log').empty()
        });

        function saveTransferStock(ts) {
            $.ajax({
                url: '../services/TransferStockService.asmx/SaveTransferStock',
                data: '{obj:' + JSON.stringify(ts) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset = utf-8;',
                success: function (data) {
                    if (data) {
                        alert('Save Success!');
                        location.reload();
                    }
                    else {
                        alert('Save Failed!');
                    }
                }
            });
        }

        $('#btn-submit-transfer-stock').click(function () {
            if (confirm('Do you want to save this transfer?') == true) {
                var TS = {};
                var table = $("#tbody-table-list-item tr");
                TS.FromOutlet = $('#id-outlet-from').val();
                TS.ToOutlet = $('#cbtransfer-stock-to').val();
                TS.Note = $('#note-stock-transfer').val();
                TS.Details = [];
                table.each(function () {
                    var detail = {};
                    detail.IDVariant = $(this).attr('id');
                    detail.InStock = $(this).find("td:nth-child(2)").text();
                    detail.TrfQty = $(this).find("td:nth-child(3)").find("input[type=number]").val();
                    detail.SKU = $(this).find("td:nth-child(3)").find("input[type=hidden]").val();
                    TS.Details.push(detail);
                });
                saveTransferStock(TS);
            }
        });
    </script>
</body>
</html>

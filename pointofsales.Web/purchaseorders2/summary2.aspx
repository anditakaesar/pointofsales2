﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="summary2.aspx.cs" Inherits="pointofsales.Web.purchaseorders2.summary2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Summary</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/daterangepicker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--if dibawah berfungsi untuk mengecek apakah user sudah login, dan mempunyai hak akses untuk mengakses halaman ini--%>
    <% 
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {
    %>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>Summary</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                    
                        <div class="form-inline">
                            <div class="form-group">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date-range" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="searching">search</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    <input type="text" class="form-control" id="searching" placeholder="Search.." />
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary pull-right" disabled="disabled">Export</button>
                        </div>
                        <div class="form-group">
                            <br />
                        </div>
                        <table id="tbl-sum" class="table table-bordered">
                            <thead>
                                <tr style="background-color: #cccccc;">
                                    <th>Name - Variant</th>
                                    <th>Category</th>
                                    <th>Beginning</th>
                                    <th>Purchase Order</th>
                                    <th>Sales</th>
                                    <th>Transfer</th>
                                    <th>Adjusment</th>
                                    <th>Ending</th>
                                </tr>
                            </thead>
                            <tbody id="tbl-data-sum">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        
    </div>

    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
    <script src="../Scripts/moment.min.js"></script>
    <script src="../Scripts/moment-with-locales.min.js"></script>
    <script src="../Scripts/bootstrap-datetimepicker.min.js"></script>
    <script src="../Scripts/daterangepicker.js"></script>
    <script>
        <% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString()); %>
        
        function loadData(IDOutlet) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetSum',
                data: '{id:'+JSON.stringify(IDOutlet)+'}',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (dataSum) {
                    var cetakSum = "";
                    $.each(dataSum.d, function (index, item) {
                        cetakSum += '<tr><td>' + item.ItemName + ' ' + item.VariantName + '</td><td>'+item.CategoryName+'</td><td>' + item.Beginning + '</td><td>' + item.PurchaseOrder + '</td><td>' + item.Sales + '</td><td>' + item.Transfer + '</td><td>' + item.Adjusment + '</td><td>' + item.Ending + '</td></tr>';
                    });
                    $('#tbl-data-sum').html(cetakSum);
                }
            });
        }
        function searchSum(searchValue, IDOutlet) {
            //alert(OutletID);
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetSumBySearch',
                data: '{"search":"' + searchValue + '","id":"' + IDOutlet + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataSum) {
                    var cetakSum = "";
                    $.each(dataSum.d, function (index, item) {
                        cetakSum += '<tr><td>' + item.ItemName + ' ' + item.VariantName + '</td><td>' + item.CategoryName + '</td><td>' + item.Beginning + '</td><td>' + item.PurchaseOrder + '</td><td>' + item.Sales + '</td><td>' + item.Transfer + '</td><td>' + item.Adjusment + '</td><td>' + item.Ending + '</td></tr>';
                    });
                    $('#tbl-data-sum').html(cetakSum);
                }
            });
        }
        $('#searching').keypress(function () {
            var searchValue = $('#searching').val();
            searchSum(searchValue, IDOutlet);
        });
        $(document).ready(function () {
            loadData(IDOutlet);
        });
        $('#date-range').daterangepicker();
    </script>
</asp:Content>


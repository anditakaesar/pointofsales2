﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="confirm-po2.aspx.cs" Inherits="pointofsales.Web.purchaseorders2.confirm_po2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Confirm Purchase Order</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .input-po {
            width: 100%;
            border: none;
            border-color: none;
        }

        .tr-th-polos {
            border: none;
        }

        .button-po {
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            outline: none;
            border: 0;
            background: transparent;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--if dibawah berfungsi untuk mengecek apakah user sudah login, dan mempunyai hak akses untuk mengakses halaman ini--%>
    <% 
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {
    %>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>Confirm Purchase Order</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form>
                            <div class="form-group">
                                <button class="btn btn-default" type="button" id="resend" disabled="disabled">Resend</button>
                                <div class="dropdown pull-right">
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        More
                                <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <label class="btn" id="success">Mark as fulfilled </label>
                                            <span style="color: green" class="glyphicon glyphicon-ok-sign"></span></li>
                                        <li>
                                            <label class="btn" id="cancel">Cancel Order <span style="color: red" class="glyphicon glyphicon-minus-sign"></span></label>
                                        </li>
                                        <li><label class="btn" id="print">Print</label></li>
                                        <li><label class="btn" id="edit">Edit</label></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="outlet">Purchase Order Details</label>
                                <hr />
                                <div class="h4">Nama Suppliers </div>
                                <table class="table table-bordered">
                                    <tbody id="tbl-supplier"></tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <label for="supplier">Note</label>
                                <hr />
                                <p id="txt-note"></p>
                                <hr />
                                <p id="txt-detail-order"></p>
                            </div>
                            <div class="form-group">
                                <label for="add-po">Status Activity</label>
                                <hr />
                                <input type="text" class="form-control" id="txt-status" />
                            </div>
                            <div class="form-group">
                                <label for="purchase-order">Purchase Order</label>
                                <hr />
                                <table class="table table-bordered">
                                    <thead>
                                        <tr style="border: none;">
                                            <th>Item</th>
                                            <th>In Stock</th>
                                            <th>Order</th>
                                            <th>Unit Cost</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl-items">
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                TOTAL
                            </div>
                            <div class="form-group">
                                <button type="button" id="btn-done" class="btn btn-primary pull-right">Done</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        <%--modal untuk edit--%>
        <div id="modal-edit-po" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="outlet">Choose Outlet</label>
                            <hr />
                            <select id="select-outlet" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="supplier">Choose Supplier</label>
                            <hr />
                            <select id="select-supplier" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="modal-note">Note</label>
                            <hr />
                            <textarea rows="4" id="modal-note" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="add-po">Purchase Order</label>
                            <hr />
                            <div class="hidings">
                                <table class="table table-bordered" id="tbl-add-po">
                                    <thead>
                                        <tr style="border: none;">
                                            <th>Item</th>
                                            <th>In Stock</th>
                                            <th>Order</th>
                                            <th>Unit Cost</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl-modal-po">
                                    </tbody>
                                </table>
                            </div>
                            <label for="totals">Total</label><input id="total-po" type="hidden" value="Rp. " class="pull-right" />
                            <button type="button" class="form-control btn btn-primary" id="btn-edit-po">Save Puschase Order</button>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>

        <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
    <script src="https://mathias-bank.de/jQuery/jquery.getUrlParam.js"></script>
    <script>
        var dataId = $(document).getUrlParam("id");
        function getAllId(dataId) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetAllId',
                data: '{"id":"' + dataId + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        loadSupplier(item.SupplierID);
                        loadPO(dataId);
                        loadItem(dataId);
                        loadSupplierModal(item.SupplierID);;
                        loadOutletModal(item.OutletID);
                        loadEditItem(dataId);
                    });
                }
            });
        }
        $(document).ready(function () {
            getAllId(dataId);
            loadDataPO(dataId);
        });

        function loadSupplier(idsup) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetSupplierById',
                data: '{"id":"' + idsup + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var suplier = "";
                    $.each(dataPO.d, function (index, items) {
                        suplier += '<tr><td>' + items.Phone + '</td><td colspan="2">' + items.Email + '</td></tr><tr><td colspan="3">' + items.Address + '</td></tr><tr><td>' + items.DistrictID + '</td><td>' + items.DistrictID + '</td><td>' + items.PostalCode + '</td></tr>';
                    });
                    $('#tbl-supplier').html(suplier);
                }
            });
        }
        function loadPO(id) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetDetailConfirm',
                data: '{"id":"' + id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetak = "";
                    var not = "";
                    var stat = "";
                    $.each(dataPO.d, function (index, items) {
                        cetak += 'PO Number ' + items.OrderNo + ' <br>Created at : ' + formatJSONDate(items.CreatedOn).toDateString() + '<br>Created by : ' + items.CreatedBy + '<br>Email : ' + items.Email + '<br>Outlet Name : ' + items.OtlName + '<br>Phone : ' + items.OtlPhone + '<br>Address : ' + items.OtlAddress + '<br>';
                        not = items.Notes;
                        stat = 'At ' + formatJSONDate(items.CreatedOn).toDateString() + ' ' + items.OrderNo + 'is created';
                    });
                    $('#txt-detail-order').html(cetak);
                    $('#txt-note').html(not);
                    $('#modal-note').html(not);
                    document.getElementById('txt-status').value = stat;
                    //$('#txt-status').html(stat);
                }
            });
        }
        function loadItem(Id) {
            $.ajax({
                url: '../Services/PurchaseOrder.asmx/GetItemByPOId',
                data: '{"id":"' + Id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataItem) {
                    var listItem = "";
                    $.each(dataItem.d, function (index, item) {
                        listItem += '<tr id="item-' + item.IdItem + '"><td><input type="hidden" id="idvariant" value="' + item.IdVariant + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input class="input-po" type="text" id="jumlah-order" value="' + item.Order + '"></td><td>' + item.UnitCost + '</td><td>' + item.Total + '</td></tr>';
                    });
                    $('#tbl-items').html(listItem);
                    $('#tbl-modal-edit-po').html(listItem);
                }
            });
        }
        function loadEditItem(Id) {
            $.ajax({
                url: '../Services/PurchaseOrder.asmx/GetItemByPOId',
                data: '{"id":"' + Id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataItem) {
                    var listItem = "";
                    $.each(dataItem.d, function (index, item) {
                        listItem += '<tr id="item-' + item.IdItem + '"><td><input type="hidden" id="idvariant" value="' + item.IdVariant + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input class="input-po" type="text" id="jumlah-order" value="' + item.Order + '"></td><td><input class="input-po" type="text" id="jumlah-unitcost" value="' + item.UnitCost + '"></td><td><input class="input-po" type="text" id="jumlah-total" value="' + item.Total + '"></td></tr>';
                    });
                    $('#tbl-modal-po').html(listItem);
                }
            });
        }
        function statusSuccess() {
            var id = dataId;
            var order = $('#jumlah-order').val();
            var status = 1;
            $.ajax({
                url: '../services/PurchaseOrder.asmx/UpdateStatusPO',
                data: '{id:' + JSON.stringify(id) + ',stat:' + JSON.stringify(status) + ',order:' + JSON.stringify(order) + '}',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    alert("Status Berhasil diubah");
                }
            });
        }
        function statusCancel() {
            var id = dataId;
            var order = $('#jumlah-order').val();
            var status = 2;
            $.ajax({
                url: '../services/PurchaseOrder.asmx/UpdateStatusPO',
                data: '{id:' + JSON.stringify(id) + ',stat:' + JSON.stringify(status) + ',order:' + JSON.stringify(order) + '}',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    alert("Pembatalan Berhasil");
                    window.location.href = "purchase-order2.aspx";
                }
            });
        }
        function saveEdit() {
            var addPO = {};
            addPO.ID = dataId;
            addPO.OutletID = $("#select-outlet").val();
            addPO.SupplierID = $("#select-supplier").val();
            addPO.Notes = $("#modal-note").val();
            addPO.IdVariant = $("#idvariant").val();
            addPO.Quantity = $("#jumlah-order").val();
            addPO.UnitCost = $("#jumlah-unitcost").val();
            addPO.SubTotal = $("#jumlah-total").val();
            //addPO.CreatedBy = ;
            $.ajax({
                url: '../services/PurchaseOrder.asmx/EditTrxPO',
                data: '{add:' + JSON.stringify(addPO) + '}',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    alert("Transaksi berhasil disimpan.");
                }
            });
            //alert(addPO.Notes);
            $('#modal-edit-po').modal('hide');
        }
        function loadSupplierModal(idsup) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetSupplierById',
                data: '{"id":"' + idsup + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var suplier = "";
                    $.each(dataPO.d, function (index, items) {
                        suplier += '<option value="' + items.ID + '">' + items.Name + '</option>';;
                    });
                    $('#select-supplier').html(suplier);
                }
            });
        }
        function loadOutletModal(OutletID) {
            // parameter outlet by login
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetOutletById',
                data: '{id:' + JSON.stringify(OutletID) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakOutlet = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakOutlet += '<option value="' + item.ID + '">' + item.OutletName + '</option>';
                    });
                    $('#select-outlet').html(cetakOutlet);
                }
            });
        }
        function formatJSONDate(jsonDate) {
            var newDate = new Date(parseInt(jsonDate.substr(6)));
            return newDate;
        }
        $('#btn-edit-po').click(function () {
            saveEdit();
            //$('#modal-edit-po').modal('hide');
            //$('#popup-edit-po').modal('show');
        })
        $('#success').click(function () {
            var respond = confirm("Are you want to Save this order ?");
            if (respond == true) {
                statusSuccess();
            } else {
                false;
            }
        });
        $('#cancel').click(function () {
            var respond = confirm("Are you want to Cancel this order ?");
            if (respond == true) {
                statusCancel();
            } else {
                false;
            }
        })
        $('#edit').click(function () {
            $("#modal-edit-po").modal('show');
            //editPo();
        })
        $('#btn-done').click(function () {
            window.location.href = "purchase-order2.aspx";
        });
    </script>
</asp:Content>

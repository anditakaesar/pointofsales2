﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="create-po2.aspx.cs" Inherits="pointofsales.Web.purchaseorders2.create_po2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Create Purchase Order</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .input-po {
            width: 100%;
            border: none;
            border-color: none;
        }

        .tr-th-polos {
            border: none;
        }

        .button-po {
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            outline: none;
            border: 0;
            background: transparent;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--if dibawah berfungsi untuk mengecek apakah user sudah login, dan mempunyai hak akses untuk mengakses halaman ini--%>
    <% 
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {
    %>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>Create Purchase Order</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form>
                            <div class="form-group">
                                <label for="outlet">Choose Outlet</label>
                                <hr />
                                <select id="select-outlet" class="form-control">
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="supplier">Choose Supplier</label>
                                <hr />
                                <select id="select-supplier" class="form-control">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="note">Note</label>
                                <hr />
                                <textarea rows="4" placeholder="Notes" id="note" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="add-po">Purchase Order</label>
                                <hr />
                                <div class="hidings">
                                    <table class="table table-bordered" id="tbl-add-po">
                                        <thead>
                                            <tr style="border: none;">
                                                <th>Item</th>
                                                <th>In Stock</th>
                                                <th>Order</th>
                                                <th>Unit Cost</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbl-po">
                                        </tbody>
                                    </table>
                                </div>
                                <label for="totals">Total</label><input id="total-po" type="hidden" value="Rp. " class="pull-right" />
                                <button type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-add-po">Add Purchase Order</button>
                            </div>
                            <div class="form-group">
                                <br />
                            </div>
                            <div class="form-group pull-right">
                                <button type="button" class="btn btn-default" onclick="goBack()">Cancel</button>
                                <button type="button" class="btn btn-primary" onclick="saveItem()">Save</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        <%-- MODAL--%>
        <div id="modal-add-po" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" id="txt-search-item" />
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr style="border: none;">
                                    <th>Item</th>
                                    <th>In Stock</th>
                                    <th>Order</th>
                                    <th>Unit Cost</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id="tbl-modal-add-po">
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" onclick="callAdd()">Add</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
    <script>
        //ambil id outlet dari session staff yang sedang login
        <% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString() + ";");
           Response.Write("var IDStaff =" + HttpContext.Current.Session["IDStaff"].ToString() + ";"); %>
        // ini nanti diganti jadi dari Session
        var OutletID = IDOutlet;
        function loadOutlet(OutletID) {
            // parameter outlet by login
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetOutletById',
                data: '{id:' + JSON.stringify(OutletID) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakOutlet = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakOutlet += '<option value="' + item.ID + '">' + item.OutletName + '</option>';
                    });
                    $('#select-outlet').html(cetakOutlet);
                }
            });
        }
        function loadSupplier() {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetSupplier',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakSupplier = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakSupplier += '<option value="' + item.ID + '">' + item.Name + '</option>';
                    });
                    $('#select-supplier').html(cetakSupplier);
                }
            });
        }
        function goBack() {
            window.history.back();
        }
        function cobain(searchValue, OutletID) {
            //alert(OutletID);
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetItemSearch',
                data: '{"search":"' + searchValue + '","outlet":"'+OutletID+'"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (datacoba) {
                    var cetakcoba = "";
                    $.each(datacoba.d, function (index, item) {
                        cetakcoba += '<tr id="item-' + item.IdVariant + '"><td><input type="hidden" id="itemid" value="' + item.IdVariant + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input type="text" id="input-order" value=" " class="input-po"/></td><td><input type="text" id="input-unitcost" value=" " onchange="count()" class="input-po"/></td><td><input type="text" id="input-total" value=" " class="input-po" /></td></tr>';
                    });
                    $('#tbl-modal-add-po').html(cetakcoba);
                }
            });
        }
        function cobain1(Id) {
            $.ajax({
                url: '../Services/PurchaseOrder.asmx/GetItemById',
                data: '{"id":"' + Id + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataItem) {
                    var listItem = "";
                    $.each(dataItem.d, function (index, item) {
                        listItem += '<tr id="item-' + item.IdVariant + '"><td><input type="hidden" id="idvariant" value="' + item.IdVariant + '"/>' + item.NamaItem + ' ' + item.NamaVariant + '</td><td>' + item.Stock + '</td><td><input type="text" id="order-po" class="input-po"/></td><td><input type="text" id="unitcost-po" class="input-po"/></td><td><input type="text" id="total-po" class="input-po" /><button class="button-po" onclick="deletePo(' + item.IdVariant + ')"><span style="color:red" class="glyphicon glyphicon-remove-circle"></span></button></td></tr>';
                    });
                    var order = $('#input-order').val();
                    var unitcost = $('#input-unitcost').val();
                    var total = $('#input-total').val();
                    if (order < 0 || unitcost < 0) {
                        alert("Salah memasukkan harga. Mohon Teliti lagi!");
                        return false;
                    } else {
                        //$('#tbl-po').append(listItem);
                        alert("Berhasil ditambahkan");
                        return true;
                    }
                    $('#tbl-po').append(listItem);
                }
            });
            //var order = $('#input-order').val();
            //var unitcost = $('#input-unitcost').val();
            //var total = $('#input-total').val();
            //if (order < 0 || unitcost < 0) {
            //    alert("Salah memasukkan harga. Mohon Teliti lagi!")
            //    return false;
            //} else {
            var order = $('#input-order').val();
            var unitcost = $('#input-unitcost').val();
            var total = $('#input-total').val();
            $('#modal-add-po').modal('hide');
            
                document.getElementById('order-po').value = order;
                document.getElementById('unitcost-po').value = unitcost;
                document.getElementById('total-po').value = total;
            
            
        }
        function callAdd() {
            var id = $('#itemid').val();
            cobain1(id);
            $('#modal-add-po').modal('hide');
        }
        function count() {
            var cost = $('#input-unitcost').val();
            var order = $('#input-order').val();
            var total = cost * order;
            document.getElementById('input-total').value = total;
        }
        function deletePo(Id) {
            var itemID = '#item-' + Id.toString();
            $(itemID).remove();
        }
        function savePo() {

        }
        function saveItem() {
            var addPO = {};
            addPO.OutletID = $("#select-outlet").val();
            addPO.SupplierID = $("#select-supplier").val();
            addPO.Notes = $("#note").val();
            addPO.IdVariant = $("#idvariant").val();
            addPO.Quantity = $("#order-po").val();
            addPO.UnitCost = $("#unitcost-po").val();
            addPO.SubTotal = $("#total-po").val();
            addPO.CreatedBy = IDStaff;
            $.ajax({
                url: '../services/PurchaseOrder.asmx/AddTrxPo',
                data: '{add:' + JSON.stringify(addPO) + '}',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    alert("Transaksi berhasil disimpan.");
                    window.location.href = "confirm-po2.aspx?id=" + JSON.stringify(data.d);
                }
            });
        }
        function formatJSONDate(jsonDate) {
            var newDate = new Date(parseInt(jsonDate.substr(6)));
            return newDate;
        }
        $('#txt-search-item').keypress(function () {
            var searchValue = $('#txt-search-item').val();
            cobain(searchValue, OutletID);
        });
        $(document).ready(function () {
            loadOutlet(OutletID);
            loadSupplier();
            //cobain();
        });
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="purchase-order2.aspx.cs" Inherits="pointofsales.Web.purchaseorders2.purchase_order2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Purchase Order</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/daterangepicker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--if dibawah berfungsi untuk mengecek apakah user sudah login, dan mempunyai hak akses untuk mengakses halaman ini--%>
    <% 
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {
    %>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>Purchase Order</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date-range" />
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="select-status" onchange="loadSearch()">
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="searching">search</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                    <input type="text" class="form-control" id="searching" placeholder="Search.." />
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <a href="../purchaseorders2/create-po2.aspx" class="btn btn-primary ">Create Purchased Order</a>
                                <button type="button" class="btn btn-primary-spacing btn-primary" disabled="disabled">Export</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <br />
                    </div>
                    <div class="col-md-12">
                        <table id="tbl-po" class="table table-bordered">
                            <thead style="border: none;">
                                <tr style="background-color: #cccccc;">
                                    <th>Date</th>
                                    <th>Supplier</th>
                                    <th>Order No.</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="tbl-data-po">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
    <script src="../Scripts/moment.min.js"></script>
    <script src="../Scripts/moment-with-locales.min.js"></script>
    <script src="../Scripts/bootstrap-datetimepicker.min.js"></script>
    <script src="../Scripts/daterangepicker.js"></script>
    <script>
        <% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString() + ";");
           Response.Write("var IDStaff =" + HttpContext.Current.Session["IDStaff"].ToString() + ";"); %>
        function loadData(IDOutlet) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetData',
                data: '{id:'+IDOutlet+'}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakTabel = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakTabel += '<tr><td>' + formatJSONDate(item.CreatedOn).toDateString() + '</td><td>' + item.Name + '</td><td>' + item.OrderNo + '</td><td>' + item.SubTotal + '</td><td>' + item.StatusName + '</td></tr>';
                    });
                    $('#tbl-data-po').html(cetakTabel);
                }
            });
        }
        function loadDataSearch(searchValue, IDOutlet) {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetDataBySearch',
                data: '{"search":"'+searchValue+'","id":"'+IDOutlet+'"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakTabel = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakTabel += '<tr><td>' + formatJSONDate(item.CreatedOn).toDateString() + '</td><td>' + item.Name + '</td><td>' + item.OrderNo + '</td><td>' + item.SubTotal + '</td><td>' + item.StatusName + '</td></tr>';
                    });
                    $('#tbl-data-po').html(cetakTabel);
                }
            });
        }
        function loadSearch() {
            var strid = $('#select-status').val();
            var id = parseInt(strid);
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetStatusById',
                data: '{id:' + JSON.stringify(id) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakTabel = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakTabel += '<tr><td>' + formatJSONDate(item.CreatedOn).toDateString() + '</td><td>' + item.Name + '</td><td>' + item.OrderNo + '</td><td>' + item.SubTotal + '</td><td>' + item.StatusName + '</td></tr>';
                    });
                    $('#tbl-data-po').html(cetakTabel);
                }
            });
        }
        function loadStatus() {
            $.ajax({
                url: '../services/PurchaseOrder.asmx/GetStatus',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataPO) {
                    var cetakStatus = "";
                    $.each(dataPO.d, function (index, item) {
                        cetakStatus += '<option id="pilihstatus" value="' + item.ID + '">' + item.StatusName + '</option>';
                    });
                    $('#select-status').html(cetakStatus);
                }
            });
        }
        $(document).ready(function () {
            loadData(IDOutlet);
            loadStatus();
            loadSearch();
        });
        //formatJSONDate(item.CreatedOn).toDateString()
        function formatJSONDate(jsonDate) {
            var newDate = new Date(parseInt(jsonDate.substr(6)));
            return newDate;
        }
        $('#searching').keypress(function () {
            var searchValue = $('#searching').val();
            loadDataSearch(searchValue, IDOutlet);
        });
        $('#date-range').daterangepicker();
    </script>
</asp:Content>

﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for TransferStockService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TransferStockService : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<Outlet> GetAllOutlet()
        {
            return TransferStockDAL.GetAllOutlet();
        }

        [WebMethod]
        public bool SaveTransferStock(tempTransferStock obj)
        {
            return TransferStockDAL.SaveTransferStock(obj);
        }

        [WebMethod]
        public List<TransferStockViewModel> GetAllTransferStockByIdOutlet(int id)
        {
            return TransferStockDAL.GetAllTransferStockByIdOutlet(id);
        }
        
        [WebMethod]
        public List<Detail> GetAllTransferStockDetailById(int id)
        {
            return TransferStockDAL.GetAllTransferStockDetailById(id);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.DAL;
using pointofsales.Model;
using pointofsales.DAL.ItemModel;


namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for ItemServis
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ItemServis : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        
        // Git Checkout
        // [WebMethod]
        // public List<ItemDAL> GetLayoutItem(string search)
        // {
        //    return ItemDAL.GetLayoutItem(search);
        // }

        //[WebMethod]
        //public List<ItemDAL> GetLayoutItem(string search, string IDOutlet)
        //{
        //    return ItemDAL.GetLayoutItem(search, int.Parse(IDOutlet));
        //}
        
        //[WebMethod]
        //public List<ItemDAL> GetDataByID(string search, string IDOutlet)
        //{
        //    return ItemDAL.GetDataByID(int.Parse(search), int.Parse(IDOutlet));
        //}
        //[WebMethod]
        //public List<ItemDAL> GetLayoutItembyCat(string search, string IDOutlet)
        //{
        //    return ItemDAL.GetLayoutItembyCat(search, int.Parse(IDOutlet));
        //}

        //[WebMethod]
        //public List<ItemDAL> GetDataByIDVar(string ID, string IDOutlet)
        //{
        //    return ItemDAL.GetDataByIDVar(int.Parse(ID), int.Parse(IDOutlet));
        //}

        [WebMethod]
        public List<ItemDAL> GetLayoutItem(string search)
        {
            return ItemDAL.GetLayoutItem(search);
        }

        [WebMethod]
        public List<ItemDAL> GetDataByID(string search, string IDOutlet)
        {
            return ItemDAL.GetDataByID(int.Parse(search), int.Parse(IDOutlet));
        }
        [WebMethod]
        public List<ItemDAL> GetLayoutItembyCat(string search)
        {
            return ItemDAL.GetLayoutItembyCat(search);
        }

        [WebMethod]
        public List<ItemDAL> GetDataByIDVar(string ID)
        {
            return ItemDAL.GetDataByIDVar(int.Parse(ID));
        }

        [WebMethod]
        public bool SaveItem(ItemSave Item)
        {
            return ItemDAL.SaveItem3(Item);
        }

        [WebMethod]
        public bool UpdateItem(ItemSave Item)
        {
            return ItemDAL.UpdateItem(Item);
        }

        [WebMethod]
        public bool DeleteVariant(string ID)
        {
            return ItemDAL.DeleteVariant(int.Parse(ID));
        }

        //untuk load item - variant berdasarkan id outlet yang login by Toni
        [WebMethod]
        public List<ItemDAL> GetDataVar(int IDOutlet)
        {
            return ItemDAL.GetDataVar(IDOutlet);
        }
    }
}

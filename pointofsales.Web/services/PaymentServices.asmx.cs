﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.Model;
using pointofsales.DAL;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for PaymentServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PaymentServices : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public bool SavePay(pointofsales.Model.Payment Pay, List<PaymentDetail> PDs)
        {
            return PaymentDAL.SavePay(Pay, PDs);
        }
        
        [WebMethod]
        public List<Customer> GetCustemer(String Search)
        {
            return CostemerDAL.GetCustomer(Search);
        }
        
        [WebMethod]
        public bool SaveCustemer(Customer Search)
        {
            return CostemerDAL.SaveCustemer(Search);
        }

    }
}

﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
//penambahan library
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for loginservices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class loginservistoni : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        // Edit, LoginDAL sudah Ada
        [WebMethod(EnableSession = true)]
        public Boolean CekUser(string username, string password)
        {
            return LoginDALToni.CekUser(username, password);
        }

        [WebMethod]
        public Boolean CekPassUser(int ID, string Pass)
        {
            return LoginDALToni.CekPassUser(ID, Pass);
        }

        [WebMethod]
        public Boolean UbahPassUser(int ID, string Pass)
        {
            return LoginDALToni.ChangePass(ID, Pass);
        }

        [WebMethod(EnableSession = true)]
        public bool Logout()
        {
            return LoginDALToni.RemoveSession();
        }

        [WebMethod(EnableSession = true)]
        public LoginDALToni.UserLogin CekUser2(string username, string password)
        {
            return LoginDALToni.CekUser2(username, password);
        }
    }
}

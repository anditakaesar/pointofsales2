﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.Model;
using pointofsales.DAL;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for lokasiservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class lokasiservice : System.Web.Services.WebService
    {
        // Provinsi Service
        [WebMethod]
        public List<Province> GetProvinsi()
        {
            return LokasiDAL.GetProvinsi();
        }

        [WebMethod]
        public Province GetProvinsiById(string id)
        {
            return LokasiDAL.GetProvinsiById(int.Parse(id));
        }

        [WebMethod]
        public List<Province> SearchProvinsi(string search)
        {
            return LokasiDAL.SearchProvinsi(search);
        }

        // Kota Service
        [WebMethod]
        public List<Region> GetKota()
        {
            return LokasiDAL.GetKota();
        }

        [WebMethod]
        public Region GetKotaById(string id)
        {
            return LokasiDAL.GetKotaById(int.Parse(id));
        }

        [WebMethod]
        public List<Region> SearchKota(string search)
        {
            return LokasiDAL.SearchKota(search);
        }

        [WebMethod]
        public List<Region> GetKotaByProvId(string id)
        {
            return LokasiDAL.GetKotaByProvId(int.Parse(id));
        }

        // Kecamatan
        [WebMethod]
        public List<District> GetKecamatan()
        {
            return LokasiDAL.GetKecamatan();
        }

        [WebMethod]
        public District GetKecamatanById(string id)
        {
            return LokasiDAL.GetKecamatanById(int.Parse(id));
        }

        [WebMethod]
        public List<District> SearchKecamatan(string search)
        {
            return LokasiDAL.SearchKecamatan(search);
        }

        [WebMethod]
        public List<District> GetKecamatanByKotaId(string id)
        {
            return LokasiDAL.GetKecamatanByKotaId(int.Parse(id));
        }

        [WebMethod]
        public string GetKecamatanNameById(string id)
        {
            return LokasiDAL.GetNamaKecamatanById(int.Parse(id));
        }
    }
}

﻿using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.DAL;
using pointofsales.DAL.ViewModel;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for categoryservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class categoryservice : System.Web.Services.WebService
    {
        [WebMethod]
        public List<Category> GetSemuaCategory()
        {
            return CategoryDAL.GetCategory();
        }

        [WebMethod]
        public List<Category> GetCategoryById(string id)
        {
            return CategoryDAL.GetCategory(int.Parse(id));
        }

        [WebMethod]
        public List<Category> SearchCategory(string key) // true untuk yang tidak dihapus
        {
            return CategoryDAL.SearchCategory(key, true);
        }

        [WebMethod]
        public bool AddCategory(Category category)
        {
            if (CategoryDAL.InsertCategory(category))
                return true;
            else
                return false;
        }

        [WebMethod]
        public bool UpdateCategory(Category category)
        {
            if (CategoryDAL.UpdateCategory(category))
                return true;
            else
                return false;
        }

        [WebMethod]
        public bool DeleteCategory(string id)
        {
            if (CategoryDAL.DeleteCategory(int.Parse(id)))
                return true;
            else
                return false;
        }

        // Get Category dengan View Model -> agar item terhitung dan Enabled = true
        [WebMethod]
        public List<CategoryViewModel> GetCategoryVM()
        {
            return CategoryDAL.GetCategoryCount(true);
        }

        // Untuk Item yang tidak memiliki Category
        [WebMethod]
        public List<Item> GetSemuaItem()
        {
            return CategoryDAL.GetAllItem();
        }

        // Assigning Category pada Item
        [WebMethod]
        public bool UpdateItem(Item item)
        {
            return CategoryDAL.UpdateItem(item);
        }

        // Search Item
        [WebMethod]
        public List<Item> SearchItem(string key)
        {
            return CategoryDAL.SearchItem(key);
        }

        // Get Item Exclude Category
        [WebMethod]
        public List<Item> GetItemExcludeCategory(string id)
        {
            return CategoryDAL.GetItemExcludeCategory(int.Parse(id));
        }

        [WebMethod]
        public bool UpdateCategoryOfItem(UpdateCategoryVM update)
        {
            return CategoryDAL.UpdateCategoryOfItem(update);
        }
    }
}

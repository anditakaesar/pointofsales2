﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.DAL;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for utilitiservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class utilitiservice : System.Web.Services.WebService
    {
        [WebMethod]
        public string SHA1(string key)
        {
            return Utility.Hash(key);
        }
    }
}

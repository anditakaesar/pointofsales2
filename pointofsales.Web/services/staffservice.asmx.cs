﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for staffservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class staffservice : System.Web.Services.WebService
    {
        [WebMethod]        
        public List<Employee> GetSemuaStaff()
        {
            return EmployeeDAL.GetEmployee();
        }

        [WebMethod]
        public Employee GetStaffById(string id)
        {
            return EmployeeDAL.GetEmployee(int.Parse(id));
        }

        [WebMethod]
        public bool AddEmployee(Employee employee)
        {
            return EmployeeDAL.InsertEmployee(employee);
        }

        [WebMethod]
        public bool UpdateEmployee(Employee employee)
        {
            return EmployeeDAL.UpdateEmployee(employee);
        }

        [WebMethod]
        public List<Employee> SearchEmployee(string key)
        {
            return EmployeeDAL.SearchEmployee(key);
        }

        // Adding Load Role
        [WebMethod]
        public List<Role> GetRoles()
        {
            return EmployeeDAL.GetRoles();
        }

        // Load role by Id
        [WebMethod]
        public Role GetRoleById(string id)
        {
            return EmployeeDAL.GetRoleById(int.Parse(id));
        }
    }
}

﻿using pointofsales.DAL;
using pointofsales.Model;
using pointofsales.DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for PurchaseOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PurchaseOrder : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public List<GetAllVM> GetData()
        {
            return PurchaseOrderDAL.GetData();
        }
        [WebMethod]
        public List<GetAllVM> GetDataBySearch(string search, int id)
        {
            return PurchaseOrderDAL.GetDataBySearch(search, id);
        }
        [WebMethod]
        public List<Outlet> GetOutletById(int id)
        {
            return PurchaseOrderDAL.GetOutletById(id);
        }
        //[WebMethod]
        //public List<Outlet> GetOutlet()
        //{
        //    return PurchaseOrderDAL.GetOutlet();
        //}
        [WebMethod]
        public List<Outlet> GetOutletByPOId(int id) 
        {
            return PurchaseOrderDAL.GetOutletByPOId(id);
        }
        [WebMethod]
        public List<Supplier> GetSupplier()
        {
            return PurchaseOrderDAL.GetSupplier();
        }
        [WebMethod]
        public List<PurchaseOrderStatus> GetStatus()
        {
            return PurchaseOrderDAL.GetStatus();
        }
        [WebMethod]
        public List<Supplier> GetSupplierById(int id)
        {
            return PurchaseOrderDAL.GetSupplierById(id);
        }
        [WebMethod]
        public List<Supplier> GetSupplierById2(int id)
        {
            return PurchaseOrderDAL.GetSupplierById2(id);
        }
        [WebMethod]
        public List<StatusVM> GetStatusById(int id)
        {
            return PurchaseOrderDAL.GetStatusById(id);
        }
        [WebMethod]
        public List<SummaryVM> GetSum(int id)
        {
            return PurchaseOrderDAL.GetSum(id);
        }
        [WebMethod]
        public List<SummaryVM> GetSumBySearch(string search, int id)
        {
            return PurchaseOrderDAL.GetSumBySearch(search, id);
        }
        [WebMethod]
        public List<ItemVM> GetForAddPO()
        {
            return PurchaseOrderDAL.GetForAddPO();
        }
        [WebMethod]
        public List<ItemVM> GetItemSearch(string search, int outlet)
        {
            return PurchaseOrderDAL.GetItemSearch(search, outlet);
        }
        [WebMethod]
        public List<ItemVM> GetItemById(int id)
        {
            return PurchaseOrderDAL.GetItemById(id);
        }
        [WebMethod]
        public List<GetAllVM> GetAllId(int id)
        {
            return PurchaseOrderDAL.GetAllId(id);
        }
        [WebMethod]
        public int AddTrxPo(PurchaseOrderVM add)
        {
            return PurchaseOrderDAL.AddTrxPo(add);
        }
        [WebMethod]
        public bool EditTrxPO(PurchaseOrderVM add)
        {
            return PurchaseOrderDAL.EditTrxPO(add);
        }
        [WebMethod]
        public List<ConfirmVM> GetDetailConfirm(int id)
        {
            return PurchaseOrderDAL.GetDetailConfirm(id);
        }
        [WebMethod]
        public List<ItemVM> GetItemByPOId(int id)
        {
            return PurchaseOrderDAL.GetItemByPOId(id);
        }
        [WebMethod]
        public bool UpdateStatusPO(int id, int stat, int order)
        {
            return PurchaseOrderDAL.UpdateStatusPO(id,stat, order);
        }
        [WebMethod]
        public string RandomString(int length)
        {
            return PurchaseOrderDAL.RandomString(length);
        }
    }
}

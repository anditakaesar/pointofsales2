﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAppRole.Master" AutoEventWireup="true" CodeBehind="indexPayment.aspx.cs" Inherits="pointofsales.Web.home.indexPayment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        if (HttpContext.Current.Session["IDRole"] != null || HttpContext.Current.Session["IDRole"].ToString() != "1")
        {
    %>
    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1>Selamat datang</h1>
        <p>Silakan pilih menu cashier di atas</p>
    </div>
    <%}
    else
    {
        Response.Redirect("../login/index.aspx");
    }
    %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavascript" runat="server">
    <script type="text/javascript">
        <% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString()); %>

    </script>
</asp:Content>

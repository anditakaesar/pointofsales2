﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="index2.aspx.cs" Inherits="pointofsales.Web.home.index2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Home</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {
        /*if (HttpContext.Current.Session["username"] != "")
        {*/
            NameValueCollection menuBackOffice = new NameValueCollection()
{
    { "Home", "home" },
    { "Transfer Stock", "transferstock" }
};
    %>
    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1>Point Of Sales web application</h1>
        <p>Ini adalah halaman default login untuk BackOffice Role</p>
        <p>Silakan pilih menu di atas</p>
        <p>
            
        </p>
    </div>
    <%}
    else
    {
        Response.Redirect("../login/index.aspx");
    }
    %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server"></asp:Content>

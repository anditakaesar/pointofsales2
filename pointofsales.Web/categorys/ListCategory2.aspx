﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="ListCategory2.aspx.cs" Inherits="pointofsales.Web.categorys.ListCategory2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/select2-4.0.3/dist/css/select2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        // Cek USER jika memiliki IDRole 1 dan 3 yaitu (Backend dan SuperAdmin)
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {

    %>
    <div class="row">
        <h3>Category List</h3>
    </div>
    <br />
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <input type="text" class="form-control" id="category-search" placeholder="Search..." style="width: 50%" />
        </div>
        <div class="col-md-6 col-lg-6">
            <button type="button" class="btn btn-success pull-right" id="category-btncreate">Create Category</button>
        </div>
    </div>
    <br />
    <div class="row">
        <table class="table table-striped" id="table-category">
            <thead>
                <tr>
                    <th>Category Name</th>
                    <th>Item Stocks</th>
                    <%--<th>CreatedOn</th>--%>
                    <th></th>
                </tr>
            </thead>
            <tbody id="table-category-body"></tbody>
            <tfoot>
                <tr>
                    <th>Category Name</th>
                    <th>Item Stocks</th>
                    <%--<th>CreatedOn</th>--%>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- Modal Untuk Add Category -->
    <div class="modal fade" tabindex="-1" role="dialog" id="category-modal-add">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Add Category</h4>
                    <hr />
                    <div>
                        <input type="text" class="form-control" id="category-modal-input" placeholder="Add Category Name..." />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <%--<button type="button" class="btn btn-primary" id="category-modal-btnsavetoall">Save To All Outlets</button>--%>
                    <button type="button" class="btn btn-primary" id="category-modal-btnsave">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Untuk Add Category -->
    <!-- Modal Untuk Edit Category -->
    <div class="modal fade" tabindex="-1" role="dialog" id="category-modal-edit">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Edit Category</h4>
                    <hr />
                    <div>
                        <input type="text" class="form-control" id="category-modal-edit-input" placeholder="Edit..." />
                        <input type="hidden" id="category-modal-edit-id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" id="category-modal-edit-btndelete"><span class="glyphicon glyphicon-trash"></span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="category-modal-edit-btnsave">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Untuk Edit Category -->
    <!-- Modal Untuk Assign Category -->
    <div class="modal fade" tabindex="-1" role="dialog" id="category-assign">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Assign Category: <span id="category-assign-name"></span></h4>
                    <hr />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <input type="hidden" id="category-assign-id" />
                            <%--<input type="text" class="form-control" id="search-item" placeholder="Find item..." />--%>
                            <select class="select2 form-control" id="search-item" multiple="multiple" style="width: 100%;"></select>
                        </div>
                    </div>
                    <%--<br />
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <table class="table table-bordered">
                                    <tbody id="assign-item-table"></tbody>
                                </table>
                            </div>
                        </div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="category-btn-assign">Assign</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Untuk Assign Category -->
    <!-- Modal Pesan -->
    <div class="modal fade" tabindex="-1" role="dialog" id="category-pesan">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <span id="category-pesan-msg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Pesan -->
    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footerBuatJavascript" runat="server">
    <script type="text/javascript">

        <% 
        Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString() + ";");
        Response.Write("var IDStaff =" + HttpContext.Current.Session["IDStaff"].ToString() + ";");
        %>
        // Fungsi Load semua Category: Dengan Enabled TRUE
        function LoadCategory(callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/GetCategoryVM',
                //url: '../services/categoryservice.asmx/GetSemuaCategory',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data);
                }
            });
        }

        // Fungsi Load Item GetItemExcludeCategory
        function LoadItemUncategorized(catId, callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/GetItemExcludeCategory',
                data: '{"id":' + catId + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataItem) {
                    callback(dataItem.d);
                }
            });
        }

        // Fungsi Search Item by Name
        function SearchItem(searckey, callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/SearchItem',
                data: '{ "key":' + JSON.stringify(searchkey) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataItem) {
                    callback(dataItem.d);
                }
            });
        }

        // Fungsi fill Item pada table
        function FillItemTable(dataItem) {
            var listItem = '';
            $.each(dataItem, function (i, item) {
                listItem += '<tr>'
                    + '<td>' + item.Name
                    + '<button onclick="AddToCategory(this.value)" type="button" class="btn pull-right" value="'
                    + item.ID
                    + '"><span class="glyphicon glyphicon-plus-sign"></span></button></td>'
                    + '</tr>';
            });
            $('#assign-item-table').html(listItem);
        }


        // Fungsi Load Category dengan ID tertentu
        function LoadCategorByID(catID, callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/GetCategoryById',
                data: '{ "id" : ' + catID + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data);
                }
            });
        }

        // Fungsi search Category by Name
        function SearchCategory(searchkey, callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/SearchCategory',
                data: '{ "key":' + JSON.stringify(searchkey) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data);
                }
            })
        }

        // Load Table #table-category-body
        function FillCategoryTable(data) {
            var listCategory = '';
            $.each(data.d, function (index, item) {
                listCategory += '<tr>'
                  + '<td>' + item.Name + '</td>'
                  + '<td>' + item.ItemCount + '</td>'
                  //+ '<td>' + formatJSONDate(item.CreatedOn).toDateString() + '</td>'
                  + '<td><button onclick="AssignCategory(this.value)" type="button" class="btn btn-sm btn-default" value="' + item.ID + '">Assign To Item</button>'
                  + '<button onclick="EditCategory(this.value)" type="button" class="btn btn-sm btn-primary pull-right" value="' + item.ID + '">Edit</button>'
                  + '</tr>'
            });
            $('#table-category-body').html(listCategory);
            //$('#table-category').DataTable();
        }

        // Save Category, ADD Category
        function SaveCategory(catName, callback) {
            var category = {};
            category.Name = catName;
            category.CreatedBy = IDStaff;
            $.ajax({
                url: '../services/categoryservice.asmx/AddCategory', // implement Add
                data: '{ category : ' + JSON.stringify(category) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data);
                }
            });
        }

        // Update Category
        function UpdateCategory(category, callback) {
            var catToUpdate = category;
            $.ajax({
                url: '../services/categoryservice.asmx/UpdateCategory', // implement Update
                data: '{ category : ' + JSON.stringify(catToUpdate) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data);
                }
            })
        }

        // Hapus Category
        function DeleteCategory(catId, callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/DeleteCategory', // implement Pseudo-DELETE
                data: '{ "id" : ' + catId + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data);
                }
            })
        }

        // show message modal
        function TampilkanPesanCategory(pesan) {
            $('#category-pesan-msg').html(pesan);

            $('#category-pesan').modal('show');

            LoadCategory(FillCategoryTable);
        }

        // Utility: format Date dari .NET
        function formatJSONDate(jsonDate) {
            var newDate = new Date(parseInt(jsonDate.substr(6)));
            return newDate;
        }

        /* ASSIGN BUTTON */
        function AssignCategory(catID) {
            LoadCategorByID(catID, function (data) {
                var nama = '';
                // var id = '';
                $.each(data.d, function (index, item) {
                    nama = item.Name;
                    id = item.ID;
                });
                // insert to modal assign input form  
                $('#category-assign-name').html(nama);
                $('#category-assign-id').val(id);

                LoadItemUncategorized(catID, FillSelectItem);

                $('#category-assign').modal('show');
            })
        }

        // Fill Select Item
        function FillSelectItem(dataItem) {
            var listItem = '';
            $.each(dataItem, function (index, item) {
                listItem += '<option value="' + item.ID + '">' + item.Name + '</option>';
            });
            $('#search-item').html(listItem);
        }

        // Utility: Parsing Array karena datanya berupa string array
        // -- Eror ketika JSON.stringify -> recursive
        function ParseIntArray(array) {
            var temp = [];
            if (Array.isArray(array)) {
                for (i = 0; i < array.length; i++) {
                    temp.push(parseInt(array[i]));
                }
            }
            return temp;
        }

        // Assign Button Trigger
        $('#category-btn-assign').click(function () {
            // array id item
            var values = $('#search-item').val();
            //// object to update
            var UpdateCategoryVM = {};
            // id category
            UpdateCategoryVM.CategoryID = parseInt($('#category-assign-id').val());
            UpdateCategoryVM.ItemsToUpdate = ParseIntArray(values);

            //Update
            UpdateCategoryOfItem(UpdateCategoryVM, function (hasil) {
                var pesan = "Item Gagal Di Update"
                if (hasil)
                    pesan = "Item Berhasil Di Update"

                LoadCategory(FillCategoryTable);
                TampilkanPesanCategory(pesan);

                $('#category-assign').modal('hide');

            });
        });


        // Fungsi Update Category of item
        function UpdateCategoryOfItem(update, callback) {
            $.ajax({
                url: '../services/categoryservice.asmx/UpdateCategoryOfItem',
                data: '{ update : ' + JSON.stringify(update) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    callback(data.d);
                }
            })
        }

        /* ASSIGN BUTTON */

        /* CREATE BUTTON */
        $('#category-btncreate').click(function () {
            $('#category-modal-add').modal('show');
        })

        // #category-modal-btnsave -> save category ke CURRENT outlet
        $('#category-modal-btnsave').click(function () {
            // if validasi nama apakah sudah ada?
            // validasi kalau nama 
            var catName = $('#category-modal-input').val();
            if (catName.length >= 5) {
                SaveCategory(catName, function (data) {
                    var pesan = "Category gagal dibuat";
                    if (data)
                        pesan = "Category berhasil dibuat"

                    TampilkanPesanCategory(pesan);

                })
                // close modal
                $('#category-modal-add').modal('hide');
                $('#category-modal-input').val('');
            } else {
                var pesan = "Nama category harus > 5 karakter";
                TampilkanPesanCategory(pesan);
            }

        })


        /* SAVE BUTTON */
        /* CREATE BUTTON */

        /* EDIT BUTTON - Edit Modal */
        function EditCategory(catID) {
            // load nama category by ID
            LoadCategorByID(catID, function (data) {
                var nama = '';
                var id = '';
                $.each(data.d, function (index, item) {
                    nama = item.Name;
                    id = item.ID;
                });
                // insert to modal edit input form    
                $('#category-modal-edit-input').val(nama);
                $('#category-modal-edit-id').val(id);
                $('#category-modal-edit').modal('show');
            })
        }

        // Trigger Save Edit Button
        $('#category-modal-edit-btnsave').click(function () {
            // reference category
            var category = {};
            category.ID = $('#category-modal-edit-id').val();
            category.Name = $('#category-modal-edit-input').val();
            category.ModifiedBy = IDStaff;

            // Panggil ajax
            UpdateCategory(category, function (data) {

                var pesan = "Category gagal diUpdate";
                if (data)
                    pesan = "Category berhasil diUpdate"

                TampilkanPesanCategory(pesan);
            });

            // Hide Modal Edit
            $('#category-modal-edit').modal('hide');
        });

        // Trigger Delete Button DeleteCategory
        $('#category-modal-edit-btndelete').click(function () {
            var catIDtoDelete = $('#category-modal-edit-id').val();
            DeleteCategory(catIDtoDelete, function (data) {
                var pesan = "Category gagal diHapus";
                if (data)
                    pesan = "Category berhasil dihapus"

                TampilkanPesanCategory(pesan);
            });
            // Hide Modal Edit
            $('#category-modal-edit').modal('hide');
        });

        /* EDIT BUTTON - Edit Modal */

        // Trigger Search
        $('#category-search').keypress(function () {
            var key = $('#category-search').val();
            if (key.length >= 3) {
                SearchCategory(key, FillCategoryTable);
            } else {
                LoadCategory(FillCategoryTable);
            }
        });

        $(document).ready(function () {
            LoadCategory(FillCategoryTable);

            $('#search-item').select2();

        })

    </script>
</asp:Content>

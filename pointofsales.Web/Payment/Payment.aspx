﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="pointofsales.Web.Scripts.Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 43px;
        }

        div#panelright {
            position: absolute;
            right: 0px;
            height: auto;
            width: 400px;
            top: 15%;
            background: #ffffff;
            overflow-y: auto;
        }

        .table-borderless tbody tr td, .table-borderless tbody tr th, .table-borderless thead tr th {
            border: none;
            border-bottom: hidden;
        }
    </style>
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>P A Y M E N T</h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="DashBoard">Dashboard</a>
                        <span class="divider"></span>
                    </li>
                    <li class="active">Payment
                    </li>
                </ul>
            </div>
        </div>
        <%-- Panel Utama tempat munculnya tebel list item --%>
        <div id="Panel-Item" class="panel" style="margin-right: 350px;">
            <div class="panel-heading" style="padding-bottom: 2px; padding-top: 2px; background-color: lightgray">
                <h4 style="margin-bottom: 3px; margin-top: 3px">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    List Item
                </h4>
            </div>

            <div class="panel-body">
                <div class="col-md-12" style="margin-bottom: 5%">
                    <div class="col-md-2">
                        <div class="btn-group">
                            <button id="btn-ddcat" class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" style="width: 100%">
                                All Ketegori <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="dd-data">
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <input id="txt-SearchItem" type="text" class="form-control" placeholder="Search Item" style="height: auto" />
                            <span class="input-group-btn">
                                <button id="btn-SearchItem" class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form">
                        <table class="table table-striped" style="margin: 1px 1px 1px 1px; padding: 2px 2px 2px 2px;">
                            <thead>
                                <tr style="border-style: solid;">
                                    <th class="auto-style1"><span class="glyphicon glyphicon-list"></span></th>
                                    <th>Nama Item </th>
                                    <th>Price </th>
                                    <th style="width: 10%;"></th>
                                </tr>
                            </thead>
                            <tbody id="data-item">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%--// PANEL PINGGIR TEMPAT MUNCULNYA PEMBAYARAN--%>
        <div>
            <div id="panelright" class="panel panel-primary">
                <div class="panel-heading" style="padding-bottom: 2px; padding-top: 4px;">
                    <div class="input-group col-md-12">
                        <div class="col-md-2"><span class="glyphicon glyphicon-user" style="font-size: 30px"></span></div>
                        <div style="text-align: center" class="col-md-8">
                            <label class="btn" id="Txt-Costemer" style="text-align: center; font-size: large">New Custemer</label>
                            <input type="hidden" id="txt-CostID" />
                        </div>
                        <div>
                            <div class="col-md-1" style="right: 0%"><span class="glyphicon glyphicon-list-alt" style="font-size: 30px"></span></div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tbody id="data-pembayaran">
                        </tbody>
                        <tfoot>
                            <tr style="border-bottom: hidden">
                                <td>Subtotal </td>
                                <td></td>
                                <td style="text-align: right" id="SubtotalPrice"></td>
                            </tr>
                            <tr>
                                <td>Total </td>
                                <td></td>
                                <td class="Total" style="text-align: right" id="TotalPrice"></td>
                            </tr>
                            <tr style="border-bottom: 2px solid; border-top: 2px solid">
                                <td colspan="3" style="text-align: center;">
                                    <label id="ClearSale" class="btn">Clear Sale </label>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="panel-footer panel-primary" style="text-align: center; background-color: #337ab7;">
                    <div id="btn-charge" class="form-inline btn">
                        <label class="h3" style="color: white">Charge RP.</label>
                        <label class="h3" style="color: white" id="charge"></label>
                    </div>
                </div>
            </div>
        </div>
        <%--//MODAL UNTUK INPUT NEW COSTUMER--%>
        <div class="modal fade" id="modal-NewCostemer" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2>NEW COSTEMER</h2>
                        <h4>P R O F I L E</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-borderless toggle-disabled" id="Input">
                            <tr>
                                <td>
                                    <input id="CustName" type="text" class="form-control" style="outline: none; width: 100%;" placeholder="Customer Name..." name="Name" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="CustEmail" type="email" class="form-control" style="outline: none; width: 100%;" placeholder="Email (Required)" name="email" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="CustPhone" type="text" class="form-control" style="outline: none; width: 100%;" placeholder="Phone" name="Phone" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="CustBirthday" type="text" class="form-control" style="outline: none; width: 100%;" placeholder="Birthday" name="Birthday" /></td>
                            </tr>
                        </table>
                        <h4>Address(Optional)</h4>
                        <table class="table table-borderless" style="width: 100%">
                            <tr>
                                <td colspan="3">
                                    <input id="CustAddrass" type="text" class="form-control" style="outline: none; width: 100%;" placeholder="Address..." name="Adress" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="Provinsi" id="propinsi-id" class="form-control">
                                    </select>
                                </td>
                                <td>
                                    <select name="Kota" id="kota-id" class="form-control">
                                    </select>
                                </td>
                                <td>
                                    <select id="kecamatan-id" class="form-control">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <button type="submit" id="SaveCustomer" class="btn btn-primary btn-block">Save Customer</button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%-- Modal Untuk tempat list costumer --%>
        <div class="modal fade" id="modal-ListCostemer" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <button class="btn btn-default" data-dismiss="modal"> Cancel </button>
                            </div>
                            <div class="col-md-8" style="text-align: center">
                                <label class="h4" id="Txt-listCost" style="text-align: center; font-size: large">List Costemer</label>
                            </div>                           
                        </div>
                    </div>
                    <div class="modal-body">
                        <table class="tabel table-borderless" style="width: 100%">
                            <thead>
                                <tr style="width: 100%; margin-bottom: 10px">
                                    <td colspan="4" style="padding-bottom: 10px">
                                        <div class="input-group" style="width: 100%;">
                                            <input id="txt-SearchCouster" type="text" class="form-control" placeholder="Search Item" style="width: 100%" />
                                            <span class="input-group-btn">
                                                <button id="btn-SearcCostemer" class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="width: 100%; margin-bottom: 10px">
                                    <td colspan="4" style="padding-bottom: 20px">
                                        <button id="btn-NewCostemer" type="button" class="btn btn-primary" style="width: 100%">New Costemer</button>
                                    </td>
                                </tr>

                            </thead>
                            <tbody id="Data-ListCostemer"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%-- Modal Untuk Charge --%>
        <div class="modal fade" id="modal-Change" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>C H A R G E</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-borderless">
                            <tr>
                                <td>
                                    <input type="text" class="form-control" id="in-cash" style="width: 100%;" placeholder="Cash..." /></td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" id="out-total" class="form-control" style="width: 100%;" placeholder="Total" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <h3 style="text-align: center;">Change: Rp.<span id="change"></span></h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button class="btn btn-primary btn-block">Print Receipt</button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <link href="../Content/bootstrap.css" rel="stylesheet" />

    <script src="../Scripts/jquery-3.1.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>
    <script src="../Scripts/location-ajax.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <script>

        <%--<% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString()); %>--%>
        <%--<% Response.Write("var EmployeID =" + HttpContext.Current.Session["IDStaff"].ToString()); %>--%>
        var IDOutlet = 1; 
        var EmployeID = 1;

        //Load Item ALL and bysearch
        function loadSearch(SearchValue) {
            $.ajax({
                url: '../Services/ItemServis.asmx/GetLayoutItem',
                data: '{"search":"' + SearchValue + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (Items) {
                    var result = "";

                    $.each(Items.d, function (index, item) {
                        if (item.IDOutlet == IDOutlet) {
                            result += '<tr>' +
                            '<td>' + '</td>' +
                            '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
                            '<td>' + item.Price + '</td>' +
                            '<td style=""><span onclick="ChooseCost(' + item.varID + ')" class="glyphicon glyphicon-menu-right btn"></span></td>' +
                            '<td class="hidden">' + item.Stock + '</td>' +
                            '</tr>'
                        }
                    });

                    $('#data-item').html(result);

                }
            });
        }
        //Load item by category
        function loadcategory(SearchValue) {
            $.ajax({
                url: '../Services/ItemServis.asmx/GetLayoutItembyCat',
                data: '{"search":"' + SearchValue + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (Items) {
                    var result = "";

                    $.each(Items.d, function (index, item) {
                        if (item.IDOutlet == IDOutlet) {
                            result += '<tr>' +
                            '<td>' + '</td>' +
                            '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
                            '<td>' + item.Price + '</td>' +
                            '<td style="padding-bottom: 10px; font-size: medium"><span onclick="ChooseCost(' + item.ID + ')" class="glyphicon glyphicon-menu-right btn"></span></td>' +
                            '<td class="hidden">' + item.Stock + '</td>' +
                            '</tr>'
                        }
                    });

                    $('#data-item').html(result);
                }
            });
        }
        //load Category di DD
        function loadCat(SearchValue) {
            $.ajax({
                url: '../Services/categoryservice.asmx/GetSemuaCategory',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (Items) {
                    var result = "";
                    result += '<li><a href="#">' + 'All Category' + '</a></li>'

                    $.each(Items.d, function (index, item) {
                        result += '<li><a href="#">' + item.Name + '</a></li>'
                    });

                    $('#dd-data').html(result);
                }
            });
        }

        $(document).ready(function () {
            var key = $('#txt-SearchItem').val();
            var key1 = '';
            loadSearch(key);
            loadCat(key1);
            LoadSemuaProvinsi(IsiSelectProvinsi);
        })

        $(document).on('click', '#dd-data li a', function () {
            var selText = $(this).text();
            $('#btn-ddcat').html(selText + '<span class="caret"</span>');
            if (selText == 'All Category') {
                selText = '';
            }
            loadcategory(selText);
        });

        $('#btn-SearchItem').click(function () {
            var SearchValue = $('#txt-SearchItem').val();
            loadSearch(SearchValue);
        });

        //click tombol munculi modal list costemer
        $('#Txt-Costemer').click(function () {
            LoadSemuaProvinsi(IsiSelectProvinsi);
            LoadCostumer('');
            $('#modal-ListCostemer').modal('show');
        });

        function IsiSelectProvinsi(dataProvinsi) {
            var listProv = '';
            listProv += '<option value="-1">--Pilih Provinsi--</option>'
            $.each(dataProvinsi, function (index, item) {
                listProv += '<option value="' + item.ID + '">' + item.ProvinceName + '</option>'
            })
            $('#propinsi-id').html(listProv);
        }

        $('#propinsi-id').change(function () {
            var listKota = '';
            listKota += '<option value="-1">--Pilih Kota--</option>'
            var provId = $('#propinsi-id').val();
            // Clear kota
            $('#kota-id').html(listKota);
            LoadKotaByProvinsiId(provId, function (datakota) {
                $.each(datakota, function (i, item) {
                    listKota += '<option value="' + item.ID + '">' + item.RegionName + '</option>';
                })
                $('#kota-id').html(listKota);
            })
        })

        $('#kota-id').change(function () {
            var listKeca = '';
            listKeca += '<option value="-1">--Pilih Kecamatan--</option>'
            var kotaId = $('#kota-id').val();
            // Clear kota
            $('#kecamatan-id').html(listKeca);
            LoadKecamatanByKotaId(kotaId, function (datakeca) {
                $.each(datakeca, function (i, item) {
                    listKeca += '<option value="' + item.ID + '">' + item.DistrictName + '</option>';
                })
                $('#kecamatan-id').html(listKeca);
            })
        })

        //click tombol munculi modal new costemer
        $('#btn-NewCostemer').click(function () {
            $('#CustName').val('');
            $('#CustEmail').val('');
            $('#CustPhone').val('');
            $('#CustBirthday').val('');
            $('#CustAddrass').val('');
            $('#kecamatan-id').val('');
            $('#modal-ListCostemer').modal('hide');
            $('#modal-NewCostemer').modal('show');
        });

        function SumPrice() {
            var PriceList = [];
            var UnitList = [];
            var total = 0;
            var idx = 0;
            $(".price").each(function () {
                PriceList[idx] = $(this).text()
                idx++;
            });
            idx = 0;
            $(".unit").each(function () {
                UnitList[idx] = $(this).text()
                idx++;
            })
            for (var i = 0; i <= ($(".price").length - 1) ; i++) {
                total += parseInt(PriceList[i]) * parseInt(UnitList[i]);
            }
            $('#SubtotalPrice').text(total);
            $('#TotalPrice').text(total);
        }

        function ChooseCost(ID) {
            $.ajax({
                url: '../Services/ItemServis.asmx/GetDataByIDVar',
                data: '{"ID":"' + JSON.stringify(ID) + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (Items) {
                    var ID = [];
                    var clue = false;
                    var idx = 0;
                    var jmlh = []
                    var BuyTable = '';
                    $.each(Items.d, function (index, item) {
                        $('#data-pembayaran tr').each(function () {
                            ID.push($(this).find("td:nth-child(4)").text());
                            if (ID[idx] == item.varID) {
                                clue = true;
                                jmlh.push($(this).find("td:nth-child(2)").text());
                                jmlh = parseInt(jmlh) + 1;
                                return false;
                            }
                            else {

                            }
                            clue = false;
                            idx += 1;
                        });
                        idx += 1;
                    });
                    $.each(Items.d, function (index, item) {
                        if (item.Stock != 0) {
                            if (clue) {
                                var Buy = '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
                                            '<td class="unit">' + jmlh + '</td>' +
                                            '<td style="text-align:right" class="price">' + item.Price + '</td>' +
                                            '<td class="hidden">' + item.varID + '</td>' +
                                            '<td class="hidden">0</td>'
                                $('#data-pembayaran tr:nth-child(' + idx + ')').html(Buy);
                            }

                            else {
                                BuyTable += '<tr>' +
                                '<td >' + item.ItemName + " - " + item.Variant + '</td>' +
                                '<td class="unit">' + 1 + '</td>' +
                                '<td style="text-align:right" class="price">' + item.Price + '</td>' +
                                '<td class="hidden">' + item.varID + '</td>' +
                                '<td class="hidden">0</td>' +
                                '</tr>'
                            }
                        }
                        else
                            return false;
                    });
                    $('#data-pembayaran').append(BuyTable);
                    SumPrice();
                    charge();
                }
            });
        }

        $('#ClearSale').click(function () {
            var VarTable = "";
            $('#data-pembayaran').html(VarTable);
            SumPrice();
        });

        function charge() {
            $('#charge').text($('.Total').text());

        }

        $('#btn-charge').click(function () {
            $('#modal-Change').modal('show');
            $('#out-total').val($('.Total').text());
            SavePay();
        });

        $('#in-cash').keyup(function () {
            var cash = parseInt($('#in-cash').val());
            code = isNaN(cash)
            if (code)
                var change = 0 - parseInt($('#out-total').val());
            else
                var change = parseInt($('#in-cash').val()) - parseInt($('#out-total').val());
            $('#change').text(change);
        })

        function SavePay() {
            var PaySave = {};
            //PaySave.ID = $('#txt-PayID').val() //Input belom ada
            PaySave.CustomerID = $('#txt-CostID').val() //input belom ada
            PaySave.EmployeeID = EmployeID //input belom ada
            PaySave.GrandTotal = $('.Total').text();
            var PDs = [];
            $("#data-pembayaran tr").each(function (index, item) {
                var PD = {};
                //Masukin data ke PD
                PD.UnitPrice = $(this).find('td:nth-child(3)').text();
                PD.Quantity = $(this).find('td:nth-child(2)').text();
                PD.VariantID = $(this).find('td:nth-child(4)').text();
                PD.SubTotal = $('#SubtotalPrice').text();
                //PD.PaymentID = PaySave.ID;
                PD.ID = $(this).find('td:nth-child(5)').text();
                PDs.push(PD);
            })
            PaySave.PaymentDetails = PDs;
            $.ajax({
                url: '../Services/PaymentServices.asmx/SavePay',
                data: '{Pay: ' + JSON.stringify(PaySave) + ',PDs:' + JSON.stringify(PDs) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    alert("Pembelian Berhasil");
                }
            });
        }

        function LoadCostumer(SearchValue) {
            $.ajax({
                url: '../services/PaymentServices.asmx/GetCustemer',
                data: '{"Search":"' + SearchValue + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (Items) {
                    var result = "";
                    $.each(Items.d, function (index, item) {
                        result += '<tr class="table table-striped">' +
                                        '<td style="padding-bottom: 10px; padding-left: 5px; font-size: medium"><span class="glyphicon glyphicon-user"></span> ' + item.CustomerName + '</td>' +
                                        '<td style="padding-bottom: 10px; font-size: medium"><span class="glyphicon glyphicon-envelope"></span> ' + item.Email + '</td>' +
                                        '<td style="padding-bottom: 10px; font-size: medium"><span class="glyphicon glyphicon-earphone"></span> ' + item.Phone + '</td>' +
                                        '<td style="padding-bottom: 10px; font-size: medium"><span onclick="Select(' + item.ID + ')" class="glyphicon glyphicon-menu-right btn"></span></td>' +
                                    '</tr>'
                    });
                    $('#Data-ListCostemer').html(result);
                }
            });
        }

        function SaveCustemer() {
            var Custemer = {};

            Custemer.CustomerName = $('#CustName').val();
            Custemer.Email = $('#CustEmail').val();
            Custemer.Phone = $('#CustPhone').val();
            Custemer.BirthDate = $('#CustBirthday').val();
            Custemer.Address = $('#CustAddrass').val();
            Custemer.DistrictID = $('#kecamatan-id').val();

            $.ajax({
                url: '../services/PaymentServices.asmx/SaveCustemer',
                data: '{ Search : ' + JSON.stringify(Custemer) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    var pesan = "Costumer GAGAL Dibuat";
                    if (result)
                        pesan = "Costumer BERHASIL Dibuat"
                    alert(pesan);
                    clear();
                }
            });
        }

        function clear() {
            $('#CustName').text('');
            $('#CustEmail').text('');
            $('#CustPhone').text('');
            $('#CustBirthday').text('');
            $('#CustAddrass').text('');
            $('#kecamatan-id').val('');
        }

        $('#SaveCustomer').click(function (e) {
            SaveCustemer();
            var Search = $('#txt-SearchCouster').val();
            LoadCostumer(Search);
            $('#modal-NewCostemer').modal('hide');
            $('#modal-ListCostemer').modal('show');
        })

        $('#btn-SearcCostemer').click(function () {
            var Search = $('#txt-SearchCouster').val();
            LoadCostumer(Search);
        });

        function Select(ID, Name) {
            //$('#Txt-Costemer').text('Name');
            $('#txt-CostID').val(ID);
            var SearchValue = '';
            $.ajax({
                url: '../services/PaymentServices.asmx/GetCustemer',
                data: '{"Search":"' + SearchValue + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        if (item.ID == ID) {
                            $('#Txt-Costemer').text(item.CustomerName);
                        }
                    })
                }
            })
            $('#modal-ListCostemer').modal('hide');
        }   
        //function valid(){
        //    $('#Input').validate({
        //        rules: {
        //            Name: "required",
        //            email: {
        //                required: true,
        //                email: true
        //            },

        //            Phone: "required",
        //            Birthday: "required",
        //            Adress: "required"
        //        },
        //        messages: {
        //            Name: "Please enter your Name",
        //            Phone: "Please enter your Phone Number",
        //            Adress: "Please enter your Address",
        //            Birthday: "Please enter your Birthday",
        //            email: "Please enter a valid email address"
        //        }
        //    })
        //}

        //$('#Input').submit(function (e) {
        //    e.preventDefault();
        //})
    </script>
</body>
</html>

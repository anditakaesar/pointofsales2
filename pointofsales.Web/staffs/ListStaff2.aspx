﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="ListStaff2.aspx.cs" Inherits="pointofsales.Web.staffs.ListStaff2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .form-control {
            margin-bottom: 12px;
        }

        .staff-row {
            width: 60%;
            margin-bottom: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
    // Cek USER jika memiliki IDRole 1 dan 3 yaitu (Backend dan SuperAdmin)
        if (HttpContext.Current.Session["IDRole"] != null && (HttpContext.Current.Session["IDRole"].ToString() == "1" || HttpContext.Current.Session["IDRole"].ToString() == "3"))
        {

    %>
    <div class="row">
        <h3>Staff</h3>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-7 col-md-7">
            <input type="hidden" id="staff-id" />
            <div class="form-inline">
                <select id="staff-title" class="form-control" style="width: 48%;">
                    <option value="Mr.">Mr.</option>
                    <option value="Mrs.">Mrs.</option>
                    <option value="Ms.">Ms.</option>
                </select>
                <input type="text" class="form-control pull-right" id="staff-first-name" placeholder="First Name..." style="width: 48%;" />
            </div>
            <div class="form-inline">
                <input type="text" class="form-control" id="staff-last-name" placeholder="Last Name..." style="width: 48%;" />
                <input type="text" class="form-control pull-right" id="staff-email" placeholder="Email..." style="width: 48%;" />
            </div>
            <div class="form-inline">
                <input type="password" class="form-control" id="staff-pass1" placeholder="Password..." style="width: 48%;" />
                <input type="password" class="form-control pull-right" id="staff-pass2" placeholder="Type Password Again" style="width: 48%;" />
            </div>
            <div class="form-inline">
                <!-- Role hanya membuat staff tertentu saja -->
                <%--<button class="btn btn-primary" id="staff-assign-btn" style="width: 48%; margin-bottom: 12px;">Assign To Outlet</button>--%>
                <select id="staff-role" class="form-control pull-right" style="width: 100%;">
                </select>
            </div>
            <div class="form-inline">
                <button type="button" class="btn btn-primary btn-block" id="staff-add-btn" style="width: 100%;">Add Staff</button>
            </div>
        </div>
    </div>
    <!-- Table Employee / Staff -->
    <br />
    <div class="row">
        <h4>Staff List</h4>
        <hr />
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Access</th>
                </tr>
            </thead>
            <tbody id="staff-table-body">
            </tbody>
            <tfoot>
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Access</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- Modal Pesan -->
    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footerBuatJavascript" runat="server">
    <script type="text/javascript">
        <% 
        Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString() + ";");
        Response.Write("var IDStaff =" + HttpContext.Current.Session["IDStaff"].ToString() + ";");
        %>

        // Fungsi load semua employee
        function LoadStaff(callback) {
            $.ajax({
                url: '../services/staffservice.asmx/GetSemuaStaff',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataStaff) {
                    callback(dataStaff);
                }
            });
        }

        // Fungsi AddStaff
        function AddStaff(employee, callback) {
            $.ajax({
                url: '../services/staffservice.asmx/AddEmployee',
                data: '{ employee:'+ JSON.stringify(employee) +'}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    callback(result);
                }
            });
        }

        // Fungsi Load Role
        function LoadRoles(callback) {
            $.ajax({
                url: '../services/staffservice.asmx/GetRoles',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (roles) {
                    callback(roles.d);
                }
            });
        }

        // Load Role by Id
        function LoadRoleById(roleId, callback) {
            $.ajax({
                url: '../services/staffservice.asmx/GetRoleById',
                data: '{"id":'+ roleId +'}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (role) {
                    callback(role.d);
                }
            });
        }

        // Fungsi Isi table Staff
        function FillStaffTable(dataStaff) {
            var listStaff = '';
            $.each(dataStaff.d, function (index, item) {
                listStaff += '<tr>'
                    + '<td>' + item.Title + ' ' + item.FirstName + ' ' + item.LastName + '</td>'
                    + '<td>' + item.Email + '</td>'
                    //+ '<td>' + item.RoleID + '</td>'
                    + '<td>' + LoadRoleName(item.RoleID) + '</td>'
                    + '</tr>';
            })
            $('#staff-table-body').html(listStaff);
        }

        // Mengisi Dropdown role
        function FillRoleDropdown(roles) {
            var list = '';
            $.each(roles, function (i, role) {
                list += '<option value="'+ role.ID +'">'
                    + role.RoleName + '</option>'
            })
            $('#staff-role').html(list);
        }

        function LoadRoleName(id) {
            if (id == 1) {
                return "BackOffice";
            } else if (id == 2) {
                return "App";
            } else if (id == 3) {
                return "App & BackOffice";
            } else {
                return "";
            }
        }

        // Clear Form Staff
        function ClearFormStaff() {
            //$('#').val('');
            $('#staff-id').val('-1');
            $('#staff-title').val('Mr.');
            $('#staff-first-name').val('');
            $('#staff-last-name').val('');
            $('#staff-email').val('');
            $('#staff-pass1').val('');
            $('#staff-pass2').val('');
        }

        // Trigger Button AddStaff
        // staff-add-btn
        $('#staff-add-btn').click(function () {
            // Jika ID kosong
            var idTest = $('#staff-id').val();
            var pass1 = $('#staff-pass1').val();
            var pass2 = $('#staff-pass2').val();
            // var IDOutlet
            if (pass1 == pass2) {
                if (idTest) {
                    // UPDATE
                } else {
                    // CREATE
                    // Masukkan object employee
                    var employee = {};
                    employee.Title = $('#staff-title').val();
                    employee.FirstName = $('#staff-first-name').val();
                    employee.LastName = $('#staff-last-name').val();
                    employee.Email = $('#staff-email').val();
                    employee.Password = $('#staff-pass1').val();
                    employee.RoleID = $('#staff-role').val();
                    // ambil OutletID
                    employee.OutletID = IDOutlet;
                    employee.CreatedBy = IDStaff;

                    // Buat Employee
                    AddStaff(employee, function (result) {
                        var pesan = "Staff GAGAL DIBUAT"
                        if (result)
                            pesan = "Staff BERHASIL DIBUAT"

                        TampilkanPesanStaff(pesan);
                        ClearFormStaff();
                    });
                }
            } else {
                TampilkanPesanStaff("Password tidak sama!!")
            }

        })

        // Tampilkan pesan staff
        function TampilkanPesanStaff(pesan) {
            $('#staff-pesan-msg').html(pesan);

            $('#staff-pesan').modal('show');

            LoadStaff(FillStaffTable);
        }

        $(document).ready(function () {
            LoadStaff(FillStaffTable);
            LoadRoles(FillRoleDropdown);
        })
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBackOffice.Master" AutoEventWireup="true" CodeBehind="ChangePassword2.aspx.cs" Inherits="pointofsales.Web.staffs.ChangePassword2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="title" runat="server"><title>Change Password</title></asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .form-control {
            margin-bottom: 12px;
        }

        .staff-row {
            width: 60%;
            margin-bottom: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        if (HttpContext.Current.Session["IDRole"] != null )
        {
    %>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>Change Password</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <input type="hidden" id="staff-id" value="1" />
                                <h3>Hello, <span id="staff-name"></span></h3>
                                <hr />
                                <div class="form-inline">
                                    <input type="password" class="form-control" id="staff-pass-old" placeholder="Type your last password..." style="width: 100%;" />
                                </div>
                                <hr />
                                <h4>New Password</h4>
                                <div class="form-inline">
                                    <input type="password" class="form-control" id="staff-pass1" placeholder="New Password..." style="width: 48%;" />
                                    <input type="password" class="form-control" id="staff-pass2" placeholder="Type New Password Again" style="width: 48%;" />
                                </div>
                                <div class="form-inline">
                                    <input type="button" class="btn btn-primary btn-block" id="staff-change-pass-btn" style="width: 100%;" value="Change Password" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>

    <%}
        else
        {
            Response.Redirect("../login/index.aspx");
        }
    %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerBuatJavaScript" runat="server">
    <script>
        //ambil id outlet dari session staff yang sedang login
        <% Response.Write("var Nama =\"" + HttpContext.Current.Session["Nama"].ToString()+"\";"); %>
        <% Response.Write("var IDStaff =\"" + HttpContext.Current.Session["IDStaff"].ToString()+"\""); %>

        $(document).ready(function () {
            $("#staff-pass-old").val('');
            $("#staff-pass1").val('');
            $("#staff-pass2").val('');
            $('#staff-name').html(Nama);
        });

        function saveTransferStock(ts) {
            $.ajax({
                url: '../services/TransferStockService.asmx/SaveTransferStock',
                data: '{obj:' + JSON.stringify(ts) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset = utf-8;',
                success: function (data) {
                    if (data) {
                        alert('Save Success!');
                        location.reload();
                    }
                    else {
                        alert('Save Failed!');
                    }
                }
            });
        }

        //event saat tombol change password ditekan
        $('#staff-change-pass-btn').click(function () {
            var result = true;

            if (result && $("#staff-pass-old").val() == '') {
                result = false;
                alert("Password lama harus diisi!");
                $("#staff-pass-old").focus();
            }
            if (result && $("#staff-pass1").val() == '') {
                result = false;
                alert("Password baru harus diisi!");
                $("#staff-pass1").focus();
            }
            if (result && $("#staff-pass2").val() == '') {
                result = false;
                alert("Ketik ulang password baru harus diisi!");
                $("#staff-pass2").focus();
            }
            if (result && $("#staff-pass2").val() != $("#staff-pass1").val()) {
                result = false;
                alert("Password baru tidak sama!");
                $("#staff-pass2").focus();
            }
            if (result) {
                var oldPass = $("#staff-pass-old").val();
                var pass1 = $("#staff-pass1").val();
                var pass2 = $("#staff-pass2").val();
                if (confirm("Are you sure want to change your password?")) {
                    $.ajax({
                        url: '../services/loginservistoni.asmx/CekPassUser',
                        data: '{ID:' + IDStaff + ', "Pass":"'+oldPass+'"}',
                        type: 'POST',
                        dataType: 'JSON',
                        contentType: 'application/json; charset=utf-8',
                        success: function (dataAnu) {
                            if (dataAnu.d) {
                                changePass(IDStaff, pass1);
                            } else {
                                alert("password lama anda salah!");
                                $("#staff-pass-old").focus();
                            }
                        }
                    });
                }
            }
        });

        function changePass(id,pass) {
            $.ajax({
                url: '../services/loginservistoni.asmx/UbahPassUser',
                data: '{ID:' + id + ', "Pass":"' + pass + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataAnu) {
                    if (dataAnu.d) {
                        alert("Change Password Success!");
                        $(document).ready(function () {
                            $("#staff-pass-old").val('');
                            $("#staff-pass1").val('');
                            $("#staff-pass2").val('');
                        });

                    } else {
                        alert("Change Password Failed!");
                    }
                }
            });
        }
    </script>
</asp:Content>

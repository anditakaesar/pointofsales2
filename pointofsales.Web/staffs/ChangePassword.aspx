﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="pointofsales.Web.staffs.ChangePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <style>
        .form-control {
            margin-bottom: 12px;
        }

        .staff-row {
            width: 60%;
            margin-bottom: 8px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <h3>Staff</h3>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-offset-2 col-lg-7 col-md-7">
                <input type="hidden" id="staff-id" value="1"/>
                <h3>Hello, <span id="staff-name"></span></h3>
                <hr />
                <div class="form-inline">
                    <input type="password" class="form-control" id="staff-pass-old" placeholder="Type your last password..." style="width: 100%;" />
                </div>
                <hr />
                <h4>New Password</h4>
                <div class="form-inline">
                    <input type="password" class="form-control" id="staff-pass1" placeholder="New Password..." style="width: 48%;" />
                    <input type="password" class="form-control" id="staff-pass2" placeholder="Type New Password Again" style="width: 48%;" />
                </div>
                <div class="form-inline">
                    <button class="btn btn-primary btn-block" id="staff-change-pass-btn" style="width: 100%;">Change Password</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Pesan -->
    <div class="modal fade" tabindex="-1" role="dialog" id="pesan">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <span id="pesan-msg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Pesan -->
    <script src="../Scripts/jquery-3.1.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>
    <script type="text/javascript">
        // Get Staff by ID
        // Bisa diganti dari SESSION staff
        function GetStaffById(staffId, callback) {
            $.ajax({
                url: '../services/staffservice.asmx/GetStaffById',
                data: '{ "id":' + staffId + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (staff) {
                    callback(staff.d);
                }
            });
        }

        // Update Password
        function UpdatePassword(staffId, pass) {
            var employee = {};
            employee.ID = staffId;
            employee.Password = pass;
            $.ajax({
                url: '../services/staffservice.asmx/GetStaffById',
                data: '{ employee:' + JSON.stringify(employee) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (staff) {
                    callback(staff.d);
                }
            });
        }

        $(document).ready(function () {
            var id = $('#staff-id').val();
            GetStaffById(id, function (staff) {
                $('#staff-name').html(staff.Title + ' ' + staff.FirstName);
            });
        });

    </script>
</body>
</html>

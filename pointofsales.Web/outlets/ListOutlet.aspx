﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListOutlet.aspx.cs" Inherits="pointofsales.Web.outlets.ListOutlet" %>

<!DOCTYPE html>
<!-- MULAI EDIT PADA file ListOutlet2.aspx -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>List Outlet</title>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
</head>
<body style="margin-top: 10px;">
    <div class="container">
        <div class="row">
            <h3>List Outlet</h3>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <input type="text" class="form-control" id="outlet-search" placeholder="search..." />
            </div>
            <div class="col-md-6 col-lg-6">
                <button type="button" class="btn btn-success pull-right" id="outlet-add-btn">Add Outlet</button>
            </div>
        </div>
        <hr />
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Outlet Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>District</th>
                    <th>Postal Code</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="outlet-table-body">
            </tbody>
            <tfoot>
                <tr>
                    <th>Outlet Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>District</th>
                    <th>Postal Code</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- Modal Untuk Outlet -->
    <div class="modal fade" tabindex="-1" role="dialog" id="outlet-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 style="margin-left: 5px;">Outlet Detail</h4>
                    <hr />
                    <div class="form-group">
                        <input type="hidden" id="outlet-id"/>
                        <input type="text" class="form-control" placeholder="Outlet Name..." style="margin-bottom: 4px;" id="outlet-name"/>
                        <input type="text" class="form-control" placeholder="Alamat" style="margin-bottom: 4px;" id="outlet-address"/>
                        <input type="text" class="form-control" placeholder="Telepon" style="width: 100%; margin-bottom: 4px;" id="outlet-phone"/>
                    </div>
                    <div class="form-group">
                        <div class="form-inline">
                            <select id="select-provinsi" class="form-control" style="width: 49%; margin-bottom: 4px;"></select>
                            <select id="select-kota" class="form-control" style="width: 49%; margin-bottom: 4px;"></select>
                        </div>
                        <div class="form-inline">                          
                            <select id="select-kecamatan" class="form-control" style="width: 49%; margin-bottom: 4px;"></select>
                            <input type="text" class="form-control" placeholder="Postcode" style="width: 49%; margin-bottom: 4px;" id="outlet-postcode" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="outlet-modal-save">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Untuk Outlet -->
    <!-- Modal Pesan -->
    <div class="modal fade" tabindex="-1" role="dialog" id="outlet-pesan">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <span id="outlet-pesan-msg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Pesan -->
    <script src="../Scripts/jquery-3.1.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>
    <script src="../Scripts/location-ajax.js"></script>
    <script type="text/javascript">
        // Fungsi get semua outlet
        function LoadOutlet(callback) {
            $.ajax({
                url: '../services/outletservice.asmx/GetOutletWithLocation',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (dataOutlet) {
                    callback(dataOutlet.d);
                }
            });
        }

        // Fungsi getoutlet by outletID
        function LoadOutletById(outID, callback) {
            $.ajax({
                url: '../services/outletservice.asmx/GetOutletVMById',
                data: '{ "id" : '+ outID +'}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (outlet) {
                    callback(outlet.d);
                }
            });
        }

        // Fungsi Search Outlet
        function SearchOutlet(key, callback) {
            $.ajax({
                url: '../services/outletservice.asmx/SearchOutletVM',
                data: '{ "key" : ' + JSON.stringify(key) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (outlet) {
                    callback(outlet.d);
                }
            });
        }

        // Fungsi Add Outlet
        function AddOutlet(outlet, callback) {
            $.ajax({
                url: '../services/outletservice.asmx/AddOutlet',
                data: '{ outlet : ' + JSON.stringify(outlet) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    callback(result);
                }
            });
        }

        // Fungsi Edit Outlet
        function UpdateOutlet(outlet, callback) {
            $.ajax({
                url: '../services/outletservice.asmx/UpdateOutlet',
                data: '{ outlet : ' + JSON.stringify(outlet) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    callback(result);
                }
            });
        }

        // Fungsi fill table outlet
        function FillTableOutlet(outlets) {
            var listOutlet = '';
            $.each(outlets, function (index, item) {
                listOutlet += '<tr>'
                + '<td>' + item.OutletName + '</td>'
                + '<td>' + item.Address + '</td>'
                + '<td>' + item.Phone + '</td>'
                + '<td>' + item.Kecamatan + '</td>'
                + '<td>' + item.PostalCode + '</td>'
                + '<td><button type="button" onclick="EditOutlet(this.value)" value="'+ item.ID +'" class="btn btn-primary">Edit</button></td>'
                + '</tr>';
            });
            $('#outlet-table-body').html(listOutlet);
        }

        // Trigger Search Outlet
        $('#outlet-search').keypress(function () {
            var key = $('#outlet-search').val();
            if (key && key.length >= 3) {
                SearchOutlet(key, FillTableOutlet);
            } else {
                LoadOutlet(FillTableOutlet);
            }
        })

        // Load Nama Kecamatan
        var LoadNamaKecamatan = function(id) {
            LoadNamaKecamatanById(id, function (data) {
                alert(data);
                return data;
            })
        }

        // Clear Form
        function ClearFormOutlet() {
            $('#outlet-id').val('');
            $('#outlet-name').val('');
            $('#outlet-address').val('');
            $('#outlet-phone').val('');
            $('#select-kota').html('<option>--Pilih Provinsi--</option>');
            $('#select-kecamatan').html('<option>--Pilih Kota--</option>');
            $('#outlet-postcode').val('');
        }

        // Trigger Add
        $('#outlet-add-btn').click(function () {
            ClearFormOutlet();
            $('#outlet-modal').modal('show');
        })

        // Trigger save button pada Outlet Detail
        $('#outlet-modal-save').click(function () {
            var idTest = $('#outlet-id').val();
            var outlet = {};
            
            // Masukkan data outlet
            outlet.OutletName = $('#outlet-name').val();
            outlet.Address = $('#outlet-address').val();
            outlet.Phone = $('#outlet-phone').val();
            outlet.DistrictID = $('#select-kecamatan').val();
            outlet.PostalCode = $('#outlet-postcode').val();

            // jika Ada berarti update
            if (idTest) { // UPDATE
                
                outlet.ID = $('#outlet-id').val();
                UpdateOutlet(outlet, function (result) {
                    var pesan = "Outlet GAGAL DiUpdate"
                    if (result)
                        pesan = "Outlet BERHASIL DiUpdate"

                    TampilkanPesanOutlet(pesan);
                    ClearFormOutlet();
                    LoadOutlet(FillTableOutlet);
                });
            } else { // CREATE
                AddOutlet(outlet, function (data) {
                    var pesan = "Outlet GAGAL Dibuat";
                    if (data)
                        pesan = "Outlet BERHASIL Dibuat"

                    TampilkanPesanOutlet(pesan);
                    ClearFormOutlet();
                    LoadOutlet(FillTableOutlet);
                });
            }

        });

        // Fungsi Edit
        function EditOutlet(outId) {
            LoadOutletById(outId, function (outlet) {
                $('#outlet-id').val(outlet.ID);
                $('#outlet-name').val(outlet.OutletName);
                $('#outlet-address').val(outlet.Address);
                $('#outlet-phone').val(outlet.Phone);
                //$('#outlet-district').val(outlet.DistrictID);
                //var kecaId = outlet.DistrictID;
                //$('#select-kecamatan').html('<option value="'+ kecaId +'">' + kecaId + '</option>')
                var keca = '<option value="' + outlet.DistrictID + '">' + outlet.Kecamatan + '</option>';
                var kota = '<option>' + outlet.Kota + '</option>';
                $('#select-kecamatan').html(keca);
                $('#select-kota').html(kota);
                $('#outlet-postcode').val(outlet.PostalCode);
                $('#outlet-modal').modal('show');
            });
            
        }

        // LOKASI
        // Isi Dropdown Provinsi
        function IsiSelectProvinsi(dataProvinsi) {
            var listProv = '';
            listProv += '<option value="-1">--Pilih Provinsi--</option>'
            $.each(dataProvinsi, function (index, item) {
                listProv += '<option value="' + item.ID + '">' + item.ProvinceName + '</option>'
            })
            $('#select-provinsi').html(listProv);
        }

        // Isi Dropdown Kecamatan

        // Isi Dropdown Kota
        // Trigger provinsi diubah -> kota di load
        $('#select-provinsi').change(function () {
            var listKota = '';
            listKota += '<option value="-1">--Pilih Kota--</option>'
            var provId = $('#select-provinsi').val();
            // Clear kota
            $('#select-kota').html(listKota);
            LoadKotaByProvinsiId(provId, function (datakota) {
                $.each(datakota, function (i, item) {
                    listKota += '<option value="' + item.ID + '">' + item.RegionName + '</option>';
                })
                $('#select-kota').html(listKota);
            })
        })

        // Isi Dropdown Kecamatan
        // Trigger Kota diubah -> kecamatan di load
        $('#select-kota').change(function () {
            var listKeca = '';
            listKeca += '<option value="-1">--Pilih Kecamatan--</option>'
            var kotaId = $('#select-kota').val();
            // Clear kota
            $('#select-kecamatan').html(listKeca);
            LoadKecamatanByKotaId(kotaId, function (datakeca) {
                $.each(datakeca, function (i, item) {
                    listKeca += '<option value="' + item.ID + '">' + item.DistrictName + '</option>';
                })
                $('#select-kecamatan').html(listKeca);
            })
        })

        // Set Dropdown to default
        function ClearLocationDropdown() {
            var listKota = '<option value="-1">--Pilih Kota--</option>';
            var listKeca = '<option value="-1">--Pilih Kecamatan--</option>';
            $('#select-kecamatan').html(listKeca);
            $('#select-kota').html(listKota);
        }

        // Fungsi tampilkan pesan
        function TampilkanPesanOutlet(pesan) {
            $('#outlet-modal').modal('hide');

            $('#outlet-pesan-msg').html(pesan);

            $('#outlet-pesan').modal('show');

        }

        $(document).ready(function () {
            LoadOutlet(FillTableOutlet);
            LoadSemuaProvinsi(IsiSelectProvinsi);
        })
    </script>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cobacoba.aspx.cs" Inherits="pointofsales.Web.Items.cobacoba" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
    <form id="myform"   >
        <table>
            <tr>
                <td>
                    <input type="text" name="field1" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)" />
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="field2" />
                </td>
            </tr>
            <tr>
                <td>
                    <input id="IDsubmit" type="submit" class="btn btn-success" />
                </td>
            </tr>
        </table>
    </form>
</body>

<link href="../Content/bootstrap.css" rel="stylesheet" />
<script src="../Scripts/jquery-3.1.1.js"></script>
<script src="../Scripts/form-validator/jquery.form-validator.min.js"></script>
<script src="../Scripts/jquery.validate.js"></script>
<script src="../Scripts/form-validator/jquery.form-validator.js"></script>
<script src="../Scripts/bootstrap.js"></script>

<script>
    $(document).ready(function () {
        $.validate({
            form: '#myform',
            modules: 'location, date, security, file',
            onSuccess: function ($form) {
                alert('The form ' + $form.attr('id') + ' is valid!');
                return false; // Will stop the submission of the form
            },
            onError: function (e) {
                return true;
                alert('hihi')
            }
        })
    });


    
</script>
</html>

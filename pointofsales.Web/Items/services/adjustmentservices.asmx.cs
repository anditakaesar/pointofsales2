﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.Entity;
using pointofsales.DAL.ViewModel;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for adjustmentservices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class adjustmentservices : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<ItemInventoryViewModel> GetAllAdjustment()
        {
            return AdjustmentDAL.GetAllAdjustment();
        }

        [WebMethod(EnableSession = true)]
        public List<EmployeeViewModel> GetCurrentOutlet()
        {
            return AdjustmentDAL.GetCurrentOutlet();
        }

        [WebMethod]
        public List<ItemVariantViewModel> GetAllVariant()
        {
            return AdjustmentDAL.GetAllVariant();
        }

        [WebMethod]
        public List<ItemVariantViewModel> GetVariantByID(int ID)
        {
            return AdjustmentDAL.GetVariantByID(ID);
        }

        [WebMethod]
        public bool SaveDataAdjusment(HeaderAdjusmentStockViewModel obj)
        {
            return AdjustmentDAL.SaveDataAdjusment(obj);
        }

    }
}

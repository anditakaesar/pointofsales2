﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for PurchaseOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PurchaseOrder : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetData()
        {
            return PurchaseOrderDAL.GetData();
        }
        [WebMethod]
        public List<Outlet> GetOutlet()
        {
            return PurchaseOrderDAL.GetOutlet();
        }
        [WebMethod]
        public List<Supplier> GetSupplier()
        {
            return PurchaseOrderDAL.GetSupplier();
        }
        [WebMethod]
        public List<PurchaseOrderStatus> GetStatus()
        {
            return PurchaseOrderDAL.GetStatus();
        }
        [WebMethod]
        public List<Supplier> GetSupplierById(int id)
        {
            return PurchaseOrderDAL.GetSupplierById(id);
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetStatusById(int id)
        {
            return PurchaseOrderDAL.GetStatusById(id);
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetSum()
        {
            return PurchaseOrderDAL.GetSum();
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetForAddPO()
        {
            return PurchaseOrderDAL.GetForAddPO();
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetItemSearch(string search)
        {
            return PurchaseOrderDAL.GetItemSearch(search);
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetItemById(int id)
        {
            return PurchaseOrderDAL.GetItemById(id);
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetAllId(int id)
        {
            return PurchaseOrderDAL.GetAllId(id);
        }
        [WebMethod]
        public int AddTrxPo(PurchaseOrderDAL add)
        {
            return PurchaseOrderDAL.AddTrxPo(add);
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetDetailConfirm(int id)
        {
            return PurchaseOrderDAL.GetDetailConfirm(id);
        }
        [WebMethod]
        public List<PurchaseOrderDAL> GetItemByPOId(int id)
        {
            return PurchaseOrderDAL.GetItemByPOId(id);
        }
        [WebMethod]
        public bool UpdateStatusPO(int id, int stat, int order)
        {
            return PurchaseOrderDAL.UpdateStatusPO(id,stat, order);
        }
    }
}

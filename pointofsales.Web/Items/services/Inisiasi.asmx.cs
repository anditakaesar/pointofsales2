﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Hosting;
using System.IO;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for Inisiasi
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Inisiasi : System.Web.Services.WebService
    {

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hello World";
        //}

        [WebMethod]
        public List<Category> InisiasiDB()
        {
            return InisiasiDAL.Inisiasi();
        }

        //[WebMethod]
        //public List<Province> InisiasiProvinsi()
        //{
        //    return InisiasiDAL.InisiasiLokasi(File.OpenRead(HttpContext.Current.Server.MapPath("provinsi.csv")));
        //}
    }
}

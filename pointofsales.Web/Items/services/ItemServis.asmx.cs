using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.DAL;
using pointofsales.Model;


namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for ItemServis
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ItemServis : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        
        // Git Checkout
        // [WebMethod]
        // public List<ItemDAL> GetLayoutItem(string search)
        // {
        //    return ItemDAL.GetLayoutItem(search);
        // }


        [WebMethod]
        public List<ItemDAL> GetLayoutItem(string search)
        {
            return ItemDAL.GetLayoutItem(search);
        }
        
        [WebMethod]
        public List<ItemDAL> GetDataByID(string search)
        {
            return ItemDAL.GetDataByID(int.Parse(search));
        }
        [WebMethod]
        public List<ItemDAL> GetLayoutItembyCat(string search)
        {
            return ItemDAL.GetLayoutItembyCat(search);
        }

        [WebMethod]
        public bool SaveItem(ItemDAL ItemSaved, IsiItem Isi)
        {
            return ItemDAL.SaveItem3(ItemSaved,Isi);
        }

        [WebMethod]
        public List<ItemDAL> GetDataByIDVar(string ID)
        {
            return ItemDAL.GetDataByIDVar(int.Parse(ID));
        }

        [WebMethod]
        public bool DeleteVariant(string ID)
        {
            return ItemDAL.DeleteVariant(int.Parse(ID));
        }

        //untuk load item - variant berdasarkan id outlet yang login by Toni
        [WebMethod]
        public List<ItemDAL> GetDataVar(int IDOutlet)
        {
            return ItemDAL.GetDataVar(IDOutlet);
        }
    }
}
/*=======
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.DAL;
using pointofsales.Model;


namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for ItemServis
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ItemServis : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        
        // Git Checkout
        // [WebMethod]
        // public List<ItemDAL> GetLayoutItem(string search)
        // {
        //    return ItemDAL.GetLayoutItem(search);
        // }


        [WebMethod]
        public List<ItemDAL> GetLayoutItem(string search)
        {
            return ItemDAL.GetLayoutItem(search);
        }

        [WebMethod]
        public List<ItemDAL> GetLayoutItembyCat(string search)
        {
            return ItemDAL.GetLayoutItembyCat(search);
        }

        [WebMethod]
        public bool SaveItem(ItemDAL ItemSaved, IsiItem Isi)
        {
            return ItemDAL.SaveItem(ItemSaved,Isi);
        }

        [WebMethod]
        public List<ItemDAL> GetDataByIDVar(string ID)
        {
            return ItemDAL.GetDataByIDVar(int.Parse(ID));
        }
        
        //untuk load item - variant berdasarkan id outlet yang login by Toni
        [WebMethod]
        public List<ItemDAL> GetDataVar(int IDOutlet)
        {
            return ItemDAL.GetDataVar(IDOutlet);
        }


    }
}
*/

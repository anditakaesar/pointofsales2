﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using pointofsales.DAL;
using pointofsales.Model;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for supplierservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class supplierservice : System.Web.Services.WebService
    {
        [WebMethod]
        public List<Supplier> GetSemuaSupplier()
        {
            return SupplierDAL.GetSupplier();
        }

        [WebMethod]
        public Supplier GetSupplierByID(string id)
        {
            return SupplierDAL.GetSupplier(int.Parse(id));
        }

        [WebMethod]
        public List<Supplier> SearchSupplier(string key)
        {
            return SupplierDAL.SearchSupplier(key);
        }

        [WebMethod]
        public bool AddSupplier(Supplier supplier)
        {
            return (SupplierDAL.InsertSupplier(supplier));
        }

        [WebMethod]
        public bool UpdateSupplier(Supplier supplier)
        {
            return (SupplierDAL.UpdateSupplier(supplier));
        }
    }
}

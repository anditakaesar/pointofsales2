﻿using pointofsales.DAL;
using pointofsales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace pointofsales.Web.services
{
    /// <summary>
    /// Summary description for outletservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class outletservice : System.Web.Services.WebService
    {
        [WebMethod]
        public List<Outlet> GetSemuaOutlet()
        {
            return OutletDAL.GetOutlet();
        }

        [WebMethod]
        public Outlet GetOutletById(string id)
        {
            return OutletDAL.GetOutlet(int.Parse(id));
        }

        [WebMethod]
        public bool AddOutlet(Outlet outlet)
        {
            return OutletDAL.InsertOutlet(outlet);
        }

        [WebMethod]
        public bool UpdateOutlet(Outlet outlet)
        {
            return OutletDAL.UpdateOutlet(outlet);
        }

        // Method baru, search
        [WebMethod]
        public List<Outlet> SearchOutlet(string key)
        {
            return OutletDAL.SearchOutlet(key);
        }
    }
}

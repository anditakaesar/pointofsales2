<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LayoutAllItem.aspx.cs" Inherits="pointofsales.Web.Items.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        div#panelright {
            position: absolute;
            right: 0px;
            height: 100%;
            width: 450px;
            top: 15%;
            background: #ffffff;
            overflow-y: auto;
        }
    </style>
</head>
<body>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-3.1.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>


    <div id="Main-Contain-Item">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>ITEM</h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="DashBoard">Home</a>
                            <span class="divider"></span>
                        </li>
                        <li class="active">Item List
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="panel1" class="panel panel-primary">
                        <div class="panel-heading" style="padding-bottom: 2px; padding-top: 2px">
                            <h4 style="margin-bottom: 3px; margin-top: 3px">
                                <span class="glyphicon glyphicon-align-justify"></span>
                                List Item
                            </h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12" style="margin-bottom: 5%">
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <button id="btn-ddcat" class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" style="width: 100%">
                                            All Ketegori <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" id="dd-data">
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="txt-SearchItem" type="text" class="form-control" placeholder="Search Item" style="height: auto" />
                                        <span class="input-group-btn">
                                            <button id="btn-SearchAnggota" class="btn btn-default" type="button">LOL!</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-offset-3 col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" id="btn-AddItem" type="button" style="width: auto; margin-right: 6px">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                            <button class="btn btn-primary" id="btn-SortItem" type="button" style="width: auto">
                                                <span class="glyphicon glyphicon-sort"></span>
                                            </button>
                                        </span>
                                        <input id="txt-sort" type="text" class="form-control" placeholder="sort by" style="width: 100%" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form">
                                    <table class="table table-stripped" style="margin: 1px 1px 1px 1px; padding: 2px 2px 2px 2px;">
                                        <thead>
                                            <tr style="border-style: solid;">
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>In Stock</th>
                                                <th>Stock Alert</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="data-ListItem"></tbody>
                                    </table>
                                    <div class="form">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-10" style="margin-top: 2%">
                                                <button id="btn-CloseItem" class="btn btn-danger">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="panelright" class="panel panel-primary">
            <div class="panel-heading" style="height: auto; padding-bottom: 3px; padding-top: 3px">
                <h4 style="margin-top: 1px; margin-bottom: 1px">Create Item
                </h4>
            </div>
            <div class="panel-body">
                <table class="table" style="border-style: hidden; margin-bottom: auto;">
                    <tr>
                        <td style="border-bottom: hidden" class="auto-style7">
                            <input class="form-control" id="txt-NamaItem" style="width: 100%" placeholder="Nama Item" />
                            <input id="txt-VarID" type="hidden" />
                        </td>
                        <td rowspan="3" style="width: 20px;"></td>
                        <td rowspan="3" style="width: 130px">
                            <a href="#" class="thumbnail" style="height: 100%">
                                <img src="..." alt="..." style="height: 57px; width: 86px;" />
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td class="auto-style8">
                            <select class="form-control" id="DDCat">
                                
                            </select>
                        </td>
                    </tr>
                </table>
                <div class="col-md-12">
                    <table id="tbl-variant" class="table">
                        <thead>
                            <tr>
                                <th style="border-bottom: ridge">Variant </th>
                                <th style="border-bottom: ridge">Price </th>
                                <th style="border-bottom: ridge">SKU </th>
                            </tr>
                        </thead>
                        ;                   
                            <tbody id="data-variant" style="">
                                <tr>
                                    <td contenteditable='true' style="width: 30%; border: 1px solid"></td>
                                    <td contenteditable='true' style="width: 30%; border: 1px solid"></td>
                                    <td contenteditable='true' style="width: 30%; border: 1px solid"></td>
                                    <td style="display: none">1</td>
                                </tr>
                            </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <button id="btn-addvariant" class="btn btn-primary" type="button" style="width: 100%; text-align: center">Add Variant</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="col-md-12" style="margin-top: auto">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Inventory </th>
                                <th style="text-align: left">
                                    <h6 style="margin-bottom: 1px; margin-top: 1px">In Stock</h6>
                                </th>
                                <th>
                                    <h6 style="margin-bottom: 1px; margin-top: 1px">Alert</h6>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="data-invstock">
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <button id="btn-alert" class="btn btn-primary" type="button" style="width: 100%; text-align: center">Start Tracking Item Inventory and Alert</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="col-md-12">
                    <h6>Description</h6>
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" id="txt-descrip">
                    </textarea>
                </div>
            </div>
            <div class="panel-footer" style="background: #ffffff">
                <div class="btn-group col-md-12">
                    <button type="button" class="btn btn-success btn-xs" id="btn-UpdateItem" style="margin-right: 10px">Update</button>
                    <button type="button" class="btn btn-success btn-xs" id="btn-saveitem" style="margin-right: 10px">Save</button>
                    <button type="button" class="btn btn-danger btn-xs" id="btn-CancelItem">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-Addvariant" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Variant</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-primary" style="text-align: center; height: auto">
                            <div class="panel-heading" style="color: white">Add Variant</div>

                            <div class="panel-body input-group">
                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px">
                                    <input id="txt-varNama" class="form-control" placeholder="Variant" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px">
                                    <input id="txt-varPrice" class="form-control" placeholder="Price" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px">
                                    <input id="txt-varSKU" class="form-control" placeholder="SKU" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="button" id="btn-saveVar" class="btn-Success btn-md">Save</button>
                                <button type="button" id="btn-closeVar" data-dismiss="modal" class="Close btn-primary btn-md">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-addalert" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-heading">
                    <h4 style="text-align: center">Manage Inventory</h4>
                    <div class="modal-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Variant Name</th>
                                    <th>In stock</th>
                                    <th>alert at</th>
                                </tr>
                            </thead>
                            <tbody id="data-stockinv">
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="button" id="btn-saveStock" class="btn-Success btn-md">Save</button>
                                <button type="button" id="btn-closeStock" data-dismiss="modal" class="Close btn-primary btn-md">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

<script>

    <%--<% Response.Write("var IDOutlet =" + HttpContext.Current.Session["IDOutlet"].ToString()); %>--%>
    var IDOutlet = 1;
    function loadSearch(SearchValue) {
        $.ajax({
            url: '../Services/ItemServis.asmx/GetLayoutItem',
            data: '{"search":"' + SearchValue + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (Items) {
                var result = "";
                $.each(Items.d, function (index, item) {
                    if (item.IDOutlet == IDOutlet) {
                        result += '<tr>' +
                                    '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
                                    '<td>' + item.CatName + '</td>' +
                                    '<td>' + item.Price + '</td>' +
                                    '<td>' + item.Stock + '</td>' +
                                    '<td>' + item.AlertAt + '</td>' +
                        '<td>' + '<button type="button" onClick="ChooseRow(' + item.ID + ',' + item.varID + ')" class="btn btn-primary">Choose</button>' + '</td>' +
                        '<td>' + '<button type="button" onClick="DeleteRow(' + item.varID + ')" class="btn btn-primary">DeleteRow</button>' + '</td>' +
                        '</tr>'
                    }
                });
                $('#data-ListItem').html(result);
            }
        });
    }

    //function loadSearch(SearchValue, IDOutlet) {
    //    $.ajax({
    //        url: '../Services/ItemServis.asmx/GetLayoutItem',
    //        data: '{"search":"' + SearchValue + ',IDOutlet: ' + JSON.stringify(IDOutlet) + '"}',
    //        type: 'POST',
    //        dataType: 'JSON',
    //        contentType: 'application/json; charset=utf-8',
    //        success: function (Items) {
    //            var result = "";
    //            $.each(Items.d, function (index, item) {
    //                result += '<tr>' +
    //                            '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
    //                            '<td>' + item.CatName + '</td>' +
    //                            '<td>' + item.Price + '</td>' +
    //                            '<td>' + item.Stock + '</td>' +
    //                            '<td>' + item.AlertAt + '</td>' +
    //                '<td>' + '<button type="button" onClick="ChooseRow(' + item.ID + ',' + item.varID + ')" class="btn btn-primary">Choose</button>' + '</td>' +
    //                '<td>' + '<button type="button" onClick="DeleteRow(' + item.varID + ')" class="btn btn-primary">DeleteRow</button>' + '</td>' +
    //                '</tr>'
    //            });
    //            $('#data-ListItem').html(result);
    //        }
    //    });
    //}

    function loadcategory(SearchValue) {
        $.ajax({
            url: '../Services/ItemServis.asmx/GetLayoutItembyCat',
            data: '{"search":"' + SearchValue + '"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (Items) {
                var result = "";
                $.each(Items.d, function (index, item) {
                    if (item.IDOutlet == IDOutlet) {
                        result += '<tr>' +
                                    '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
                                    '<td>' + item.CatName + '</td>' +
                                    '<td>' + item.Price + '</td>' +
                                    '<td>' + item.Stock + '</td>' +
                                    '<td>' + item.AlertAt + '</td>' +
                        '<td>' + '<button type="button" onClick="ChooseRow(' + item.ID + ',' + item.varID + ')" class="btn btn-primary">Choose</button>' + '</td>' +
                        '<td>' + '<button type="button" onClick="DeleteRow(' + item.varID + ')" class="btn btn-primary">Choose</button>' + '</td>' +
                        '</tr>'
                    }
                });
                $('#data-ListItem').html(result);
            }
        });
    }

    //function loadcategory(SearchValue,IDOutlet) {
    //    $.ajax({
    //        url: '../Services/ItemServis.asmx/GetLayoutItembyCat',
    //        data: '{"search":"' + SearchValue + ',IDOutlet: ' + JSON.stringify(IDOutlet) + '"}',
    //        type: 'POST',
    //        dataType: 'JSON',
    //        contentType: 'application/json; charset=utf-8',
    //        success: function (Items) {
    //            var result = "";
    //            $.each(Items.d, function (index, item) {
    //                result += '<tr>' +
    //                            '<td>' + item.ItemName + " - " + item.Variant + '</td>' +
    //                            '<td>' + item.CatName + '</td>' +
    //                            '<td>' + item.Price + '</td>' +
    //                            '<td>' + item.Stock + '</td>' +
    //                            '<td>' + item.AlertAt + '</td>' +
    //                '<td>' + '<button type="button" onClick="ChooseRow(' + item.ID + ',' + item.varID + ')" class="btn btn-primary">Choose</button>' + '</td>' +
    //                '<td>' + '<button type="button" onClick="DeleteRow(' + item.varID + ')" class="btn btn-primary">Choose</button>' + '</td>' +
    //                '</tr>'
    //            });
    //            $('#data-ListItem').html(result);
    //        }
    //    });
    //}

    function loadCat(SearchValue) {
        $.ajax({
            url: '../Services/categoryservice.asmx/GetSemuaCategory',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (Items) {
                var result = "";
                result += '<li><a href="#">' + 'All Category' + '</a></li>'

                $.each(Items.d, function (index, item) {
                    result += '<li><a href="#">' + item.Name + '</a></li>'
                });

                $('#dd-data').html(result);
            }
        });
    }
    function loadCatAdd(SearchValue) {
        $.ajax({
            url: '../Services/categoryservice.asmx/GetSemuaCategory',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (Items) {
                var result = "";

                $.each(Items.d, function (index, item) {
                    result += '<option value="' + item.ID + '">' + item.Name + '</option>'
                });

                $('#DDCat').html(result);
            }
        });
    }
    $(document).ready(function() {
        var key = $('#txt-SearchItem').val();
        var key1 = '';
        loadSearch(key);
        loadCat(key1);
        loadCatAdd(key1)
    });
    $(document).on('click', '#dd-data li a', function () {
        var selText = $(this).text();
        $('#btn-ddcat').html(selText + '<span class="caret"</span>');
        if (selText == 'All Category') {
            selText = '';
        }
        loadcategory(selText);
    });  
    $('#btn-SearchAnggota').click(function () {
        var SearchValue = $('#txt-SearchItem').val();
        loadSearch(SearchValue);
    });
    $(function () {
        $('#panelright').stop().animate({
            'margin-right': '-100%'
        }, 1000);

        function toggleDivs() {
            var $inner = $("#panelright");
            if ($inner.css("margin-right") == "0px") {
                $inner.animate({
                    'margin-right': '-50%'
                });
                $('#panel1').animate({
                    'margin-right': '0px'
                })
            } else {
                $inner.animate({
                    'margin-right': "0px"
                });
                $('#panel1').animate({
                    'margin-right': $('#panelright').css("Width")
                });
            }
        }
        $("#btn-AddItem").bind("click", function () {
            $('#btn-saveitem').show();
            $('#btn-UpdateItem').hide();
            $('#txt-VarID').val('0')
            $("#txt-NamaItem").val('');
            $("#DDCat option:selected").text('Uncategory');
            loadCatAdd('');
            $("#data-variant").html(' <tr>' +
                                        '<td contenteditable="true" style="width: 30%; border: 1px solid"></td>' +
                                        '<td contenteditable="true" style="width: 30%; border: 1px solid"></td>' +
                                        '<td contenteditable="true" style="width: 30%; border: 1px solid"></td>' +
                                        '<td style="display:none">0</td>' +
                                    '</tr>')
            $("#data-invstock").html('');
            toggleDivs();
        });
        $("#btn-CancelItem").bind("click", function () {
            toggleDivs();
        });
    });
    $('#btn-addvariant').click(function () {
        $('#modal-Addvariant').modal('show');
    });
    $('#btn-alert').click(function () {
        $('#modal-addalert').modal('show');
    });
    $('#btn-saveVar').click(function () {
        var VariantAdd = {};
        VariantAdd.VarName = $('#txt-varNama').val();
        VariantAdd.VarPrice = $('#txt-varPrice').val();
        VariantAdd.VarSKU = $('#txt-varSKU').val();

        var newVariant = '<tr>' +
            '<td style="border:1px solid">' + VariantAdd.VarName + '</td>' +
            '<td style="border:1px solid">' + VariantAdd.VarPrice + '</td>' +
            '<td style="border:1px solid">' + VariantAdd.VarSKU + '</td>' +
            '<td style="display:none; border:1px solid">' + 0 + '</td>' +
            "</tr>"
        $("#data-variant").append(newVariant);
        $('#modal-Addvariant').modal('hide');
        $('#txt-varNama').val('')
        $('#txt-varPrice').val('');
        $('#txt-varSKU').val('');
    });
    $('#btn-alert').click(function () {
        var NVar = [];
        $("#data-variant tr").each(function () {
            NVar.push($(this).find("td:first").text()); //put elements into array
        });
        var newInv = '';
        $.each(NVar, function (index, item) {
            newInv += '<tr>' +
            '<td style="border:1px solid">' + item + '</td>' +
            '<td contenteditable="true"; style="border:1px solid" >' + '</td>' +
            '<td contenteditable="true"; style="border:1px solid" >' + '</td>' +
            "</tr>"
        });
        $('#data-stockinv').html(newInv);
        $('#btn-saveStock').click(function () {
            $('#data-invstock').html($('#data-stockinv').html());
            $('#modal-addalert').modal('hide');
        })
    });
    function simpanItem() {
        var ItemSave = {};
        ItemSave.ID = $('#txt-VarID').val();
        ItemSave.Name = $("#txt-NamaItem").val();
        ItemSave.CategoryID = $("#DDCat").val();
        ItemSave.VariantSave = [];

        var IsiItem = [];
        var InvItem = [];

        $("#data-invstock tr").each(function () {
            var obj = {};
            obj.Stock = $(this).find("td:nth-child(2)").text();
            obj.AlertAt = $(this).find("td:nth-child(3)").text();
            obj.Begining = obj.Stock;
            obj.Ending = obj.Stock;
            InvItem.push(obj);
        })
        var i = 0;
        $("#data-variant tr").each(function () {
            var obj = {};
            obj.VariantName = $(this).find("td:nth-child(1)").text();
            obj.Price = $(this).find("td:nth-child(2)").text();
            obj.SKU = $(this).find("td:nth-child(3)").text();
            //obj.VarID = $(this).find("td:nth-child(4)").text();
            obj.InveSave = InvItem[i];
            i++;
            IsiItem.push(obj);
        })

        ItemSave.VariantSave = IsiItem;

        $.ajax({
            url: '../Services/ItemServis.asmx/SaveItem',
            data: '{Item: ' + JSON.stringify(ItemSave) + '}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                alert("Itemberhasil disimpan.");
                loadSearch('');
            }
    });
    }

    function UpdateItem() {
        var ItemSave = {};
        ItemSave.ID = $('#txt-VarID').val();
        ItemSave.Name = $("#txt-NamaItem").val();
        ItemSave.CategoryID = $("#DDCat").val();
        ItemSave.VariantSave = [];

        var IsiItem = [];
        var InvItem = [];

        $("#data-invstock tr").each(function () {
            var obj = {};
            obj.Stock = $(this).find("td:nth-child(2)").text();
            obj.AlertAt = $(this).find("td:nth-child(3)").text();
            obj.Begining = obj.Stock;
            obj.Ending = obj.Stock;
            InvItem.push(obj);
        })
        var i = 0;
        $("#data-variant tr").each(function () {
            var obj = {};
            obj.VariantName = $(this).find("td:nth-child(1)").text();
            obj.Price = $(this).find("td:nth-child(2)").text();
            obj.SKU = $(this).find("td:nth-child(3)").text();
            obj.ID = $(this).find("td:nth-child(4)").text();
            obj.InveSave = InvItem[i];
            i++;
            IsiItem.push(obj);
        })

        ItemSave.VariantSave = IsiItem;

        $.ajax({
            url: '../Services/ItemServis.asmx/UpdateItem',
            data: '{Item: ' + JSON.stringify(ItemSave) + '}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                alert("Item berhasil disimpan.");
                loadSearch('');
            }
        });
    }


    $('#btn-saveitem').click(function () {
        simpanItem();
        $('#panelright').animate({
            'margin-right': '-50%'
        });
        $('#panel1').animate({
            'margin-right': '0px'
        })
    });
    function ChooseRow(ID,varID) {
        $.ajax({
            url: '../Services/ItemServis.asmx/GetDataByID',
            data: '{"search":"' + JSON.stringify(ID) +'"}',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            success: function (Items) { 
                var VarTable = "";
                var InvTabel = "";
                $.each(Items.d, function (index, item) {
                    $('#txt-VarID').val(ID)
                    $('#txt-NamaItem').val(item.ItemName),
                    $('#DDCat').val(item.CatID),
                    $("#DDCat option:selected").text(item.CatName),
                    VarTable += '<tr>' +
                    '<td contenteditable="true"; style="border:1px solid">' + item.Variant + '</td>' +
                    '<td contenteditable="true"; style="border:1px solid">' + item.Price + '</td>' +
                    '<td contenteditable="true"; style="border:1px solid">' + item.SKU + '</td>' +
                    '<td contenteditable="true"; style="display:none; border:1px solid">' + item.varID + '</td>' +
                    "</tr>"
                    InvTabel += '<tr>' +
                    '<td style="border:1px solid">' + item.Variant + '</td>' +
                    '<td contenteditable="true"; style="border:1px solid" >' + item.Stock + '</td>' +
                    '<td contenteditable="true"; style="border:1px solid" >' + item.AlertAt + '</td>' +
                    "</tr>"
                });
                $('#data-variant').html(VarTable);
                $('#data-invstock').html(InvTabel);
                $('#btn-saveitem').hide();
                $('#btn-UpdateItem').show();
                $('#panelright').animate({
                    'margin-right': '0%'
                });
                $('#panel1').animate({
                    'margin-right': $('#panelright').css("Width")
                })
            }
        });
    } 
    function DeleteRow(ID) {
        var r = confirm("Are You Sure To Delete")
        if (r == true) {
            $.ajax({
                url: '../Services/ItemServis.asmx/DeleteVariant',
                data: '{ID: ' + JSON.stringify(ID) + '}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    alert("Berhasil Dihapus");
                    loadSearch('');
                }
            })
        }
    }

    $('#btn-UpdateItem').click(function () {
        UpdateItem();
    })
</script>
</html>

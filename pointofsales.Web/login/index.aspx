﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="pointofsales.Web.login.index" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>:.Halaman Login.:</title>
    <link rel="stylesheet" href="../Content/loginstyle.css" />
</head>
<body>
    <form>

        <input name="username" placeholder="username" id="username" type="text" />
        <label for="username">Username</label>


        <input name="password" placeholder="password" id="password" type="password" />
        <label for="password">Password</label>


        <input type="submit" value="Log In" id="login" />
    </form>

    <script type="text/javascript" src="../Scripts/jquery-3.1.1.js"></script>

    <script type="text/javascript">
        //awal kodingan mendisable button back di browser firefox saat di halaman login
        function changeHashOnLoad() {
            window.location.href += "#";
            setTimeout("changeHashAgain()", "50");
        }

        function changeHashAgain() {
            window.location.href += "1";
        }

        var storedHash = window.location.hash;
        window.setInterval(function () {
            if (window.location.hash != storedHash) {
                window.location.hash = storedHash;
            }
        }, 50);

        //akhir kodingan mendisable button back di browser firefox saat di halaman login
        $(document).ready(function () {
            $("#username").val('');
            $("#password").val('');
            $.ajax({
                url: '../services/loginservistoni.asmx/Logout',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d) {
                        alert("Alert You Are Logout");
                    } else {
                        alert("Welcome");
                    }
                }
            });
            changeHashOnLoad();//memanggil fungsi untuk mendisable button back di browser firefox saat di halaman login
        });

        $("#login").click(function () {
            var username = $("#username").val();
            var password = $("#password").val();
            $.ajax({
                url: '../services/loginservistoni.asmx/CekUser2', // before: CekUser
                data: '{"username":"' + username + '","password":"' + password + '"}',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var result = response.d.result;
                    var IDRole = response.d.IDRole;

                    if (result) {
                        $("#username").val('');
                        $("#password").val('');

                        if (IDRole == "2") {
                            window.location.href = "../home/indexPayment.aspx"
                        } else {
                            window.location.href = "../home/index2.aspx";//diubah sama toni asalnya hrefnya bukan ini
                        }

                    } else {
                        $("#username").val('');
                        $("#password").val('');
                        alert("username atau password anda salah");
                    }

                }
            })
            return false;
        })

    </script>
</body>
</html>

